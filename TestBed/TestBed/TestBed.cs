using System;

using Microsoft.Xna.Framework;

using Flechette_Engine;

namespace Flechette_Engine.TestBed
{
    public class TestBed : Game
    {
        GraphicsDeviceManager graphics;

        public TestBed()
        {
            graphics = new GraphicsDeviceManager(this);

            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            base.Initialize();

            // Create a new instance of the Flechette GameController
            GameController.Instance(this, graphics);

            // Set up our screens
            ScreenController.screens["Main Menu"] = new Menu();

            ScreenController.screens["UIDemo"] = new UIDemo();
            ScreenController.screens["AIDemo"] = new AIDemo();
        }
    }
}
