﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Flechette_Engine;

namespace Flechette_Engine.TestBed
{
    public class AIDemo : Screen
    {
        NavMap map;

        NavAgent_new agent;

        public AIDemo()
            : base()
        {
            // Turn off gravity
            physicsController.world.Gravity = Vector2.Zero;

            // Draw the AI debug data (NavMap, Pathfinding, etc)
            aiController.DebugEnabled = true;

            // Background image
            using (Sprite item = new Sprite(this, null, drawLayers["Background"]))
            {
                item.SetSourceImage("Dark/Background");
                item.Size = Settings.RenderResolution;
                item.Position = Vector2.Zero;
                item.DrawOrder = 0.99f;
            };

            // Back button
            using (BackScreenButton item = new BackScreenButton(this, null, drawLayers["GUI"], defaultManager))
            {
                item.Position = new Vector2(10, 10);
                item.Size = new Vector2(100, 24);
                item.Text = "Back";
            };

            // Set up the navigation map
            map = new NavMap(this);
            map.Position = new Vector2(50, 50);
            map.Size = Settings.RenderResolution - (map.Position * 2);
            map.GenerateRegularMap(NavMap.MapTypes.Hexagonal, true);

            // Set up the AI agent
            using (Sprite item = new Sprite(this, null, drawLayers["GUI"]))
            {
                item.Size = new Vector2(10, 10);
                item.Position = new Vector2(80, 120);
                item.DrawOrder = 0.4f;

                using (NavAgent_new child = item.AddChild<NavAgent_new>())
                {
                    child.map = map;
                    agent = child;
                }
            }

            // Hook left click to target assignment of agent
            UIController.MouseLeftUp += UIController_MouseLeftUp;
        }

        void UIController_MouseLeftUp(object sender, EventArgs e)
        {
            // Assign mouse position as agent's nav target
            agent.SetImmediateTarget(new Vector2(
                UIController.Mouse.State.X, 
                UIController.Mouse.State.Y));
        }
    }
}
