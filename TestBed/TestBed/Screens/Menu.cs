﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Flechette_Engine;

namespace Flechette_Engine.TestBed
{
    public class Menu : Screen
    {
        public Menu()
            : base()
        {
            activeCamera.BackColor = new Color(50, 50, 50);

            using (Sprite item = new Sprite(this, null, drawLayers["Background"]))
            {
                item.SetSourceImage("Dark/Background");
                item.Size = Settings.RenderResolution;
                item.Position = Vector2.Zero;
                item.DrawOrder = 0.99f;
            };

            using(Sprite item = new Sprite(this, null, drawLayers["Background"]))
            {
                item.SetSourceImage("Logo");
                item.Size = new Vector2(400, 400);
                item.Position = (Settings.RenderResolution / 2) - item.Center;
            };

            using (ToScreenButton item = new ToScreenButton(this, null, drawLayers["GUI"], defaultManager))
            {
                item.Size = new Vector2(150, 25);
                item.Position = new Vector2(10, 10);
                item.targetScreen = "UIDemo";
                item.Text = "UI Demo";
            };

            using (ToScreenButton item = new ToScreenButton(this, null, drawLayers["GUI"], defaultManager))
            {
                item.Size = new Vector2(150, 25);
                item.Position = new Vector2(10, 50);
                item.targetScreen = "AIDemo";
                item.Text = "AI Demo";
            };

            using (ToScreenButton item = new ToScreenButton(this, null, drawLayers["GUI"], defaultManager))
            {
                item.Size = new Vector2(150, 25);
                item.Position = new Vector2(10, 90);
                item.targetScreen = "Play";
                item.Text = "Physics Demo";
            };

            using (ToScreenButton item = new ToScreenButton(this, null, drawLayers["GUI"], defaultManager))
            {
                item.Size = new Vector2(150, 25);
                item.Position = new Vector2(10, 130);
                item.targetScreen = "Play";
                item.Text = "Lighting Demo";
            };

            using (ToScreenButton item = new ToScreenButton(this, null, drawLayers["GUI"], defaultManager))
            {
                item.Size = new Vector2(150, 25);
                item.Position = new Vector2(10, Settings.RenderResolution.Y - 90);
                item.targetScreen = "Settings";
                item.Text = "Settings";
            };

            using (QuitGameButton item = new QuitGameButton(this, null, drawLayers["GUI"], defaultManager))
            {
                item.Size = new Vector2(150, 25);
                item.Position = new Vector2(10, Settings.RenderResolution.Y - 50);
                item.ID = "QuitButton";
            };
        }

        public override void Draw()
        {
            base.Draw();
        }

        public override void Update()
        {
            base.Update();
        }
    }
}
