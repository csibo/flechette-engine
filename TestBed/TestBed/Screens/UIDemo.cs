﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Flechette_Engine;

namespace Flechette_Engine.TestBed
{
    /// <summary>
    /// Tests various user interface elements.
    /// </summary>
    public class UIDemo : Screen
    {
        public UIDemo()
            : base()
        {
            activeCamera.BackColor = Color.CornflowerBlue;

            using(Sprite item = new Sprite(this, null, drawLayers["Background"]))
            {
                item.SetSourceImage("Dark/Background");
                item.Size = Settings.RenderResolution;
                item.Position = Vector2.Zero;
                item.DrawOrder = 0.99f;
            };

            using (BackScreenButton item = new BackScreenButton(this, null, drawLayers["GUI"], defaultManager))
            {
                item.Position = new Vector2(10, 10);
                item.Size = new Vector2(100, 24);
                item.Text = "Back";
            };

            using (TextBox item = new TextBox(this, null, drawLayers["GUI"], defaultManager))
            {
                item.AddString("1234567890");
                item.Position = new Vector2(100, 100);
                item.Size = new Vector2(150, 30);
                item.IsPassword = true;
            }

            using (TextBox item = new TextBox(this, null, drawLayers["GUI"], defaultManager))
            {
                item.AddString("1234567890");
                item.Position = new Vector2(150, 90);
                item.Size = new Vector2(150, 30);
                item.IsPassword = true;
                item.DrawOrder = 0.09f;
                item.Skin.Focused.BackgroundColor = Color.White * 0.75f;
                item.Skin.Pressed.BackgroundColor = Color.White * 0.75f;
                item.Skin.Enabled.BackgroundColor = Color.Aqua * 0.75f;
            }

            TextBox textbox = null;
            using (TextBox item = new TextBox(this, null, drawLayers["GUI"], defaultManager))
            {
                item.AddString("1234567890");
                item.Position = new Vector2(100, 200);
                item.Size = new Vector2(150, 30);
                textbox = item;
                defaultManager.FocusOn(item);
            }

            using (Sprite item = new Sprite(this, null, drawLayers["GUI"]))
            {
                item.Size = new Vector2(50, 50);
                item.Position = new Vector2(80, 120);
                item.DrawOrder = 0.4f;
            }

            using (Button item = new Button(this, null, drawLayers["GUI"], defaultManager))
            {
                item.Text = "Hide Text";
                item.Position = new Vector2(270, 202);
                item.Size = new Vector2(120, 28);
                item.MouseLeftClickAction = () => 
                { 
                    textbox.IsPassword = !textbox.IsPassword;
                    item.Text = textbox.IsPassword ? "Show Text" : "Hide Text"; 
                };
            }

            using (TextArea item = new TextArea(this, null, drawLayers["GUI"], defaultManager))
            {
                item.Position = new Vector2(100, 500);

                item.AddString(
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent in lectus ut odio tempus aliquam. In rutrum fermentum lobortis. Etiam auctor nibh nisl, non imperdiet justo sodales eu. Ut gravida nibh eu nisi luctus tempus. Nulla at tellus et nibh iaculis pretium eget ac magna. Curabitur quis ipsum tincidunt, blandit elit sed, pharetra nisi. Phasellus auctor ut tortor non pretium. Integer faucibus, orci ac convallis sodales, lectus mauris tincidunt massa, et tempus nulla dui vel urna. Donec in commodo arcu. Cras feugiat turpis a sem porta, sed pharetra nulla luctus."
                    //+ "Suspendisse posuere dictum egestas. Vivamus sit amet dolor condimentum velit sagittis faucibus. Maecenas dignissim lacinia quam, in blandit lacus. Phasellus nibh ante, bibendum ac vulputate a, adipiscing quis massa. Nunc id odio neque. Sed eleifend vulputate aliquet. Praesent rutrum commodo velit non dictum. Vivamus nec ipsum eros. Proin ac sem sit amet quam suscipit euismod. Nullam consequat mattis sapien, a semper nunc gravida et. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus tempus a magna non venenatis. Maecenas eget ullamcorper tortor, non sagittis eros.\n\n"
                    //+ "Maecenas vel vestibulum nunc, eu commodo tortor. Maecenas dapibus, ipsum a lobortis ornare, dolor odio porta neque, id eleifend purus erat a tortor. Curabitur ut diam nulla. Aenean odio enim, porta a dignissim vitae, laoreet sed lectus. In et elementum velit. Pellentesque at lacinia metus. Etiam id malesuada nunc. Fusce non arcu nunc. Proin vel malesuada magna. Phasellus iaculis metus justo, et egestas ante congue eget. Donec bibendum sed nisl quis ornare. Curabitur facilisis placerat fermentum. Curabitur eleifend lobortis nibh eget fringilla.\n\n"
                    //+ "Maecenas eros quam, fermentum id augue id, pellentesque vehicula nunc. Mauris adipiscing, neque sed fermentum blandit, odio purus lobortis lectus, a viverra eros magna vel odio. Aliquam sed eleifend orci. Curabitur at sodales mauris. Quisque vitae dolor ac nisi venenatis feugiat non id sapien. Nam eleifend vestibulum urna aliquet auctor. Cras in accumsan enim, in congue sem. Phasellus quis nulla nec lacus aliquam hendrerit sit amet vel quam.\n\n"
                    //+ "Phasellus id justo massa. Vivamus vel odio velit. Nam at nibh vitae nunc rhoncus vulputate. Donec ultrices laoreet leo, id pellentesque odio ultrices a. Pellentesque bibendum, nulla varius tempor vulputate, felis leo interdum lorem, sed facilisis lacus ipsum non erat. Sed vitae lobortis tellus. Suspendisse a elit bibendum risus sagittis tincidunt. Aliquam tincidunt orci in velit posuere faucibus in eget ante. Aenean egestas lacus non nunc lobortis mollis. Proin diam lacus, cursus vitae convallis non, luctus in risus."
                    );

                item.CaretPosition = 0;

                item.Skin.Enabled.FontProfile.Font.LineSpacing += 4;
            };

            using (Checkbox item = new Checkbox(this, null, drawLayers["GUI"], defaultManager))
            {
                item.Position = new Vector2(400, 100);
            }

            using (Checkbox item = new Checkbox(this, null, drawLayers["GUI"], defaultManager))
            {
                item.Position = new Vector2(400, 130);
            }

            RadioButtonController controller = new RadioButtonController();

            using (RadioButton item = new RadioButton(this, null, drawLayers["GUI"], defaultManager, controller))
            {
                item.Position = new Vector2(700, 100);
            }

            using (RadioButton item = new RadioButton(this, null, drawLayers["GUI"], defaultManager, controller))
            {
                item.Position = new Vector2(700, 130);
            }

            using (RadioButton item = new RadioButton(this, null, drawLayers["GUI"], defaultManager, controller))
            {
                item.Position = new Vector2(700, 160);
            }
        }

        void item_ValueChanged(object sender, EventArgs e)
        {
            Console.WriteLine(((Scrollbar)sender).Value);
        }

        public override void Draw()
        {
            base.Draw();

            if (UIController.Mouse.State.LeftButton == ButtonState.Pressed
                && UIController.Mouse.TimeSince.MouseLeftDown > 100
                && defaultManager.MouseLeftDownControl == null)
            {
                int x = Math.Min(UIController.Mouse.LastLeftDown.State.X, UIController.Mouse.State.X);
                int y = Math.Min(UIController.Mouse.LastLeftDown.State.Y, UIController.Mouse.State.Y);
                int w = Math.Max(UIController.Mouse.LastLeftDown.State.X, UIController.Mouse.State.X) - x;
                int h = Math.Max(UIController.Mouse.LastLeftDown.State.Y, UIController.Mouse.State.Y) - y;

                Primitives.DrawRectangle(
                    new Rectangle(x, y, w, h),
                    Color.Red,
                    true);
            }
        }

        public override void Update()
        {
            base.Update();
        }
    }
}
