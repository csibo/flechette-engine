﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flechette_Engine
{
    public class BackScreenButton : Button
    {
        public BackScreenButton(Screen containingScreen, GameObject parent, DrawLayer drawLayer, UIManager manager)
            : base(containingScreen, parent, drawLayer, manager)
        {
            Text = "<Back>";
        }

        public override void OnMouseLeftClick(EventArgs e)
        {
            ScreenController.screenStack.Pop();

            base.OnMouseLeftClick(e);
        }
    }
}
