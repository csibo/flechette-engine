﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flechette_Engine
{
    public class ToScreenButton : Button
    {
        public string targetScreen;

        public ToScreenButton(Screen containingScreen, GameObject parent, DrawLayer drawLayer, UIManager manager)
            : base(containingScreen, parent, drawLayer, manager)
        {
            Text = "<To Screen>";
        }

        public override void OnMouseLeftClick(EventArgs e)
        {
            ScreenController.screenStack.Push(targetScreen);

            base.OnMouseLeftClick(e);
        }
    }
}
