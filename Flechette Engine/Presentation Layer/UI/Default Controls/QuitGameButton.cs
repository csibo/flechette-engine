﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flechette_Engine
{
    public class QuitGameButton : Button
    {
        public QuitGameButton(Screen containingScreen, GameObject parent, DrawLayer drawLayer, UIManager manager)
            : base(containingScreen, parent, drawLayer, manager)
        {
            Text = "Quit";
        }

        public override void OnMouseLeftClick(EventArgs e)
        {
            GameController.game.Exit();

            base.OnMouseLeftClick(e);
        }
    }
}
