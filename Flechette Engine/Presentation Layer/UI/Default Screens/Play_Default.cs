﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Flechette_Engine
{
    public class Play_Default : Screen
    {
        public Play_Default()
            : base()
        {
            using (BackScreenButton item = new BackScreenButton(this, null, drawLayers["GUI"], defaultManager))
            {
                item.Position = new Vector2(10, 10);
                item.Size = new Vector2(100, 24);
                item.Text = "Back";
            };
        }
    }
}
