﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Flechette_Engine
{
    public class Settings_Default : Screen
    {
        //public ComboBox combobox;
        //public Label fpsLabel;

        public Settings_Default()
            : base()
        {
            using (BackScreenButton item = new BackScreenButton(this, null, drawLayers["GUI"], defaultManager))
            {
                item.Position = new Vector2(10, 10);
                item.Size = new Vector2(100, 24);
                item.Text = "Back";
            };

            //TabControl tc = new TabControl(uiManager);
            //tc.Top = 50;
            //tc.Width = 500;
            //tc.Height = 500;
            //tc.Parent = defaultWindow;
            //tc.AddPage("Graphics");
            //tc.AddPage("Audio");
            //tc.AddPage("Controls");

            //fpsLabel = new Label(uiManager);
            //fpsLabel.Parent = defaultWindow;
            //fpsLabel.Top = 10;
            //fpsLabel.Left = 10;
            //fpsLabel.Width = 200;
            //tc.TabPages[0].Add(fpsLabel);

            //Label lbl = new Label(uiManager);
            //lbl.Top = 50;
            //lbl.Left = 10;
            //lbl.Width = 100;
            //lbl.Text = "Resolution:";
            //lbl.Parent = defaultWindow;
            //tc.TabPages[0].Add(lbl);

            //combobox = new ComboBox(uiManager);
            //foreach (string res in DisplayCalculations.SupportedResolutions.Keys)
            //{
            //    combobox.Items.Add(res);
            //}
            //combobox.ItemIndex = combobox.Items.IndexOf(combobox.Items.First(e => (string)e ==
            //    GameController.graphics.PreferredBackBufferWidth + "x" + GameController.graphics.PreferredBackBufferHeight));
            //combobox.Width = 100;
            //combobox.Height = 20;
            //combobox.Top = 50;
            //combobox.Left = 120;
            //combobox.Color = Color.Red;
            //combobox.TextColor = Color.White;
            //combobox.Anchor = Anchors.Top;
            //combobox.Parent = defaultWindow;
            //tc.TabPages[0].Add(combobox);

            //lbl = new Label(uiManager);
            //lbl.Top = 80;
            //lbl.Left = 10;
            //lbl.Width = 100;
            //lbl.Text = "Full Screen:";
            //lbl.Parent = defaultWindow;
            //tc.TabPages[0].Add(lbl);

            //CheckBox chbx = new CheckBox(uiManager);
            //chbx.Name = "Fullscreen";
            //chbx.Left = 120;
            //chbx.Top = 80;
            //chbx.Text = "";
            //tc.TabPages[0].Add(chbx);

            //Button applyButton = new TomShane.Neoforce.Controls.Button(uiManager);
            //applyButton.Text = "Apply";
            //applyButton.Width = 100;
            //applyButton.Height = 20;
            //applyButton.Left = tc.Width - applyButton.Width - 10;
            //applyButton.Top = tc.Height - applyButton.Height - 30;
            //applyButton.Anchor = Anchors.Bottom;
            //applyButton.Parent = defaultWindow;
            //applyButton.Click += applyButton_Click;
            //tc.TabPages[0].Add(applyButton);
        }

        //void applyButton_Click(object sender, TomShane.Neoforce.Controls.EventArgs e)
        //{
        //    Resolution dims = DisplayCalculations.ResolutionStringToInts(combobox.Text);
        //    GameController.graphics.IsFullScreen = ((CheckBox)defaultWindow.GetControl("Fullscreen")).Checked;

        //    DisplayMechanics.ChangeResolution(dims.Width, dims.Height);
        //}

        public override void Update()
        {
            //fpsLabel.Text = "Current FPS: " + UIController.FrameRate.ToString();

            base.Update();
        }
    }
}
