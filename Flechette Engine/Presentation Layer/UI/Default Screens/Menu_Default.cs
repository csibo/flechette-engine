﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Flechette_Engine
{
    public class Menu_Default : Screen
    {
        public Menu_Default()
            : base()
        {
            using (ToScreenButton item = new ToScreenButton(this, null, drawLayers["GUI"], defaultManager))
            {
                item.Position = new Vector2(10, 10);
                item.targetScreen = "Play";
                item.Text = "Play";
                item.ID = "PlayButton";
            };

            using (ToScreenButton item = new ToScreenButton(this, null, drawLayers["GUI"], defaultManager))
            {
                item.Position = new Vector2(10, 50);
                item.targetScreen = "Settings Menu";
                item.Text = "Settings";
                item.ID = "SettingsButton";
            };

            using (QuitGameButton item = new QuitGameButton(this, null, drawLayers["GUI"], defaultManager))
            {
                item.Position = new Vector2(10, 90);
                item.ID = "QuitButton";
            };
        }
    }
}
