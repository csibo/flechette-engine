﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Flechette_Engine
{
    /// <summary>
    /// Represents a collection of Players.
    /// </summary>
    public class Team
    {
        /// <summary>
        /// Team's name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Team's avatar.
        /// </summary>
        public Texture2D Avatar { get; set; }

        /// <summary>
        /// Constituent players.
        /// </summary>
        public List<Player> Members { get; set; }

        /// <summary>
        /// Team's color for whatever use.
        /// </summary>
        public Color Color { get; set; }

        /// <summary>
        /// Custom data for any use.
        /// </summary>
        public object UserData { get; set; }

        /// <summary>
        /// Adds itself to the controller's team list.
        /// </summary>
        public Team()
        {
            Members = new List<Player>();

            Color = Color.Blue;

            PlayersController.Teams.Add(this);
        }

        /// <summary>
        /// Disposed of the avatar and removes itself from the controller's 
        /// lists and queues.
        /// </summary>
        public virtual void Dispose()
        {
            if (Avatar != null)
                Avatar.Dispose();

            if (PlayersController.Teams.Contains(this))
                PlayersController.Teams.Remove(this);
        }

        public virtual void Update()
        {

        }

        public virtual void Draw()
        {

        }
    }
}
