﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Flechette_Engine
{
    /// <summary>
    /// Represents a single player entity in the game, human or AI.
    /// </summary>
    public class Player : IDisposable
    {
        /// <summary>
        /// Player's name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Player's avatar.
        /// </summary>
        public Texture2D Avatar { get; set; }

        /// <summary>
        /// Custom data for any use.
        /// </summary>
        public object UserData { get; set; }

        /// <summary>
        /// Other players forcibly marked as enemies.
        /// </summary>
        public List<Player> EnemyPlayers { get; set; }

        /// <summary>
        /// Other players forcibly marked as allies.
        /// </summary>
        public List<Player> AllyPlayers { get; set; }

        /// <summary>
        /// Other teams forcibly marked as enemies.
        /// </summary>
        public List<Team> EnemyTeams { get; set; }

        /// <summary>
        /// Other teams forcibly marked as allies.
        /// </summary>
        public List<Team> AllyTeams { get; set; }

        /// <summary>
        /// Camera to associate with this player for whatever purpose.
        /// </summary>
        public Camera Camera { get; set; }

        /// <summary>
        /// Color to associate with this player for whatever purpose.
        /// </summary>
        public Color Color { get; set; }

        /// <summary>
        /// True to mark this as an AI player.
        /// </summary>
        public bool IsAI { get; set; }

        /// <summary>
        /// Adds itself to the controller's player list.
        /// </summary>
        public Player()
        {
            EnemyPlayers = new List<Player>();
            AllyPlayers = new List<Player>();
            EnemyTeams = new List<Team>();
            AllyTeams = new List<Team>();

            Color = ColorUtils.GenerateRandomColor(Color.White, Color.DarkGray, Color.Gray);

            IsAI = false;

            PlayersController.Players.Add(this);
        }

        /// <summary>
        /// Dispose of the avatar and remove this player from the controller's
        /// lists and queues.
        /// </summary>
        public virtual void Dispose()
        {
            if(Avatar != null)
                Avatar.Dispose();

            if (PlayersController.Players.Contains(this))
                PlayersController.Players.Remove(this);

            if (PlayersController.PlayerQueue.Contains(this))
            {
                List<Player> temp = PlayersController.PlayerQueue.ToList();
                temp.Remove(this);

                PlayersController.PlayerQueue.Clear();

                foreach(Player pl in temp)
                {
                    PlayersController.PlayerQueue.Enqueue(pl);
                }
            }
        }

        public virtual void Update()
        {

        }

        public virtual void Draw()
        {

        }
    }
}
