﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Flechette_Engine
{
    /// <summary>
    /// Camera used to create a specific view of the game world.
    /// </summary>
    public class Camera : GameObject
    {
        /// <summary>
        /// Shake defaults.
        /// </summary>
        public enum ShakeProfileDefaults
        {
            None,
            Lite,
            Medium,
            Heavy,
            Shellshock,
        }

        /// <summary>
        /// Settings profile used for shake effects.
        /// </summary>
        public class ShakeSettings
        {
            /// <summary>
            /// Max displacement in each direction during a shake.
            /// Default = Vector4.Zero
            /// </summary>
            public Vector4 ShakeAmount { get; set; }

            /// <summary>
            /// Min and Max shake interval.
            /// Default = Vector2.Zero
            /// </summary>
            public Vector2 ShakeFrequency { get; set; }

            /// <summary>
            /// Time since the shake was last used.
            /// </summary>
            public double TimeSinceShake { get; set; }
        }

        /// <summary>
        /// Default shake profiles.
        /// </summary>
        public static readonly Dictionary<ShakeProfileDefaults, ShakeSettings> ShakeDefaults = new Dictionary<ShakeProfileDefaults,ShakeSettings>()
        {
            { ShakeProfileDefaults.None, new ShakeSettings()
                { 
                    ShakeAmount = Vector4.Zero,
                    ShakeFrequency = Vector2.Zero,
                }
            },
            { ShakeProfileDefaults.Lite, new ShakeSettings()
                { 
                    ShakeAmount = new Vector4(0, 0, 10, 10),
                    ShakeFrequency = new Vector2(10, 20),
                }
            },
            { ShakeProfileDefaults.Medium, new ShakeSettings()
                { 
                    ShakeAmount = new Vector4(0, 0, 15, 15),
                    ShakeFrequency = new Vector2(5, 8),
                }
            },
            { ShakeProfileDefaults.Heavy, new ShakeSettings()
                { 
                    ShakeAmount = new Vector4(0, 0, 25, 25),
                    ShakeFrequency = new Vector2(1, 1),
                }
            },
            { ShakeProfileDefaults.Shellshock, new ShakeSettings()
                { 
                    ShakeAmount = new Vector4(150, 150, 200, 200),
                    ShakeFrequency = new Vector2(100, 400),
                }
            },
        };

        /// <summary>
        /// The camera's transform to pass into the render batch.
        /// </summary>
        public Matrix ViewMatrix { get; set; }

        public Matrix ProjectionMatrix { get; set; }

        public Matrix WorldMatrix { get; set; }

        /// <summary>
        /// Relative center based on the window size and camera zoom.
        /// </summary>
        public Vector2 Origin { get; set; }

        /// <summary>
        /// Center of the game's original view port.
        /// </summary>
        public Vector2 ScreenCenter { get; set; }

        /// <summary>
        /// Focal object which the camera should follow.
        /// </summary>
        public GameObject Subject { get; set; }

        /// <summary>
        /// True to match subject's rotation.
        /// </summary>
        public bool SyncSubjectRotation { get; set; }

        /// <summary>
        /// True to match subkect's position.
        /// </summary>
        public bool SyncSubjectPosition { get; set; }

        /// <summary>
        /// Translation speed for the camera to follow the subject.
        /// </summary>
        public float MoveSpeed { get; set; }

        /// <summary>
        /// Zoom factor of the camera.
        /// Default = 1
        /// </summary>
        public float Zoom { get; set; }

        /// <summary>
        /// Current shake profile.
        /// </summary>
        public ShakeSettings ShakeSetting { get; set; }

        /// <summary>
        /// Overlay color for the scene.
        /// </summary>
        public Color Color { get; set; }

        /// <summary>
        /// Alpha for the overlay color of the scene.
        /// </summary>
        public float Alpha { get; set; }

        /// <summary>
        /// Background color for the scene.
        /// </summary>
        public Color BackColor { get; set; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="containingScreen">Screen to which this belongs.</param>
        public Camera(Screen containingScreen)
            : base(containingScreen)
        {
            Zoom = 1;
            MoveSpeed = 1.25f;
            ShakeSetting = ShakeDefaults[ShakeProfileDefaults.None];
            Color = Color.Transparent;
            Alpha = 0f;
            BackColor = Color.Black;
            SyncSubjectRotation = false;
            SyncSubjectRotation = true;

            Size = new Vector2(1, 1);

            DrawLayer = containingScreen.drawLayers["GUI"];
            containingScreen.drawLayers["GUI"].members.Add(this);

            DrawOrder = 0f;

            WorldMatrix = Matrix.Identity;

            SetMatrices();

            Position = ScreenCenter;

            AddChild<AudioListener>();
        }

        protected virtual void SetMatrices()
        {
            // Original center of the view port.
            ScreenCenter = new Vector2(Settings.DisplayResolution.X / 2f, Settings.DisplayResolution.Y / 2f);

            ProjectionMatrix = Matrix.CreateOrthographicOffCenter(
                0,
                Settings.DisplayResolution.X,
                Settings.DisplayResolution.Y,
                0,
                0,
                1);
        }

        /// <summary>
        /// Update my position, rotation, parallax, and cinematic effects.
        /// </summary>
        public override void Update()
        {
            // Make sure our zoom is valid
            ValidateZoom();

            // Recalculate the new screen center after zooming
            Origin = ScreenCenter / Zoom;

            // Get the new position based on the subject Object, if any
            LookAt(Subject);

            // Apply shake effects, if any
            Vector2 shakeDelta = GetShake();

            Vector3 screenScalingFactor;
            if (Settings.ResolutionIndependant)
            {
                screenScalingFactor = Geometry.Vector2To3(Settings.DisplayResolution / Settings.RenderResolution);
                screenScalingFactor.Z = 1;
            }
            else
            {
                screenScalingFactor = new Vector3(1, 1, 1);
            }

            // Create the Transform used by a spritebatch process
            ViewMatrix = Matrix.Identity
                        * Matrix.CreateTranslation(-(Position.X + shakeDelta.X), -(Position.Y + shakeDelta.Y), 0)
                        * Matrix.CreateRotationZ(Rotation)
                        * Matrix.CreateTranslation(Geometry.Vector2To3(Origin))
                        * Matrix.CreateScale(Zoom)
                        * Matrix.CreateScale(screenScalingFactor)
                        ;

            base.Update();
        }

        /// <summary>
        /// Apply overlays and cinematic effects.
        /// </summary>
        public override void Draw()
        {
            if (Visible)
            {
                // Draw color overlay
                ContainingScreen.spriteBatch.Draw(
                    GraphicsDefaults.WhitePixel,
                    GetRectangle(), 
                    new Rectangle(0, 0, 1, 1), 
                    Color * Alpha,
                    0,
                    Vector2.Zero,
                    SpriteEffects.None,
                    DrawOrder);

                //Primitives.DrawSolidRectangle(GetRectangle(), Color * 0.2f, true);
            }

            base.Draw();
        }

        /// <summary>
        /// Make sure the zoom setting is valid.
        /// </summary>
        protected void ValidateZoom()
        {
            // Keep the zoom from being too small
            Zoom = System.Math.Max(Zoom, 0.001f);
        }

        /// <summary>
        /// Match a subject's settings.
        /// </summary>
        /// <param name="subject">Subject in question.</param>
        public void LookAt(GameObject subject)
        {
            if (subject != null)
            {
                // If we are to match its position...
                if (SyncSubjectPosition)
                {
                    // Use the position-based LookAt()
                    LookAt(Project(subject.Position, subject.DrawLayer.ParallaxMatrix));
                }

                // If we are to match its rotation...
                if(SyncSubjectRotation)
                {
                    // Invert the angle
                    Rotation = -subject.Rotation;
                }
            }
        }

        /// <summary>
        /// Translate to view a given location.
        /// </summary>
        /// <param name="point">Location of interest.</param>
        public void LookAt(Vector2 point)
        {
            if (point != null)
                // Lerp towards the point based on our max move speed
                Position += (point - Position) * MoveSpeed * (float)GameController.gameTime.ElapsedGameTime.TotalSeconds;
        }

        /// <summary>
        /// Apply shake settings.
        /// </summary>
        /// <returns>Shake offset.</returns>
        protected Vector2 GetShake()
        {
            bool useShake = false;

            // If we want shake at all
            if (ShakeSetting.ShakeFrequency.X > 0
                && ShakeSetting.ShakeFrequency.Y >= ShakeSetting.ShakeFrequency.X)
            {
                // If our elapsed time is greater than our minimum time
                if (ShakeSetting.TimeSinceShake > ShakeSetting.ShakeFrequency.X)
                {
                    // If our elapsed time is greater than our maximum time
                    if (ShakeSetting.TimeSinceShake > ShakeSetting.ShakeFrequency.Y)
                    {
                        useShake = true;
                    }
                    else
                    {
                        // Roll to see if we should shake or not
                        if(MathHelper.GlobalRandom.NextDouble() < 0.5f)
                        {
                            useShake = true;
                        }
                    }
                }

                if (useShake)
                {
                    ShakeSetting.TimeSinceShake = 0;

                    float xDiff = (float)MathHelper.GlobalRandom.NextDouble() * ShakeSetting.ShakeAmount.Z;
                    float yDiff = (float)MathHelper.GlobalRandom.NextDouble() * ShakeSetting.ShakeAmount.W;
                    xDiff = MathHelper.GlobalRandom.Next(-1, 2) * System.Math.Max(xDiff, ShakeSetting.ShakeAmount.X);
                    yDiff = MathHelper.GlobalRandom.Next(-1, 2) * System.Math.Max(yDiff, ShakeSetting.ShakeAmount.Y);

                    return new Vector2(xDiff,yDiff);
                }

                // Get the new time since last shake
                ShakeSetting.TimeSinceShake += GameController.gameTime.ElapsedGameTime.TotalMilliseconds;
            }

            return Vector2.Zero;
        }

        /// <summary>
        /// Get view rectangle.
        /// </summary>
        /// <returns>Rectangle of current view.</returns>
        public override Rectangle GetRectangle()
        {
            //transform four corners
            Vector2 topLeftCorner = Vector2.Transform(Vector2.Zero, Matrix.Invert(ViewMatrix));
            Vector2 bottomRightCorner = Vector2.Transform(new Vector2(GameController.game.GraphicsDevice.Viewport.Width, GameController.game.GraphicsDevice.Viewport.Height), Matrix.Invert(ViewMatrix));

            topLeftCorner = Vector2.Transform(topLeftCorner, ContainingScreen.activeCamera.ViewMatrix);
            bottomRightCorner = Vector2.Transform(bottomRightCorner, ContainingScreen.activeCamera.ViewMatrix);

            return new Rectangle(
                (int)(topLeftCorner.X),
                (int)(topLeftCorner.Y),
                (int)(bottomRightCorner.X - topLeftCorner.X),
                (int)(bottomRightCorner.Y - topLeftCorner.Y)
                );
        }

        /// <summary>
        /// Converts screen space point to world space point.
        /// </summary>
        /// <param name="screenLoc">Screen space point</param>
        /// <param name="parallax">Parallax matrix of the DrawLayer where the world space point resides.</param>
        /// <returns>World space point</returns>
        public virtual Vector2 Unproject(Vector2 screenLoc, Matrix parallax)
        {
            Matrix adjustedView = (ContainingScreen.UseParallax ? ViewMatrix * parallax : ViewMatrix);

            return Geometry.Vector3To2(
                GameController.game.GraphicsDevice.Viewport.Unproject(
                    Geometry.Vector2To3(screenLoc),
                    ProjectionMatrix,
                    adjustedView,
                    WorldMatrix)
                );
        }

        /// <summary>
        /// Converts screen space point to world space point.
        /// </summary>
        /// <param name="screenLoc">Screen space point</param>
        /// <returns>World space point</returns>
        public virtual Vector2 Unproject(Vector2 screenLoc)
        {
            return Unproject(screenLoc, Matrix.Identity);
        }

        /// <summary>
        /// Converts world space point to screen space point.
        /// </summary>
        /// <param name="worldLoc">World space point</param>
        /// <param name="parallax">Parallax matrix of the DrawLayer where the world space point resides.</param>
        /// <returns>Screen space point</returns>
        public virtual Vector2 Project(Vector2 worldLoc, Matrix parallax)
        {
            Matrix adjustedView = (ContainingScreen.UseParallax ? ViewMatrix * Matrix.Invert(parallax) : ViewMatrix);

            return Geometry.Vector3To2(
                GameController.game.GraphicsDevice.Viewport.Project(
                    Geometry.Vector2To3(worldLoc),
                    ProjectionMatrix,
                    adjustedView,
                    WorldMatrix)
                );
        }

        /// <summary>
        /// Converts world space point to screen space point.
        /// </summary>
        /// <param name="worldLoc">World space point</param>
        /// <returns>Screen space point</returns>
        public virtual Vector2 Project(Vector2 worldLoc)
        {
            return Project(worldLoc, Matrix.Identity);
        }

        public override void Window_ClientSizeChanged(object sender, System.EventArgs e)
        {
            SetMatrices();
        }
    }
}
