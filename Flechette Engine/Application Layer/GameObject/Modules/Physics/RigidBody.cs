﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using FarseerPhysics;
using FarseerPhysics.Common;
using FarseerPhysics.Common.PolygonManipulation;
using FarseerPhysics.Common.Decomposition;
using FarseerPhysics.Collision;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;

namespace Flechette_Engine
{
    public class RigidBody : GameObject
    {
        public Body body;

        public World world;

        public RigidBody(Screen containingScreen, GameObject parent, DrawLayer drawLayer)
            : base(containingScreen, parent, drawLayer)
        {
            world = ContainingScreen.physicsController.world;

            SyncOrder = SyncPresets[SyncLayers.Physics];
            SyncType = SyncTypes.SyncBoth;

            body = BodyFactory.CreateBody(world, 
                new Vector2(ConvertUnits.ToSimUnits(Position.X), ConvertUnits.ToSimUnits(Position.Y)), Rotation);

            body.BodyType = BodyType.Dynamic;

            Reset();
        }

        public override void UpdatePrepass()
        {
            base.UpdatePrepass();

            if (Enabled)
            {
                lock (body)
                {
                    Position = ConvertUnits.ToDisplayUnits(body.Position);
                    Rotation = body.Rotation;
                }
            }
        }

        public override void FullUpdate()
        {
            body.Enabled = Enabled;

            base.FullUpdate();
        }

        public override void UpdatePostpass()
        {
            base.UpdatePostpass();

            if (Enabled)
            {
                //body.Position = ConvertUnits.ToSimUnits(Position);
                //body.Rotation = Rotation;
            }
        }

        public void Reset()
        {
            body.Restitution = 0.35f;
            body.Friction = 1f;
            body.LinearDamping = 0f;
            body.AngularDamping = 0f;
            body.CollisionCategories = Category.All;
            body.CollidesWith = Category.All;
            body.ResetDynamics();
        }

        #region Factories

        //public void CreateBoundingCircle()
        //{
        //    body.Dispose();
        //    body = BodyFactory.CreateCircle(world, ConvertUnits.ToSimUnits(Parent.DrawRectangle.Width / 2f), 1f,
        //        body.Position);
        //    body.BodyType = BodyType.Dynamic;

        //    Reset();
        //}

        public void CreateBoundingRectangle()
        {
            body.Dispose();

            //body = BodyFactory.CreateRectangle(
            //    world, 
            //    ConvertUnits.ToSimUnits(Size.X),
            //    ConvertUnits.ToSimUnits(Size.Y),
            //    ConvertUnits.ToSimUnits(100f), 
            //    body.Position);

            Vertices box = PolygonTools.CreateRectangle(ConvertUnits.ToSimUnits(Size.X / 2f), ConvertUnits.ToSimUnits(Size.Y / 2f));
            PolygonShape shape = new PolygonShape(box, 5);
            body = BodyFactory.CreateBody(world);
            body.BodyType = BodyType.Dynamic;

            body.CreateFixture(shape);
            body.Position = ConvertUnits.ToSimUnits(Position);

            Reset();
        }

        //public void CreateBoundingTexture()
        //{
        //    body.Dispose();

        //    //load texture that will represent the physics body
        //    Texture2D polygonTexture = containingSprite.SourceImage;
        //    uint[] data = new uint[polygonTexture.Width * polygonTexture.Height];
        //    polygonTexture.GetData(data);

        //    //Find the vertices that makes up the outline of the shape in the texture
        //    Vertices textureVertices = PolygonTools.CreatePolygon(data, polygonTexture.Width, true);

        //    //The tool returns vertices as they were found in the texture.
        //    //We need to find the real center (centroid) of the vertices for 2 reasons:

        //    //1. To translate the vertices so the polygon is centered around the centroid.
        //    Vector2 centroid = -textureVertices.GetCentroid();
        //    textureVertices.Translate(ref centroid);

        //    //2. To draw the texture the correct place.
        //    //origin = -centroid;

        //    //We simplify the vertices found in the texture.
        //    textureVertices = SimplifyTools.ReduceByDistance(textureVertices, 4f);

        //    //Since it is a concave polygon, we need to partition it into several smaller convex polygons
        //    List<Vertices> list = Triangulate.ConvexPartition(textureVertices, TriangulationAlgorithm.Earclip);

        //    //scale the vertices from graphics space to sim space
        //    Vector2 vertScale = new Vector2(ConvertUnits.ToSimUnits(1));
        //    foreach (Vertices vertices in list)
        //    {
        //        vertices.Scale(ref vertScale);
        //    }

        //    //Create a single body with multiple fixtures
        //    body = BodyFactory.CreateCompoundPolygon(world, list, 1f,
        //        body.Position, BodyType.Dynamic);

        //    Reset();
        //}

        #endregion
    }
}
