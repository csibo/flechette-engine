﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace Flechette_Engine
{
    public class AudioSource : GameObject
    {
        private SoundEffect sound;

        public SoundEffect Sound { get { return sound; } set { sound = value; CreateInstance(); } }

        public SoundEffectInstance Instance { get; set; }

        public AudioEmitter Emitter { get; set; }

        public AudioSource(Screen containingScreen, GameObject parent = null, DrawLayer drawLayer = null)
            : base(containingScreen, parent, drawLayer)
        {
            Emitter = new AudioEmitter();
        }

        public override void Update()
        {
            Emitter.Position = Geometry.Vector2To3(Position);

            if(Instance != null)
                Instance.Apply3D(ContainingScreen.activeCamera.GetChild<AudioListener>().Listener, Emitter);

            base.Update();
        }

        public override void Draw()
        {
            base.Draw();
        }

        public void SetSound(string filename)
        {
            try
            {
                Sound = GameController.game.Content.Load<SoundEffect>(filename);
                SoundEffect.DistanceScale = 100;
            }
            catch(Exception e)
            {
                Sound = null;
            }
        }

        protected void CreateInstance()
        {
            if (Sound != null)
            {
                SoundEffectInstance prev = null;
                if(Instance != null)
                {
                    prev = Instance;
                }

                Instance = Sound.CreateInstance();
                Instance.Apply3D(ContainingScreen.activeCamera.GetChild<AudioListener>().Listener, Emitter);

                if (prev != null)
                {
                    Instance.IsLooped = prev.IsLooped;
                }
            }
        }

        public void Play()
        {
            if(Sound != null)
            {
                if (Instance == null)
                {
                    CreateInstance();
                }

                Instance.Play();
            }
        }
    }
}
