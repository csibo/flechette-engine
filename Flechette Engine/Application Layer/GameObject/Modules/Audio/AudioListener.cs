﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace Flechette_Engine
{
    public class AudioListener : GameObject
    {
        public Microsoft.Xna.Framework.Audio.AudioListener Listener { get; set; }

        public AudioListener(Screen containingScreen, GameObject parent = null, DrawLayer drawLayer = null)
            : base(containingScreen, parent, drawLayer)
        {
            ParentExceptions.ExceptionRules.Remove(ParentExceptions.ExceptionRules.First(e => e.Type == typeof(Camera)));

            Listener = new Microsoft.Xna.Framework.Audio.AudioListener();
            Listener.Forward = -Vector3.UnitZ;
        }

        public override void Update()
        {
            Listener.Position = new Vector3(Position.X, Position.Y, 0);
            Listener.Up = new Vector3((float)System.Math.Sin(Rotation), -(float)System.Math.Cos(Rotation), 0);

            base.Update();
        }

        public override void Draw()
        {
            base.Draw();
        }
    }
}
