﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Flechette_Engine
{
    public class Minimap : GameObject
    {
        public List<IGameObject> objects;

        public Camera camera;

        public float Scale { get; set; }

        protected Vector2 AdjustedSize { get; set; }

        public Minimap(Screen containingScreen)
            : base(containingScreen, null, containingScreen.drawLayers["GUI"])
        {
            objects = new List<IGameObject>();

            camera = new Camera(ContainingScreen);

            Scale = 1;
        }

        public override void Update()
        {
            if(Enabled)
            {
                camera.Position = ContainingScreen.activeCamera.Position;
                camera.Zoom = Scale * (Size.X / GameController.game.GraphicsDevice.Viewport.Width);// *ContainingScreen.activeCamera.Zoom;
                AdjustedSize = Size / ContainingScreen.activeCamera.Zoom;
            }

            base.Update();
        }

        public override Rectangle GetRectangle()
        {
            Rectangle temp = base.GetRectangle();

            temp.Width = (int)(AdjustedSize.X);
            temp.Height = (int)(AdjustedSize.Y);

            return temp;
        }

        public override void Draw()
        {
            if (Visible)
            {
                //Primitives.DrawSolidPolygon(
                //    new Vector2[] {
                //        Vector2.Transform(new Vector2(GetRectangle().X, GetRectangle().Y), ContainingScreen.activeCamera.ViewMatrix),
                //        Vector2.Transform(new Vector2(GetRectangle().X + AdjustedSize.X, GetRectangle().Y), ContainingScreen.activeCamera.ViewMatrix),
                //        Vector2.Transform(new Vector2(GetRectangle().X + AdjustedSize.X, GetRectangle().Y + AdjustedSize.Y), ContainingScreen.activeCamera.ViewMatrix),
                //        Vector2.Transform(new Vector2(GetRectangle().X, GetRectangle().Y + AdjustedSize.Y), ContainingScreen.activeCamera.ViewMatrix),
                //    },
                //    Color.White, true);

                Primitives.DrawSolidRectangle(GetRectangle(), Color.White, true);

                if(objects != null)
                {
                    Camera temp = ContainingScreen.activeCamera;
                    ContainingScreen.activeCamera = camera;

                    foreach(IGameObject obj in objects)
                    {
                        Vector2 adjustedPosition = Vector2.Transform(obj.Position, camera.ViewMatrix) + (Position / 2.3f);

                        //if (Contains(adjustedPosition))
                        {
                            Primitives.DrawPoint(
                                adjustedPosition,
                                obj.Size.X * camera.Zoom,
                                obj.Color);
                        }
                    }

                    ContainingScreen.activeCamera = temp;
                }
            }

            base.Draw();
        }
    }
}
