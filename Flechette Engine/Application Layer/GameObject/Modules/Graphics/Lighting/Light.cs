﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Krypton;
using Krypton.Common;
using Krypton.Lights;

namespace Flechette_Engine
{
    public class Light : GameObject, ILight2D
    {
        private bool mIsOn = true;
        private Texture2D mTexture = null;
        private float mRange = 1;
        private float mFov = (float)MathHelper.TwoPI;
        private float mIntensity = 1;
        private ShadowType mShadowType = ShadowType.Solid;

        #region Parameters

        /// <summary>
        /// The X coordinate of the light's position
        /// </summary>
        public float X
        {
            get { return this.Position.X; }
            set { this.Position = new Vector2(value, Position.Y); }
        }

        /// <summary>
        /// The Y coordinate of the light's position
        /// </summary>
        public float Y
        {
            get { return this.Position.Y; }
            set { this.Position = new Vector2(Position.X, value); }
        }

        /// <summary>
        /// The texture used as the base light map, from which shadows will be subtracted
        /// </summary>
        public Texture2D Texture
        {
            get { return this.mTexture; }
            set { this.mTexture = value; }
        }

        /// <summary>
        /// The light's maximum radius (or "half width", if you will)
        /// </summary>
        public float Range
        {
            get { return this.mRange; }
            set { this.mRange = value; }
        }

        /// <summary>
        /// Gets or sets the light's field of view. This value determines the angles at which the light will cease to draw
        /// </summary>
        public float Fov
        {
            get { return this.mFov; }
            set
            {
                this.mFov = MathHelper.Clamp(value, 0, (float)MathHelper.TwoPI);
            }
        }

        /// <summary>
        /// Gets or sets the light's intensity. Think pixel = (tex * color) ^ (1 / intensity)
        /// </summary>
        public float Intensity
        {
            get { return this.mIntensity; }
            set { this.mIntensity = MathHelper.Clamp(value, 0.01f, 3f); }
        }

        /// <summary>
        /// Gets or sets a value indicating what type of shadows this light should cast
        /// </summary>
        public ShadowType ShadowType
        {
            get { return this.mShadowType; }
            set { this.mShadowType = value; }
        }

        /// <summary>
        /// Categories of hulls which will cast shadows from this light.
        /// Leave empty for "All Hulls".
        /// </summary>
        public List<string> HullCategories { get; set; }

        #endregion Parameters

        public Light(Screen containingScreen, GameObject parent = null, DrawLayer drawLayer = null)
            : base(containingScreen, parent, drawLayer)
        {
            SyncOrder = SyncPresets[SyncLayers.Graphics];
            SyncType = SyncTypes.SyncThisToParent;

            ContainingScreen.lightingController.Add(this);

            HullCategories = new List<string>();
        }

        public override void Update()
        {
            if (Enabled)
            {
            }

            base.Update();
        }

        public override void Draw()
        {
            if (Visible)
            {
            }

            base.Draw();
        }

        /// <summary>
        /// Gets or sets a value indicating weither or not to draw the light
        /// </summary>
        public bool IsOn
        {
            get { return this.mIsOn; }
            set { this.mIsOn = value; }
        }

        /// <summary>
        /// Draws shadows from the light's position outward
        /// </summary>
        /// <param name="helper">A render helper for drawing shadows</param>
        /// <param name="hulls">The shadow hulls used to draw shadows</param>
        public void Draw(KryptonRenderHelper helper, List<Krypton.ShadowHull> hulls)
        {
            // Draw the light only if it's on
            if (!this.mIsOn)
                return;

            // Make sure we only render the following hulls
            helper.ShadowHullVertices.Clear();
            helper.ShadowHullIndicies.Clear();

            // Loop through each hull
            foreach (Krypton.ShadowHull hull in hulls)
            {
                if(IgnoreDueToParent(hull))
                    continue;

                if (IgnoreDueToCategory(hull))
                    continue;

                //if(hull.Bounds.Intersects(this.Bounds))
                // Add the hulls to the buffer only if they are within the light's range
                if (hull.Visible && Light.IsInRange(hull.Position - this.Position, hull.MaxRadius * Math.Max(hull.Scale.X, hull.Scale.Y) + this.Range))
                {
                    helper.BufferAddShadowHull(hull);
                }
            }

            // Set the effect and parameters
            helper.Effect.Parameters["LightPosition"].SetValue(this.Position);
            helper.Effect.Parameters["Texture0"].SetValue(this.mTexture);
            helper.Effect.Parameters["LightIntensityFactor"].SetValue(1 / (this.mIntensity * this.mIntensity));


            switch (this.mShadowType)
            {
                case (ShadowType.Solid):
                    helper.Effect.CurrentTechnique = helper.Effect.Techniques["PointLight_Shadow_Solid"];
                    break;

                case (ShadowType.Illuminated):
                    helper.Effect.CurrentTechnique = helper.Effect.Techniques["PointLight_Shadow_Illuminated"];
                    break;

                case (ShadowType.Occluded):
                    helper.Effect.CurrentTechnique = helper.Effect.Techniques["PointLight_Shadow_Occluded"];
                    break;

                default:
                    throw new NotImplementedException("Shadow Type does not exist: " + this.mShadowType);
            }

            foreach (var pass in helper.Effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                helper.BufferDraw();
            }

            helper.Effect.CurrentTechnique = helper.Effect.Techniques["PointLight_Light"];
            foreach (var pass in helper.Effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                helper.DrawClippedFov(this.Position, this.Rotation, this.mRange * 2, this.Color, this.mFov);
            }

            helper.Effect.CurrentTechnique = helper.Effect.Techniques["ClearTarget_Alpha"];
            foreach (var pass in helper.Effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                helper.GraphicsDevice.DrawUserPrimitives<VertexPositionTexture>(PrimitiveType.TriangleStrip, KryptonRenderHelper.UnitQuad, 0, 2);
            }
        }

        private bool IgnoreDueToParent(Krypton.ShadowHull hull)
        {
            if (Parent != null)
            {
                ShadowHull parentHull = Parent.GetChild<ShadowHull>();

                if (parentHull == null
                    && Parent.GetType() == typeof(ShadowHull))
                {
                    parentHull = (ShadowHull)Parent;
                }

                if (parentHull != null)
                {
                    if (parentHull.Hull == hull)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IgnoreDueToCategory(Krypton.ShadowHull hull)
        {
            if (HullCategories != null
                && HullCategories.Count() > 0)
            {
                ShadowHull myHull = null;
                if (ContainingScreen.lightingController.Hulls != null
                    && ContainingScreen.lightingController.Hulls.ContainsKey(hull))
                {
                    myHull = ContainingScreen.lightingController.Hulls[hull];
                }

                if (myHull != null)
                {
                    if (myHull.HullCategory == null)
                    {
                        return true;
                    }

                    if (!HullCategories.Contains(myHull.HullCategory))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Determines if a vector's length is less than a specified value
        /// </summary>
        /// <param name="offset">Offset</param>
        /// <param name="dist">Distance</param>
        /// <returns></returns>
        private static bool IsInRange(Vector2 offset, float dist)
        {
            // a^2 + b^2 < c^2 ?
            return offset.X * offset.X + offset.Y * offset.Y < dist * dist;
        }

        /// <summary>
        /// Gets the world-space bounds which contain the light
        /// </summary>
        public BoundingRect Bounds
        {
            get
            {
                BoundingRect rect;

                rect.Min.X = this.Position.X - this.mRange;
                rect.Min.Y = this.Position.Y - this.mRange;
                rect.Max.X = this.Position.X + this.mRange;
                rect.Max.Y = this.Position.Y + this.mRange;

                return rect;
            }
        }
    }
}
