﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Krypton;
using Krypton.Common;
using Krypton.Lights;

namespace Flechette_Engine
{
    public class ShadowHull : GameObject
    {
        public Krypton.ShadowHull Hull { get; set; }

        /// <summary>
        /// Category to use for shadow casting.
        /// Value here must be in the light's HullCategories list to cast shadows.
        /// Lights with empty catagory lists will allow all hulls to cast shadows.
        /// </summary>
        public string HullCategory { get; set; }

        public ShadowHull(Screen containingScreen, GameObject parent = null, DrawLayer drawLayer = null)
            : base(containingScreen, parent, drawLayer)
        {
            SyncOrder = SyncPresets[SyncLayers.Graphics];
            SyncType = SyncTypes.SyncThisToParent;

            Hull = Krypton.ShadowHull.CreateRectangle(Vector2.One);
            ContainingScreen.lightingController.Add(this);

            ContainingScreen.lightingController.Hulls.Add(Hull, this);

            HullCategory = null;
        }

        protected override void Dispose(bool disposing)
        {
            ContainingScreen.lightingController.Hulls.Remove(Hull);

            base.Dispose(disposing);
        }

        public override void Update()
        {
            if (Enabled)
            {
                Hull.Position = Position;
                Hull.Scale = Size;
                Hull.Angle = Rotation;
            }

            base.Update();
        }

        public override void Draw()
        {
            if (Visible)
            {
            }

            base.Draw();
        }
    }
}
