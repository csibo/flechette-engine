﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Flechette_Engine
{
    public class Sprite : GameObject
    {
        protected Texture2D texture;

        protected Rectangle sourceRectangle;

        protected Rectangle drawRectangle;

        public Texture2D Texture { get { return texture; } set { texture = value; SetTextureCenter(); } }

        public Rectangle SourceRectangle { get { return sourceRectangle; } set { sourceRectangle = value; SetSourceCenter(); } }

        public Vector2 TextureCenter { get; protected set; }

        public Vector2 SourceCenter { get; protected set; }

        public Vector2 DrawOrigin { get; set; }

        public bool Centered { get; set; }

        public SpriteEffects Effects { get; set; }

        new public readonly bool AllowMultiples = true;

        public Sprite(Screen containingScreen, GameObject parent = null, DrawLayer drawLayer = null)
            : base(containingScreen, parent, drawLayer)
        {
            Rotation = 0;
            DrawOrigin = Vector2.Zero;
            Centered = false;
            Effects = SpriteEffects.None;
            ManuallySized = false;

            SetSourceImage(GraphicsDefaults.DefaultTexture);

            drawRectangle = new Rectangle(0, 0, 0, 0);
            SourceRectangle = new Rectangle(0, 0, 0, 0);

            SyncOrder = SyncPresets[SyncLayers.Graphics];
            SyncType = SyncTypes.SyncThisToParent;
        }

        public override void Update()
        {
            if (Enabled)
            {
                if (Centered)
                {
                    Recenter();
                }

                drawRectangle.X = (int)Position.X;
                drawRectangle.Y = (int)Position.Y;
                drawRectangle.Width = (int)Size.X;
                drawRectangle.Height = (int)Size.Y;
            }

            base.Update();
        }

        public override void Draw()
        {
            if (Visible)
            {
                if(Texture == null)
                    Texture = GraphicsDefaults.DefaultTexture;

                ContainingScreen.spriteBatch.Draw(
                    Texture, 
                    drawRectangle, 
                    SourceRectangle,
                    Color, 
                    Rotation, 
                    DrawOrigin, 
                    Effects, 
                    DrawOrder);
            }

            base.Draw();
        }

        /// <summary>
        /// Sets the texture.
        /// Handles resizing the source and draw rectangles automatically.
        /// </summary>
        /// <param name="filename">Path to image</param>
        public virtual void SetSourceImage(string filename)
        {
            Texture = GraphicsDefaults.DefaultTexture;

            if (filename != null)
            {
                try
                {
                    // Load the image
                    Texture = GameController.game.Content.Load<Texture2D>(filename);
                }
                catch (Exception e)
                {
                    Texture = GraphicsDefaults.DefaultTexture;
                }
            }

            SetSourceImage(Texture);
        }

        /// <summary>
        /// Sets the texture.
        /// Handles resizing the source and draw rectangles automatically.
        /// </summary>
        /// <param name="texture">Texture</param>
        public virtual void SetSourceImage(Texture2D texture)
        {
            if (texture != null)
                Texture = texture;
            else
                Texture = GraphicsDefaults.DefaultTexture;

            SourceRectangle = new Rectangle(0, 0, Texture.Width, Texture.Height);

            // If the user suppied a custom size before, skip automatic resizing
            if (!ManuallySized)
            {
                // Set the draw rectangle to the source size
                Size = new Vector2(Texture.Width, Texture.Height);

                ManuallySized = false;
            }
        }

        protected void Recenter()
        {
            DrawOrigin = SourceCenter;
        }

        protected void SetSourceCenter()
        {
            SourceCenter = new Vector2(sourceRectangle.Width / 2f, sourceRectangle.Height / 2f);
        }

        protected void SetTextureCenter()
        {
            TextureCenter = new Vector2(texture.Width / 2f, texture.Height / 2f);
        }
    }
}
