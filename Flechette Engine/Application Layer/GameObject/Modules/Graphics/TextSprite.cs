﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Flechette_Engine
{
    public class TextSprite : GameObject
    {
        public string Text { get; set; }

        public float Scale { get; set; }

        protected Rectangle drawRectangle;

        public Vector2 DrawOrigin { get; set; }

        public bool Centered { get; set; }

        public SpriteFont Font { get; set; }

        public SpriteEffects Effects { get; set; }

        new public readonly bool AllowMultiples = true;

        public TextSprite(Screen containingScreen, GameObject parent = null, DrawLayer drawLayer = null)
            : base(containingScreen, parent, drawLayer)
        {
            Rotation = 0;
            DrawOrigin = Vector2.Zero;
            Centered = false;
            Effects = SpriteEffects.None;
            ManuallySized = false;
            Scale = 1;
            Color = Color.Black;
            Font = UIController.DefaultFont;

            drawRectangle = new Rectangle(0, 0, 0, 0);

            SyncOrder = SyncPresets[SyncLayers.Graphics];
            SyncType = SyncTypes.SyncThisToParent;
        }

        public override void Update()
        {
            if (Enabled)
            {
                drawRectangle.X = (int)Position.X;
                drawRectangle.Y = (int)Position.Y;
                drawRectangle.Width = (int)Size.X;
                drawRectangle.Height = (int)Size.Y;
            }

            base.Update();
        }

        public override void Draw()
        {
            if (Visible)
            {
                ContainingScreen.spriteBatch.DrawString(
                    Font,
                    Text,
                    Position,
                    Color,
                    Rotation,
                    DrawOrigin,
                    Scale,
                    Effects,
                    DrawOrder);
            }

            base.Draw();
        }
    }
}
