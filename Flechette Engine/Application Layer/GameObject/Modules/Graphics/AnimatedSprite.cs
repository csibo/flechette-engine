﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Flechette_Engine
{
    public class AnimatedSprite : Sprite
    {
        public AnimationUtils.Directions PrimaryAnimationDirection { get; set; }

        public AnimationUtils.Directions SecondaryAnimationDirection { get; set; }

        public int TargetLoopCount { get; set; }

        public int CurrentLoopCount { get; set; }

        public int Columns { get; set; }

        public int Rows { get; set; }

        public double FrameDelay { get; set; }

        private double TimeSinceLastFrame { get; set; }

        public bool IsPlaying { get; protected set; }

        public AnimatedSprite(Screen containingScreen, GameObject parent = null, DrawLayer drawLayer = null)
            : base(containingScreen, parent, drawLayer)
        {
            PrimaryAnimationDirection = AnimationUtils.Directions.None;
            SecondaryAnimationDirection = AnimationUtils.Directions.None;

            TargetLoopCount = -1;
            CurrentLoopCount = 0;

            Rows = 0;
            Columns = 0;

            FrameDelay = 1000;

            IsPlaying = true;
        }

        public override void Update()
        {
            if (Enabled)
            {
                if(!FinishedLooping()
                    && IsPlaying)
                {
                    if (TimeSinceLastFrame >= FrameDelay)
                    {
                        NextFrame();
                        TimeSinceLastFrame = 0;
                    }
                    else
                    {
                        TimeSinceLastFrame += GameController.gameTime.ElapsedGameTime.TotalMilliseconds;
                    }
                }
            }

            base.Update();
        }

        /// <summary>
        /// Sets the texture.
        /// Handles resizing the source and draw rectangles automatically.
        /// </summary>
        /// <param name="filename">Path to image</param>
        public override void SetSourceImage(string filename)
        {
            base.SetSourceImage(filename);

            if (PrimaryAnimationDirection != AnimationUtils.Directions.None)
            {
                // Set the source rectangle size
                SourceRectangle = new Rectangle(
                    0,
                    0,
                    SourceRectangle.Width / Columns,
                    SourceRectangle.Height / Rows);

                // If the user suppied a custom size before, skip automatic resizing
                if (!ManuallySized)
                {
                    // Set the draw rectangle to the source size
                    Size = new Vector2(Size.X / Columns, Size.Y / Rows);

                    ManuallySized = false;
                }
            }
        }

        public bool FinishedLooping()
        {
            if (TargetLoopCount == -1)
            {
                return false;
            }

            if (CurrentLoopCount < TargetLoopCount)
            {
                return false;
            }

            return true;
        }

        #region Frame Controls

        private void StepFrameForwardHorizontal(bool stepInto = false)
        {
            if (sourceRectangle.X + sourceRectangle.Width >= Texture.Width)
            {
                // Reset the column
                sourceRectangle.X = 0;

                if (stepInto)
                {
                    StepFrameForwardVertical();
                }
            }
            else
            {
                // Increment our frame by one column
                sourceRectangle.X += sourceRectangle.Width;
            }
        }

        private void StepFrameForwardVertical(bool stepInto = false)
        {
            // If we are at the end of the column
            if (sourceRectangle.Y + sourceRectangle.Height >= Texture.Height)
            {
                // Reset to the first row
                sourceRectangle.Y = 0;

                if (stepInto)
                {
                    StepFrameForwardHorizontal();
                }
            }
            else
            {
                // Increment our frame by one row
                sourceRectangle.Y += sourceRectangle.Height;
            }
        }

        private void StepFrameBackwardHorizontal(bool stepInto = false)
        {
            if (sourceRectangle.X <= 0)
            {
                // Reset the column
                sourceRectangle.X = Texture.Width - sourceRectangle.Width;

                if (stepInto)
                {
                    StepFrameBackwardVertical();
                }
            }
            else
            {
                // Decrement our frame by one column
                sourceRectangle.X -= sourceRectangle.Width;
            }
        }

        private void StepFrameBackwardVertical(bool stepInto = false)
        {
            // If we are at the front of the column
            if (sourceRectangle.Y <= 0)
            {
                // Reset to the last row
                sourceRectangle.Y = Texture.Height - sourceRectangle.Height;

                if (stepInto)
                {
                    StepFrameBackwardHorizontal();
                }
            }
            else
            {
                // Decrement our frame by one row
                sourceRectangle.Y -= sourceRectangle.Height;
            }
        }

        /// <summary>
        /// Moves to the next frame in the animation.
        /// Will loop to first frame if called on the last frame.
        /// </summary>
        public void NextFrame()
        {
            if (PrimaryAnimationDirection == AnimationUtils.Directions.Horizontal
                || PrimaryAnimationDirection == AnimationUtils.Directions.LeftToRight)
            {
                StepFrameForwardHorizontal(true);
            }
            else if (PrimaryAnimationDirection == AnimationUtils.Directions.RightToLeft)
            {
                StepFrameBackwardHorizontal(true);
            }
            else if (PrimaryAnimationDirection == AnimationUtils.Directions.Vertical
                || PrimaryAnimationDirection == AnimationUtils.Directions.TopToBottom)
            {
                StepFrameForwardVertical(true);
            }
            else if (PrimaryAnimationDirection == AnimationUtils.Directions.BottomToTop)
            {
                StepFrameBackwardVertical(true);
            }
        }

        /// <summary>
        /// Moves to the previous frame in the animation.
        /// Will loop to last frame if called on the first frame.
        /// </summary>
        public void PreviousFrame()
        {
            if (PrimaryAnimationDirection == AnimationUtils.Directions.Horizontal)
            {
                StepFrameBackwardHorizontal(true);
            }
            else if (PrimaryAnimationDirection == AnimationUtils.Directions.Vertical)
            {
                StepFrameBackwardVertical(true);
            }
        }

        /// <summary>
        /// Move to the first frame of the animation.
        /// </summary>
        public void FirstFrame()
        {
            sourceRectangle.X = 0;
            sourceRectangle.Y = 0;
            //currFrame = 0;
        }

        /// <summary>
        /// Move to the last frame of the animation.
        /// </summary>
        public void LastFrame()
        {
            //sourceRectangle.X = sourceImage.Width - sourceRectangle.Width;
            //sourceRectangle.Y = sourceImage.Height - sourceRectangle.Height;
            //currFrame = numFrames - 1;
        }

        #endregion

        public void Play()
        {
            FirstFrame();
            Resume();
        }

        public void Resume()
        {
            IsPlaying = true;
        }

        public void Stop()
        {
            FirstFrame();
            Pause();
        }

        public void Pause()
        {
            IsPlaying = false;
        }
    }
}
