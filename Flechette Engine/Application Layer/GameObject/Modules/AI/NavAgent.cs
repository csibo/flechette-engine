﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

using Microsoft.Xna.Framework;

using FarseerPhysics;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;

namespace Flechette_Engine
{
    public class NavAgent : GameObject
    {
        public enum eNavModes
        {
            None,
            Seek,
            Flee,
            Pursue,
            Evade,
            Wander,
        }

        #region Properties

        public Steering.SteeringData currentState;
        public Steering.SteeringData oldState;
        public Steering.SteeringData targetState;

        public Steering.SteeringData displacement;
        public List<Steering.SteeringData> targetsList;

        public NavMap map;
        public Pathfinding.Methods pathfindingMethod = Pathfinding.Methods.aStar;

        public eNavModes oldNavMode;
        public eNavModes currentNavMode;

        public float wanderRadius;
        public float wanderRegionOffset;

        public float maxTimeOnPath = 4000; // Four Seconds
        public float timeOnPath = float.PositiveInfinity;

        public float maxTimeWithoutDisplacement = 50;
        public float timeWithoutDisplacement = 0;

        public AIGroup flock;
        public bool affectedByGroup = true;
        public bool affectsGroup = true;

        public FarseerPhysics.Collision.RayCastInput forwardWhisker;
        public FarseerPhysics.Collision.RayCastInput leftWhisker;
        public FarseerPhysics.Collision.RayCastInput rightWhisker;

        public RigidBody rigidBody;

        #endregion

        public NavAgent(Screen containingScreen, GameObject parent, DrawLayer drawLayer, RigidBody theRigidBody = null)
            : base(containingScreen, parent, drawLayer)
        {
            rigidBody = theRigidBody;

            if(rigidBody == null)
            {
                if(parent != null)
                {
                    RigidBody found = parent.GetChild<RigidBody>();
                    if(found != null)
                    {
                        rigidBody = found;
                    }
                    else
                    {
                        rigidBody = parent.AddChild<RigidBody>();
                    }
                }
                else
                {
                    throw new Exception("No rigid body or parent specified.");
                }
            }

            currentState = new Steering.SteeringData(0);
            oldState = new Steering.SteeringData(0);
            targetState = new Steering.SteeringData(0);
            targetState.kinematicData.position.X = float.NaN;
            targetsList = new List<Steering.SteeringData>();

            oldNavMode = eNavModes.None;
            currentNavMode = eNavModes.None;

            SyncOrder = SyncPresets[SyncLayers.AI];
            SyncType = SyncTypes.SyncBoth;

            //EngineDaemon.aiController.navAgents.Add(this);
        }

        protected override void UnloadContent()
        {
            //GameController.aiController.navAgents.Remove(this);

            base.UnloadContent();
        }

        //public override int CountDrawableObjects()
        //{
        //    int objs = 0;
        //    objs += queuedTargetLines.Count();

        //    return objs + base.CountDrawableObjects();
        //}

        #region Behaviors

        /// <summary>
        /// Foundational movement behavior, to or away from a target.
        /// </summary>
        /// <param name="stoppingDistance">Radius to accept a completed solution.</param>
        /// <param name="reverse">Move away from the target.</param>
        /// <param name="allowArrive">Allows for velocity damping near the target.</param>
        /// <param name="allowFlocking">Calculate flock's influence</param>
        /// <param name="avoidObstacles">Steer around objects</param>
        public void Move(float stoppingDistance, bool reverse = false, bool allowArrive = true,
            bool allowFlocking = true, bool avoidObstacles = true)
        {
            if (targetState.kinematicData.position != null
                && !float.IsNaN(targetState.kinematicData.position.X)
                && targetState.kinematicData.position != currentState.kinematicData.position)
            {
                // Update state variables
                displacement = Steering.Move(ref currentState, ref targetState, stoppingDistance, reverse, allowArrive);

                currentState.kinematicData.linearAcceleration = displacement.kinematicData.linearAcceleration;
                currentState.kinematicData.linearVelocity = displacement.kinematicData.linearVelocity;
                currentState.kinematicData.orientation = displacement.kinematicData.orientation;
                currentState.distance = displacement.distance;
                currentState.displacementDirection = displacement.displacementDirection;
                currentState.kinematicData.orientationVector = displacement.kinematicData.orientationVector;

                if (avoidObstacles)
                {
                    if (rightWhisker.Point2 != rightWhisker.Point1)
                    {
                        PerformRaycast(rightWhisker);
                    }

                    if (leftWhisker.Point2 != leftWhisker.Point1)
                    {
                        PerformRaycast(leftWhisker);
                    }

                    if (forwardWhisker.Point2 != forwardWhisker.Point1)
                    {
                        PerformRaycast(forwardWhisker);
                    }
                }

                if (currentState.kinematicData.linearVelocity != Vector2.Zero)
                {
                    timeOnPath += (float)GameController.gameTime.ElapsedGameTime.TotalMilliseconds;
                }

                if (currentState.kinematicData.position == oldState.kinematicData.position
                    && oldState.kinematicData.linearVelocity != Vector2.Zero)
                {
                    timeWithoutDisplacement += (float)GameController.gameTime.ElapsedGameTime.TotalMilliseconds;
                }
                else
                {
                    timeWithoutDisplacement = 0;
                }

                float distance = (targetState.kinematicData.position - currentState.kinematicData.position).Length();

                if (timeWithoutDisplacement >= maxTimeWithoutDisplacement
                 || timeOnPath >= maxTimeOnPath
                 || reverse ? distance >= stoppingDistance : distance <= stoppingDistance)
                {
                    ArriveAtTarget();
                }
            }
        }

        /// <summary>
        /// Movement behavior that will attempt to intercept the target.
        /// </summary>
        /// <param name="reverse">True for Evade behavior.</param>
        public void Pursue(bool reverse = false)
        {
            if (targetState.kinematicData.linearVelocity.Length() > 0)
            {
                float timeToIntercept = (targetState.kinematicData.position - currentState.kinematicData.position).Length();
                timeToIntercept /= currentState.kinematicData.linearVelocity.Length() + targetState.kinematicData.linearVelocity.Length();

                targetState.kinematicData.linearAcceleration = Vector2.Normalize(targetState.kinematicData.linearVelocity) * (targetState.kinematicData.maxLinearAcceleration
                    * timeToIntercept);
                targetState.kinematicData.linearVelocity = Kinematics.UpdateLinearVelocity(ref targetState.kinematicData, timeToIntercept);
                targetState.kinematicData.position = Kinematics.UpdatePosition(ref targetState.kinematicData, timeToIntercept);

                SetImmediateTarget(targetState);
            }

            if (reverse)
            {
                Move(currentState.fleeStoppingRadius, true, false);
            }
            else
            {
                Move(currentState.normalStoppingRadius);
            }
        }

        /// <summary>
        /// Movement behavior that will select it's own targets in order to meander.
        /// </summary>
        public void Wander()
        {
            if (float.IsNaN(targetState.kinematicData.position.X))
            {
                Vector2 wanderCirclePosition = Vector2.Zero;

                wanderCirclePosition = currentState.kinematicData.position + (currentState.kinematicData.orientationVector * wanderRegionOffset);

                Vector2 targetPos = new Vector2(MathHelper.GlobalRandom.Next((int)(wanderCirclePosition.X - wanderRadius), (int)(wanderCirclePosition.X + wanderRadius)),
                    MathHelper.GlobalRandom.Next((int)(wanderCirclePosition.Y - wanderRadius), (int)(wanderCirclePosition.Y + wanderRadius)));

                if (map == null)
                {
                    SetImmediateTarget(targetPos);
                }
                else
                {
                    targetState.kinematicData.position = map.PointToNode(targetPos, true, true).Location;
                    CalculatePath(targetState);
                }
            }

            Move(currentState.wanderStoppingRadius);
        }

        #endregion

        #region Collision Avoidance

        protected void PerformRaycast(FarseerPhysics.Collision.RayCastInput ray)
        {
            Vector2 point = Vector2.Zero;
            Vector2 normal = Vector2.Zero;

            bool hitAny = false;
            //ContainingScreen.physicsController.world.RayCast((f, p, n, fr) =>
            //{
            //    Body body = f.Body;
            //    if (body.UserData != null)
            //    {
            //        if ((string)body.UserData == "Wall")
            //        {
            //            hitAny = true;
            //            point = p;
            //            normal = n;
            //            return 0;
            //        }
            //    }

            //    return -1;
            //}, ray.Point1, ray.Point2);

            if (hitAny)
            {
                //Vector2 whisker = ray.Point2 - ray.Point1;
                //whisker = new Vector2(-whisker.Y, whisker.X);
                //whisker.Normalize();

                //Vector2 avoidanceTarget;
                //if (whisker.Y > 0)
                //{
                //    avoidanceTarget = (point + whisker);
                //}
                //else
                //{
                //    avoidanceTarget = (point - whisker);
                //}

                Vector2 avoidanceTarget = point + normal;

                //Steering.SteeringData temp = targetState;
                //targetState.kinematicData.position = avoidanceTarget;
                //Move(0, false, false, false, false);
                //targetState = temp;

                //Vector2 difference = avoidanceTarget - currentState.kinematicData.position;

                //float strength = 100000f / difference.LengthSquared();
                //strength = currentState.kinematicData.maxLinearAcceleration * 10000;

                // Update state variables
                currentState.kinematicData.linearAcceleration = currentState.kinematicData.maxLinearAcceleration * Vector2.Normalize(normal) * 5f;
                currentState.kinematicData.linearVelocity = Kinematics.UpdateLinearVelocity(ref currentState.kinematicData);
            }
        }

        protected void DrawRaycast(FarseerPhysics.Collision.RayCastInput ray)
        {
            Vector2 point = Vector2.Zero, normal = Vector2.Zero;

            bool hitAny = false;
            ContainingScreen.physicsController.world.RayCast((f, p, n, fr) =>
            {
                Body body = f.Body;
                if (body.UserData != null)
                {
                    if ((string)body.UserData == "Wall")
                    {
                        hitAny = true;
                        point = p;
                        normal = n;
                        return 0;
                    }
                }

                return -1;
            }, ray.Point1, ray.Point2);

            //if (hitAny)
            //{
            //    EngineDaemon.physicsDebugView.DrawPoint(point, 0.2f, new Color(0.4f, 0.9f, 0.4f), true);

            //    EngineDaemon.physicsDebugView.DrawSegment(ray.Point1, point, new Color(0.8f, 0.8f, 0.8f), true);

            //    Vector2 head = point + 0.5f * normal;
            //    EngineDaemon.physicsDebugView.DrawSegment(point, head, new Color(0.9f, 0.9f, 0.4f), true);
            //}
            //else
            //{
            //    EngineDaemon.physicsDebugView.DrawSegment(ray.Point1, ray.Point2, new Color(0.8f, 0.8f, 0.8f), true);
            //}
        }

        #endregion

        #region Syncing

        /// <summary>
        /// Syncs the parent object's properties to our NavAgent's kinematics.
        /// </summary>
        public void SyncToKinemetics()
        {
            //// Update draw variables
            //if (!float.IsNaN(currentState.kinematicData.orientation))
            //{
            //    rigidBody.body.Rotation = currentState.kinematicData.orientation;
            //}

            //if (!float.IsNaN(currentState.kinematicData.linearVelocity.X)
            //    && !float.IsNaN(currentState.kinematicData.linearVelocity.Y))
            //{
            //    rigidBody.body.LinearVelocity = ConvertUnits.ToSimUnits(currentState.kinematicData.linearVelocity);
            //}

            //rigidBody.body.Position = ConvertUnits.ToSimUnits(currentState.kinematicData.position);

            if (!float.IsNaN(currentState.kinematicData.orientation))
            {
                Rotation = currentState.kinematicData.orientation;
                rigidBody.Rotation = Rotation;
            }

            if (!float.IsNaN(currentState.kinematicData.linearVelocity.X)
                && !float.IsNaN(currentState.kinematicData.linearVelocity.Y))
            {
                rigidBody.body.LinearVelocity = ConvertUnits.ToSimUnits(currentState.kinematicData.linearVelocity);
            }

            Position = currentState.kinematicData.position;
            //Position -= Vector2.One * 0.02f;
        }

        /// <summary>
        /// Syncs the NavAgent kinematics to the parent object's properties.
        /// </summary>
        public void SyncKinemetics()
        {
            //currentState.kinematicData.orientation = rigidBody.body.Rotation;
            //currentState.kinematicData.orientationVector = Kinematics.SolveOrientationVector(ref currentState.kinematicData);
            //currentState.kinematicData.position = ConvertUnits.ToDisplayUnits(rigidBody.body.Position);

            //if (!float.IsNaN(rigidBody.body.LinearVelocity.X)
            //    && !float.IsNaN(rigidBody.body.LinearVelocity.Y))
            //{
            //    currentState.kinematicData.linearVelocity = ConvertUnits.ToDisplayUnits(rigidBody.body.LinearVelocity);
            //}

            currentState.kinematicData.orientation = Rotation;
            currentState.kinematicData.orientationVector = Kinematics.SolveOrientationVector(ref currentState.kinematicData);
            currentState.kinematicData.position = Position;

            if (!float.IsNaN(rigidBody.body.LinearVelocity.X)
                && !float.IsNaN(rigidBody.body.LinearVelocity.Y))
            {
                currentState.kinematicData.linearVelocity = ConvertUnits.ToDisplayUnits(rigidBody.body.LinearVelocity);
            }
        }

        #endregion

        #region Waypoint Mangement

        public void AddWaypoint(Steering.SteeringData target)
        {
            targetsList.Add(target);

            UpdateDebug();
        }

        public void AddWaypoint(Vector2 target)
        {
            Steering.SteeringData tempTarget = new Steering.SteeringData(target);

            AddWaypoint(tempTarget);
        }

        public void SetImmediateTarget(Steering.SteeringData target)
        {
            ClearWaypoints();

            AddWaypoint(target);

            targetState = targetsList[0];

            timeOnPath = 0;
            timeWithoutDisplacement = 0;
        }

        public void SetImmediateTarget(Vector2 target)
        {
            Steering.SteeringData tempTarget = new Steering.SteeringData(target);

            SetImmediateTarget(tempTarget);
        }

        public void RemoveWaypoint(int index)
        {
            if (targetsList.Count() > 0
                && targetsList[index].kinematicData.position != null)
            {
                targetsList.RemoveAt(index);
            }
        }

        public void ClearWaypoints()
        {
            targetsList.Clear();
        }

        public void ArriveAtTarget()
        {
            RemoveWaypoint(0);

            timeOnPath = 0;
            timeWithoutDisplacement = 0;
        }

        #endregion

        #region Path Following

        public void CalculatePath(Steering.SteeringData target)
        {
            bool doHybrid = false;

            PathNode currentNode = map.PointToNode(currentState.kinematicData.position, true, true);
            PathNode targetNode = map.PointToNode(target.kinematicData.position, false, true);
            if (targetNode == null
                || targetNode.IsOccupied)
            {
                targetNode = map.PointToNode(target.kinematicData.position, true, true);
                doHybrid = true;
            }

            PathNode pathNode = Pathfinding.FindPath(pathfindingMethod, ref currentNode, ref targetNode, ref map);

            List<PathNode> path = new List<PathNode>();
            path = QueuePathNodes(pathNode, path);

            if (path != null)
            {
                ClearWaypoints();

                path.RemoveAt(0);
                foreach (PathNode node in path)
                {
                    AddWaypoint(new Steering.SteeringData(node.Location));
                }

                //if (doHybrid)
                {
                    AddWaypoint(target);
                }
            }

            //AddWaypoint(new Steering.SteeringData(target.kinematicData.position));
        }

        private List<PathNode> QueuePathNodes(PathNode node, List<PathNode> path, int depth = 0)
        {
            if (depth > 100)
            {
                return path;
            }

            if (node != null)
            {
                if (node.backPointer != null)
                {
                    QueuePathNodes(node.backPointer, path, depth + 1);
                }

                path.Add(node);

                return path;
            }

            return null;
        }

        #endregion

        public eNavModes NavMode
        {
            set
            {
                if (currentNavMode != value)
                {
                    oldNavMode = currentNavMode;
                    currentNavMode = value;
                    //ClearWaypoints();
                }
            }
        }

        public override void Update()
        {
            //if (EngineDaemon.showNavDebug)
            //{
            //    stopwatch.Reset();
            //    stopwatch.Start();

            //    EngineDaemon.navAgents++;
            //}

            UpdateDebug();

            // Sync my initial Kinematic data to my container's state.
            SyncKinemetics();

            rightWhisker.Point1 = ConvertUnits.ToSimUnits(currentState.kinematicData.position);
            rightWhisker.Point2 = rightWhisker.Point1 + (ConvertUnits.ToSimUnits(20)
                * Kinematics.SolveOrientationVector(currentState.kinematicData.orientation + (float)(System.Math.PI / 8f)));

            leftWhisker.Point1 = ConvertUnits.ToSimUnits(currentState.kinematicData.position);
            leftWhisker.Point2 = leftWhisker.Point1 + (ConvertUnits.ToSimUnits(20)
                * Kinematics.SolveOrientationVector(currentState.kinematicData.orientation - (float)(System.Math.PI / 8f)));

            forwardWhisker.Point1 = ConvertUnits.ToSimUnits(currentState.kinematicData.position);
            forwardWhisker.Point2 = forwardWhisker.Point1 + (ConvertUnits.ToSimUnits(60)
                * Kinematics.SolveOrientationVector(currentState.kinematicData.orientation));

            if (map != null
                && targetsList.Count() > 0)
            {
                CalculatePath(targetsList[targetsList.Count() - 1]);
            }

            // Set our target state to default if we have no waypoints queued.
            if (targetsList.Count() > 0)
            {
                targetState = targetsList[0];
            }
            else if (!float.IsNaN(targetState.kinematicData.position.X))
            {
                targetState = new Steering.SteeringData(0);
                targetState.kinematicData.position.X = float.NaN;
            }

            // Perform my current behavior.
            switch (currentNavMode)
            {
                case eNavModes.Seek:
                    Move(currentState.normalStoppingRadius, false);
                    break;
                case eNavModes.Flee:
                    Move(currentState.fleeStoppingRadius, true);
                    break;
                case eNavModes.Pursue:
                    Pursue(false);
                    break;
                case eNavModes.Evade:
                    Pursue(true);
                    break;
                case eNavModes.Wander:
                    Wander();
                    break;
                case eNavModes.None:
                default:
                    break;
            }

            // Sync my container to my calculated Kinematic data.
            // Unless I'm told to be doing nothing.
            switch (currentNavMode)
            {
                case eNavModes.Seek:
                case eNavModes.Flee:
                case eNavModes.Pursue:
                case eNavModes.Evade:
                case eNavModes.Wander:
                    SyncToKinemetics();
                    break;
                case eNavModes.None:
                default:
                    break;
            }

            oldState = currentState;
            oldNavMode = currentNavMode;

            base.Update();

            //if (EngineDaemon.showNavDebug)
            //{
            //    stopwatch.Stop();
            //    EngineDaemon.curNavUpdateTime += (float)stopwatch.ElapsedTicks / TimeSpan.TicksPerMillisecond;
            //    EngineDaemon.avgNavUpdateTime += EngineDaemon.curNavUpdateTime;
            //    EngineDaemon.avgNavUpdateTime /= 2f;
            //}
        }

        public override void Draw()
        {
            if (ContainingScreen.aiController.DebugEnabled)
            {
                if (targetsList != null)
                {
                    if (targetsList.Count() > 0)
                    {
                        Primitives.DrawSegment(currentState.kinematicData.position, targetsList[0].kinematicData.position, Color.LightBlue);

                        for (int i = 1; i < targetsList.Count(); i++)
                        {
                            Primitives.DrawSegment(targetsList[i - 1].kinematicData.position, targetsList[i].kinematicData.position, Color.Blue);
                        }
                    }
                }

                //for (int i = 0; i < queuedTargetLines.Count(); i++)
                //{
                //    queuedTargetLines[i].ManualDraw(gameTime);
                //}

                //for (int i = 0; i < queuedTargetCrosses.Count(); i++)
                //{
                //    queuedTargetCrosses[i].ManualDraw(gameTime);
                //}

                //velocityLine.ManualDraw(gameTime);

                //if (rightWhisker.Point2 != rightWhisker.Point1)
                //{
                //    EngineDaemon.physicsDebugView.BeginCustomDraw(ref EngineDaemon.projectionMatrix, ref EngineDaemon.viewMatrix);
                //    DrawRaycast(rightWhisker);
                //    DrawRaycast(leftWhisker);
                //    DrawRaycast(forwardWhisker);
                //    EngineDaemon.physicsDebugView.EndCustomDraw();
                //}
            }

            base.Draw();
        }

        private void UpdateDebug()
        {
            //if (EngineDaemon.showNavDebug)
            //{
            //    // Line for current velocity direction
            //    velocityLine.startPosition = currentState.kinematicData.position;
            //    velocityLine.endPosition = velocityLine.startPosition + (Vector2.Normalize(currentState.kinematicData.linearVelocity) * 20);

            //    for (int i = 0; i < queuedTargetLines.Count(); i++)
            //    {
            //        if (i == 0)
            //        {
            //            queuedTargetLines[i].startPosition = new Vector2(
            //                currentState.kinematicData.position.X,
            //                currentState.kinematicData.position.Y);
            //        }

            //        switch (currentNavMode)
            //        {
            //            case eNavModes.Seek:
            //            case eNavModes.Pursue:
            //                queuedTargetLines[i].drawColor = Color.Green;
            //                queuedTargetCrosses[i].drawColor = Color.Green;
            //                break;
            //            case eNavModes.Evade:
            //            case eNavModes.Flee:
            //                queuedTargetLines[i].drawColor = Color.Red;
            //                queuedTargetCrosses[i].drawColor = Color.Red;
            //                break;
            //            case eNavModes.Wander:
            //                queuedTargetLines[i].drawColor = Color.CornflowerBlue;
            //                queuedTargetCrosses[i].drawColor = Color.CornflowerBlue;
            //                break;
            //            case eNavModes.None:
            //            default:
            //                break;
            //        }

            //        queuedTargetLines[i].Update(EngineDaemon.gameTime);
            //        queuedTargetCrosses[i].Update(EngineDaemon.gameTime);
            //    }
            //    velocityLine.Update(EngineDaemon.gameTime);
            //}
        }
    }
}
