﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;

namespace Flechette_Engine
{
    /// <summary>
    /// Policy types.
    /// </summary>
    public enum ExceptionPolicies
    {
        Allow,
        Disallow
    }

    /// <summary>
    /// Single rule for child/parent/peer policies.
    /// </summary>
    public struct ObjectExceptionRule
    {
        public ExceptionPolicies Policy { get; set; }

        public Type Type { get; set; }
    }

    /// <summary>
    /// Policy to use for child/parent/peer objects.
    /// </summary>
    public struct ObjectExceptionPolicy
    {
        public ExceptionPolicies BlanketPolicy { get; set; }

        public List<ObjectExceptionRule> ExceptionRules { get; set; }
    }

    /// <summary>
    /// Types of update methods available.
    /// </summary>
    public enum SyncTypes
    {
        Independent,
        SyncParentToThis,
        SyncThisToParent,
        SyncBoth,
    }

    /// <summary>
    /// Default update priority types
    /// </summary>
    public enum SyncLayers
    {
        AI,
        Physics,
        Graphics,
    }

    public enum DrawOrderings
    {
        BeforeParent,
        AfterParent,
        Independant
    }

    /// <summary>
    /// Generic class for all self-contained items in the game.
    /// </summary>
    public class GameObject : DrawableGameComponent, IGameObject
    {
        /// <summary>
        /// Default update priorities for certain types of children.
        /// </summary>
        public static readonly Dictionary<SyncLayers, float> SyncPresets = new Dictionary<SyncLayers, float>()
        {
            { SyncLayers.AI, -2f },
            { SyncLayers.Physics, -1f },
            { SyncLayers.Graphics, 1f },
        };

        /// <summary>
        /// Don't mess with this directly.
        /// Use the accessor property.
        /// </summary>
        protected Vector2 position;

        /// <summary>
        /// Don't mess with this directly.
        /// Use the accessor property.
        /// </summary>
        protected Vector2 size;

        /// <summary>
        /// Fires on position changes.
        /// </summary>
        public event System.EventHandler PositionChanged;

        /// <summary>
        /// Fires on size changes.
        /// </summary>
        public event System.EventHandler SizeChanged;

        /// <summary>
        /// Represents the world-space location of the object.
        /// </summary>
        public Vector2 Position { get { return position; } set { position = value; OnPositionChanged(new System.EventArgs()); } }

        /// <summary>
        /// Represents the width and height of the object.
        /// </summary>
        public Vector2 Size
        { 
            get { return size; } 
            set 
            { 
                size = value; 
                SetCenter(); 
                ManuallySized = true;
                OnSizeChanged(new System.EventArgs());
            } 
        }

        /// <summary>
        /// True if the user has manually set the size.
        /// Forces the object to retain its size during some automated processes.
        /// </summary>
        public bool ManuallySized { get; set; }

        /// <summary>
        /// The relative center (ignores Position).
        /// Essentially, Size / 2
        /// </summary>
        public Vector2 Center { get; protected set; }

        /// <summary>
        /// Radian angle of rotation.
        /// </summary>
        public float Rotation { get; set; }

        public Color Color { get; set; }

        /// <summary>
        /// Parent GameObject.
        /// </summary>
        public IGameObject Parent { get; protected set; }

        /// <summary>
        /// All the child GameObjects.
        /// </summary>
        public List<IGameObject> Children { get; protected set; }

        /// <summary>
        /// List of string tags.
        /// Can be used to group separate GameObjects.
        /// </summary>
        public List<string> Tags { get; set; }

        /// <summary>
        /// Reference string. Automatically set to a hash but can
        /// be manually set as well.
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// Offset for the Position.
        /// Can be used to shift the position of this 
        /// object relative to its parent.
        /// </summary>
        public Vector2 Offset { get; set; }

        /// <summary>
        /// Offset for the Position.
        /// Can be used to shift the position of this 
        /// object relative to its parent.
        /// </summary>
        public float OffsetRadius { get; set; }

        /// <summary>
        /// The method of parent-child syncing that should happen.
        /// </summary>
        public SyncTypes SyncType { get; set; }

        /// <summary>
        /// The priority of this object's update relative to
        /// its parent.
        /// </summary>
        public float SyncOrder { get; set; }

        public bool SyncPosition { get; set; }

        public bool SyncRotation { get; set; }

        public bool SyncSize { get; set; }

        /// <summary>
        /// Draw order in the sprite batch.
        /// </summary>
        new public float DrawOrder { get; set; }

        /// <summary>
        /// Defines DrawOrder relationship with parent.
        /// </summary>
        public DrawOrderings DrawOrdering { get; set; }

        /// <summary>
        /// DrawLayer object of the ContainingScreen to which
        /// this object belongs.
        /// </summary>
        public DrawLayer DrawLayer { get; set; }

        /// <summary>
        /// Rules for allowable and disallowable parent objects.
        /// </summary>
        public ObjectExceptionPolicy ParentExceptions { get; protected set; }

        /// <summary>
        /// Rules for allowable and disallowable child objects.
        /// </summary>
        public ObjectExceptionPolicy ChildExceptions { get; protected set; }

        /// <summary>
        /// Rules for allowable and disallowable peer objects.
        /// </summary>
        public ObjectExceptionPolicy PeerExceptions { get; protected set; }

        /// <summary>
        /// Screen object to which this object belongs.
        /// </summary>
        public Screen ContainingScreen { get; set; }

        /// <summary>
        /// Custom data for any use.
        /// </summary>
        public object UserData { get; set; }

        /// <summary>
        /// Custom logic to run after each Update.
        /// </summary>
        public Action UpdateAction { get; set; }

        /// <summary>
        /// Custom logic to run after each UpdatePrepass.
        /// </summary>
        public Action UpdatePrepassAction { get; set; }

        /// <summary>
        /// Custom logic to run after each UpdatePostpass.
        /// </summary>
        public Action UpdatePostpassAction { get; set; }

        /// <summary>
        /// Custom logic to run after each Draw.
        /// </summary>
        public Action DrawAction { get; set; }

        /// <summary>
        /// Custom logic to run on Dispose.
        /// </summary>
        public Action DisposeAction { get; set; }

        /// <summary>
        /// Default Constructor.
        /// </summary>
        /// <param name="containingScreen">Screen to which this object belongs.</param>
        /// <param name="parent">Parent GameObject, if any.</param>
        /// <param name="drawLayer">DrawLayer to which this object belongs. null for undrawable objects.</param>
        public GameObject(Screen containingScreen, IGameObject parent = null, DrawLayer drawLayer = null)
            : base(GameController.game)
        {
            // Get all the parameters
            ContainingScreen = containingScreen;
            Parent = parent;
            DrawLayer = drawLayer;

            // Defaults
            Size = Vector2.Zero;
            Position = Vector2.Zero;
            Offset = Vector2.Zero;
            Color = Color.White;

            // If we're not given a parent object...
            if (Parent == null)
            {
                // Add myself to the screen's list of base objects
                ContainingScreen.gameObjects.Add(this);

                DrawOrdering = DrawOrderings.Independant;
            }
            else
            {
                // Match my parent's data
                Size = Parent.Size;
                Position = Parent.Position;

                DrawOrdering = DrawOrderings.AfterParent;
            }

            // If we're given a DrawLayer
            if (drawLayer != null)
                // Add myself to the DrawLayer's members list
                drawLayer.members.Add(this);

            // Default peer rules. Allow all.
            PeerExceptions = new ObjectExceptionPolicy()
            {
                BlanketPolicy = ExceptionPolicies.Allow,
                ExceptionRules = new List<ObjectExceptionRule>()
            };

            // Default parent rules. Allow all.
            ParentExceptions = new ObjectExceptionPolicy()
            {
                BlanketPolicy = ExceptionPolicies.Allow,
                ExceptionRules = new List<ObjectExceptionRule>()
                {
                    // Nav maps should only be added to screens.
                    new ObjectExceptionRule() { Policy = ExceptionPolicies.Disallow, Type = typeof(NavMap) },

                    // Cameras should only be added to screens.
                    new ObjectExceptionRule() { Policy = ExceptionPolicies.Disallow, Type = typeof(Camera) },
                }
            };

            // Default child rules. Allow all.
            ChildExceptions = new ObjectExceptionPolicy()
            {
                BlanketPolicy = ExceptionPolicies.Allow,
                ExceptionRules = new List<ObjectExceptionRule>()
                {
                    // Nav maps should only be added to screens.
                    new ObjectExceptionRule() { Policy = ExceptionPolicies.Disallow, Type = typeof(NavMap) },

                    // Cameras should only be added to screens.
                    new ObjectExceptionRule() { Policy = ExceptionPolicies.Disallow, Type = typeof(Camera) },
                }
            };

            Children = new List<IGameObject>();

            Tags = new List<string>();
            ID = this.GetHashCode().ToString();

            // Set the sync type to be dependant on the parent
            SyncType = SyncTypes.SyncThisToParent;

            // Set the sync order to be just after the parent
            SyncOrder = 0;

            SyncPosition = true;
            SyncRotation = true;
            SyncSize = true;

            // Set the draw order to be just after the parent, if any
            DrawOrder = 0.5f;

            // We're not manually sized yet
            ManuallySized = false;

            GameController.game.Window.ClientSizeChanged += Window_ClientSizeChanged;
        }

        /// <summary>
        /// Dispose of myself and all my children.
        /// </summary>
        /// <param name="disposing">True to dispose of unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            if(Children != null)
            {
                foreach (GameObject child in Children)
                {
                    child.Dispose(disposing);
                }
            }

            GameController.game.Window.ClientSizeChanged -= Window_ClientSizeChanged;

            if (DisposeAction != null)
                DisposeAction();

            Game.Components.Remove(this);

            base.Dispose(disposing);
        }

        /// <summary>
        /// Update pass prior to the general update logic.
        /// Called UpdatePrepass for all children as well.
        /// </summary>
        public virtual void UpdatePrepass()
        {
            // Sync myself to parent if necessary
            if (Parent != null)
            {
                if (SyncType == SyncTypes.SyncBoth)
                {
                    if (SyncPosition)
                        Position = Parent.Position + Offset;

                    if(SyncRotation)
                        Rotation = Parent.Rotation;

                    if(SyncSize)
                        Size = Parent.Size;
                }
            }

            if (Children != null)
            {
                foreach (GameObject child in Children
                    .OrderByDescending(e => e.SyncOrder)
                    .Reverse())
                {
                    child.UpdatePrepass();
                }
            }

            if (UpdatePrepassAction != null)
                UpdatePrepassAction();
        }

        /// <summary>
        /// !!DON'T CALL/CHANGE THIS!!
        /// This will update and sync to/from all children.
        /// Place custom logic in Update() and call that manually.
        /// Only change this if you really know what you're doing.
        /// </summary>
        public virtual void FullUpdate()
        {
            if (Enabled)
            {
                List<GameObject> negativeChildren = new List<GameObject>();
                List<GameObject> positiveChildren = new List<GameObject>();

                // Get the lists of children based on update order
                if (Children != null)
                {
                    foreach (GameObject child in Children
                            .OrderByDescending(e => e.SyncOrder)
                            .Reverse())
                    {
                        if(child.UpdateOrder < 0)
                        {
                            negativeChildren.Add(child);
                        }
                        else
                        {
                            positiveChildren.Add(child);
                        }
                    }
                }

                // Update children with lower orders
                UpdateChildren(negativeChildren);

                // Do my updates
                Update();

                // Update children with higher orders
                UpdateChildren(positiveChildren);
            }
        }

        /// <summary>
        /// Update specified children.
        /// </summary>
        protected virtual void UpdateChildren(List<GameObject> toUpdate)
        {
            if (Enabled)
            {
                if (toUpdate != null)
                {
                    foreach (GameObject child in toUpdate)
                    {
                        child.FullUpdate();
                    }
                }
            }
        }

        /// <summary>
        /// Updates this object and base type.
        /// Will not update or sync to/from Children.
        /// </summary>
        public virtual void Update()
        {
            if (Enabled)
            {
                SetCenter();
            }

            base.Update(GameController.gameTime);

            if (UpdateAction != null)
                UpdateAction();
        }

        /// <summary>
        /// Update pass after the general update logic.
        /// Called UpdatePostpass for all children as well.
        /// </summary>
        public virtual void UpdatePostpass()
        {
            if (Children != null)
            {
                foreach (GameObject child in Children
                    .OrderByDescending(e => e.SyncOrder)
                    .Reverse())
                {
                    child.UpdatePostpass();
                }
            }

            if (Parent != null)
            {
                // Sync myself to parent if necessary
                if (SyncType == SyncTypes.SyncThisToParent)
                {
                    if (SyncPosition)
                        Position = Parent.Position + 
                            (Vector2.Normalize(new Vector2(
                                    (float)Math.Cos(Parent.Rotation), 
                                    (float)Math.Sin(Parent.Rotation))) 
                                * OffsetRadius);

                    if (SyncRotation)
                        Rotation = Parent.Rotation;

                    if (SyncSize)
                        Size = Parent.Size;
                }
                // Sync parent to myself if necessary
                else if (SyncType == SyncTypes.SyncParentToThis
                    || SyncType == SyncTypes.SyncBoth)
                {
                    if (SyncPosition)
                        Parent.Position = Position;

                    if (SyncRotation)
                        Parent.Rotation = Rotation;

                    if (SyncSize)
                        Parent.Size = Size;
                }
            }

            // Handle auto DrawOrder with parent
            if (Parent != null)
            {
                switch (DrawOrdering)
                {
                    case DrawOrderings.AfterParent:
                        DrawOrder = Math.Min(1, Parent.DrawOrder - 0.0001f);
                        break;

                    case DrawOrderings.BeforeParent:
                        DrawOrder = Math.Max(0, Parent.DrawOrder + 0.0001f);
                        break;
                }
            }

            if (UpdatePostpassAction != null)
                UpdatePostpassAction();
        }

        /// <summary>
        /// Draw myself and all children.
        /// </summary>
        public virtual void Draw()
        {
            if (Visible)
            {
                if (Children != null)
                {
                    foreach(GameObject child in Children)
                    {
                        child.Draw();
                    }
                }

                //Primitives.DrawSolidPolygon(
                //        new Vector2[] {
                //        Vector2.Transform(new Vector2(GetRectangle().X, GetRectangle().Y), ContainingScreen.activeCamera.ViewMatrix),
                //        Vector2.Transform(new Vector2(GetRectangle().X + GetRectangle().Width, GetRectangle().Y), ContainingScreen.activeCamera.ViewMatrix),
                //        Vector2.Transform(new Vector2(GetRectangle().X + GetRectangle().Width, GetRectangle().Y + GetRectangle().Height), ContainingScreen.activeCamera.ViewMatrix),
                //        Vector2.Transform(new Vector2(GetRectangle().X, GetRectangle().Y + GetRectangle().Height), ContainingScreen.activeCamera.ViewMatrix),
                //    },
                //    Color.Yellow * 0.2f, true);
            }

            base.Draw(GameController.gameTime);

            if (DrawAction != null)
                DrawAction();
        }

        /// <summary>
        /// Set the relative center based on the current size.
        /// </summary>
        protected void SetCenter()
        {
            Center = Size / 2f;
        }

        /// <summary>
        /// Gets a rectangle using the object's Position and Size.
        /// </summary>
        /// <returns>Circumscribing rectangle</returns>
        public virtual Rectangle GetRectangle()
        {
            //return new Rectangle(
            //    (int)(Position.X - Center.X),
            //    (int)(Position.Y - Center.Y),
            //    (int)Size.X,
            //    (int)Size.Y);
            return new Rectangle(
                (int)(Position.X),
                (int)(Position.Y),
                (int)Size.X,
                (int)Size.Y);
        }

        /// <summary>
        /// Determines if this object's rectangle contains another's completely.
        /// </summary>
        /// <param name="other">The other GameObject</param>
        /// <returns>True if this contains the other</returns>
        public virtual bool Contains(IGameObject other)
        {
            return GetRectangle().Contains(other.GetRectangle());
        }

        /// <summary>
        /// Determines if this object's rectangle contains a point.
        /// </summary>
        /// <param name="subject">The point of interest</param>
        /// <returns>True if this contains the point</returns>
        public virtual bool Contains(Vector2 subject)
        {
            return Contains((int)subject.X, (int)subject.Y);
        }

        /// <summary>
        /// Determines if this object's rectangle contains a point.
        /// </summary>
        /// <param name="subject">The point of interest</param>
        /// <returns>True if this contains the point</returns>
        public virtual bool Contains(Point subject)
        {
            return Contains(subject.X, subject.Y);
        }

        /// <summary>
        /// Determines if this object's rectangle contains a point.
        /// </summary>
        /// <param name="subject">The point of interest</param>
        /// <returns>True if this contains the point</returns>
        public virtual bool Contains(int x, int y)
        {
            return GetRectangle().Contains(x, y);
        }

        /// <summary>
        /// Determines if this object's rectangle intersects another's.
        /// </summary>
        /// <param name="other">The other GameObject</param>
        /// <returns>True if this intersects the other</returns>
        public virtual bool Intersects(IGameObject other)
        {
            return GetRectangle().Intersects(other.GetRectangle());
        }

        #region Children

        public T GetChild<T>() where T : IGameObject
        {
            if (Children != null)
            {
                if (Children.OfType<T>().Any())
                {
                    return Children.OfType<T>().First();
                }
            }

            return default(T);
        }

        public T GetChild<T>(string tag) where T : IGameObject
        {
            if (Children != null)
            {
                if (Children.OfType<T>().Any(e => e.Tags.Contains(tag)))
                {
                    return Children.OfType<T>().First(e => e.Tags.Contains(tag));
                }
            }

            return default(T);
        }

        public IGameObject GetChild(string tag)
        {
            if (Children != null)
            {
                if (Children.Any(e => e.Tags.Contains(tag)))
                {
                    return Children.First(e => e.Tags.Contains(tag));
                }
            }

            return null;
        }

        public IGameObject GetChild(IGameObject subject)
        {
            if (Children != null)
            {
                if (Children.Any(e => e == subject))
                {
                    return Children.First(e => e == subject);
                }
            }

            return null;
        }

        public IGameObject GetChild(Type subject)
        {
            if (Children != null)
            {
                if (Children.Any(e => e.GetType() == subject))
                {
                    return Children.First(e => e.GetType() == subject);
                }
            }

            return null;
        }

        public T[] GetChildren<T>() where T : IGameObject
        {
            if (Children != null)
            {
                if (Children.OfType<T>().Any())
                {
                    return Children.OfType<T>().ToArray();
                }
            }

            return null;
        }

        public T[] GetChildren<T>(string tag) where T : IGameObject
        {
            if (Children != null)
            {
                if (Children.OfType<T>().Any(e => e.Tags.Contains(tag)))
                {
                    return Children.OfType<T>().Where(e => e.Tags.Contains(tag)).ToArray();
                }
            }

            return null;
        }

        public IGameObject[] GetChildren(string tag)
        {
            if (Children != null)
            {
                if (Children.Any(e => e.Tags.Contains(tag)))
                {
                    return Children.Where(e => e.Tags.Contains(tag)).ToArray();
                }
            }

            return null;
        }

        public T AddChild<T>(object[] args = null) where T : IGameObject
        {
            if (args == null)
            {
                args = new object[3];
                args[0] = ContainingScreen;
                args[1] = this;
                args[2] = DrawLayer;
            }
            else
            {
                object[] temp = new object[3];
                temp[0] = ContainingScreen;
                temp[1] = this;
                temp[2] = DrawLayer;
                args = temp.Concat(args).ToArray();
            }

            if (Children == null)
            {
                Children = new List<IGameObject>();
            }

            T newChild = (T)Activator.CreateInstance(typeof(T), args);

            if (CanAddChild(newChild))
            {
                Children.Add(newChild);

                return newChild;
            }

            return GetChild<T>();
        }

        public IGameObject AddChild(IGameObject newChild)
        {
            if (Children == null)
            {
                Children = new List<IGameObject>();
            }

            if (CanAddChild(newChild))
            {
                Children.Add(newChild);
                newChild.ContainingScreen = ContainingScreen;
                newChild.DrawLayer = DrawLayer;

                return newChild;
            }

            return GetChild(newChild);
        }

        public void ClearChildren()
        {
            if (Children != null)
            {
                Children.Clear();
            }
        }

        public bool RemoveChild<T>() where T : IGameObject
        {
            if (Children != null)
            {
                if (Children.OfType<T>().Any())
                {
                    Children.Remove(Children.OfType<T>().First());

                    return true;
                }
            }

            return false;
        }

        public bool RemoveChildren<T>() where T : IGameObject
        {
            if (Children != null)
            {
                Children.RemoveAll(e => e.GetType() == typeof(T));

                return true;
            }

            return false;
        }

        public bool RemoveChild<T>(string tag) where T : IGameObject
        {
            if (Children != null)
            {
                if (Children.OfType<T>().Any(e => e.Tags.Contains(tag)))
                {
                    Children.Remove(Children.OfType<T>().First(e => e.Tags.Contains(tag)));

                    return true;
                }
            }

            return false;
        }

        public bool RemoveChild(string tag)
        {
            if (Children != null)
            {
                if (Children.Any(e => e.Tags.Contains(tag)))
                {
                    Children.Remove(Children.First(e => e.Tags.Contains(tag)));

                    return true;
                }
            }

            return false;
        }

        public bool RemoveChildren<T>(string tag) where T : IGameObject
        {
            if (Children != null)
            {
                Children.RemoveAll(e => e.GetType() == typeof(T) && e.Tags.Contains(tag));

                return true;
            }

            return false;
        }

        public bool RemoveChildren(string tag)
        {
            if (Children != null)
            {
                Children.RemoveAll(e => e.Tags.Contains(tag));

                return true;
            }

            return false;
        }

        #endregion

        #region Policy Controls

        public ObjectExceptionRule GetRuleFor(ObjectExceptionPolicy policy, Type subject)
        {
            ObjectExceptionRule result = new ObjectExceptionRule()
            {
                Policy = policy.BlanketPolicy,
                Type = subject,
            };

            if(policy.ExceptionRules != null)
            {
                if(policy.ExceptionRules.Any(e => e.Type == subject))
                {
                    result = policy.ExceptionRules.Last(e => e.Type == subject);
                }
            }

            return result;
        }

        public bool CanBeChildOf(Type parent)
        {
            if (GetRuleFor(ParentExceptions, parent).Policy == ExceptionPolicies.Allow)
            {
                return true;
            }

            return false;
        }

        public bool CanBeParentOf(Type child)
        {
            if (GetRuleFor(ChildExceptions, child).Policy == ExceptionPolicies.Allow)
            {
                return true;
            }

            return false;
        }

        public bool CanBePeerOf(Type peer)
        {
            if (GetRuleFor(PeerExceptions, peer).Policy == ExceptionPolicies.Allow)
            {
                return true;
            }

            return false;
        }

        public bool CanAddChild(IGameObject child)
        {
            // If I can have this child and it can have me as parent
            if(     CanBeParentOf(child.GetType())
                &&  child.CanBeChildOf(this.GetType()))
            {
                // If I have any children
                if(     Children != null 
                    &&  Children.Count() > 0)
                {
                    // For each child
                    foreach(IGameObject peer in Children)
                    {
                        // If the new child and any existing children can't be peers
                        if(     !child.CanBePeerOf(peer.GetType())
                            ||  !peer.CanBePeerOf(child.GetType()))
                        {
                            return false;
                        }
                    }
                }

                return true;
            }

            return false;
        }

        #endregion

        #region Events

        public virtual void OnPositionChanged(System.EventArgs e)
        { if (PositionChanged != null) PositionChanged(this, e); }

        public virtual void OnSizeChanged(System.EventArgs e)
        { if (SizeChanged != null) SizeChanged(this, e); }

        public virtual void Window_ClientSizeChanged(object sender, EventArgs e)
        { }

        #endregion
    }
}
