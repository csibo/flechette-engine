﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Flechette_Engine
{
    public interface IGameObject : IDrawable, IDisposable
    {
        Vector2 Position { get; set; }

        Vector2 Size { get; set; }

        float Rotation { get; set; }

        Color Color { get; set; }

        IGameObject Parent { get; }

        List<IGameObject> Children { get; }

        List<string> Tags { get; set; }

        string ID { get; set; }

        Vector2 Offset { get; set; }

        SyncTypes SyncType { get; set; }

        float SyncOrder { get; set; }

        new float DrawOrder { get; set; }

        Screen ContainingScreen { get; set; }

        DrawLayer DrawLayer { get; set; }

        ObjectExceptionPolicy ParentExceptions { get; }

        ObjectExceptionPolicy ChildExceptions { get; }

        ObjectExceptionPolicy PeerExceptions { get; }



        void Initialize();

        void UpdatePrepass();

        void FullUpdate();

        void Update();

        void UpdatePostpass();

        void Draw();

        Rectangle GetRectangle();

        bool Contains(IGameObject other);

        bool Contains(Vector2 subject);

        bool Contains(Point subject);

        bool Contains(int x, int y);

        bool Intersects(IGameObject other);

        #region Children

        T GetChild<T>() where T : IGameObject;

        T GetChild<T>(string tag) where T : IGameObject;

        IGameObject GetChild(string tag);

        T[] GetChildren<T>() where T : IGameObject;

        T[] GetChildren<T>(string tag) where T : IGameObject;

        IGameObject[] GetChildren(string tag);

        T AddChild<T>(object[] args) where T : IGameObject;

        IGameObject AddChild(IGameObject newChild);

        void ClearChildren();

        bool RemoveChild<T>() where T : IGameObject;

        bool RemoveChildren<T>() where T : IGameObject;

        bool RemoveChild<T>(string tag) where T : IGameObject;

        bool RemoveChild(string tag);

        bool RemoveChildren<T>(string tag) where T : IGameObject;

        bool RemoveChildren(string tag);

        ObjectExceptionRule GetRuleFor(ObjectExceptionPolicy policy, Type subject);

        bool CanBeChildOf(Type parent);

        bool CanBeParentOf(Type child);

        bool CanBePeerOf(Type peer);

        bool CanAddChild(IGameObject child);

        #endregion
    }
}
