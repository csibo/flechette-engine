﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Flechette_Engine
{
    public class PathfindingBehavior
    {
        public delegate PathNode PathfindingAlgorithm(PathNode start, PathNode target);

        public NavAgent_new Agent { get; set; }

        public Action BeginPathfinding { get; set; }

        public PathfindingAlgorithm PerformPathfinding { get; set; }

        public Action EndPathfinding { get; set; }

        public Action ApproachTarget { get; set; }

        public Action TargetReached { get; set; }

        public Action TargetUnreachable { get; set; }

        public float ApproachDistance { get; set; }

        public float ArrivalDistance { get; set; }

        public bool ScaleApproachVelocity { get; set; }

        public int MaxTimeWithoutDisplacement { get; set; }

        public int MaxTimeOnPath { get; set; }

        public bool AffectedByFlock { get; set; }

        public bool AffectsFlock { get; set; }

        public PathfindingBehavior(NavAgent_new agent)
        {
            Agent = agent;

            PerformPathfinding = Flechette_Engine.Pathfinding.BreadthFirst;

            ApproachDistance = 50;
            ArrivalDistance = 10;

            ScaleApproachVelocity = true;

            MaxTimeWithoutDisplacement = 3000;

            MaxTimeOnPath = 10000;

            AffectedByFlock = true;
            AffectsFlock = true;
        }

        public PathNode Pathfind()
        {
            PathNode path = null;
            bool doHybrid = false;

            // Get nodes from world positions
            PathNode currentNode = Agent.map.PointToNode(Agent.currentState.kinematicData.position, true, true);
            PathNode targetNode = Agent.map.PointToNode(Agent.targetState.kinematicData.position, false, true);

            // If the target node is invalid...
            if (targetNode == null
                || targetNode.IsOccupied)
            {
                // Find the closest node to the target position
                targetNode = Agent.map.PointToNode(Agent.targetState.kinematicData.position, true, true);

                // Set the pathfinding to allow for an off-grid move as the last waypoint
                doHybrid = true;
            }


            if (BeginPathfinding != null)
                BeginPathfinding();

            if (PerformPathfinding != null)
                path = PerformPathfinding(currentNode, targetNode);

            if (EndPathfinding != null)
                EndPathfinding();

            return path;
        }
    }
}
