﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Flechette_Engine
{
    /// <summary>
    /// A single node used for AI pathfinding
    /// </summary>
    public class PathNode
    {
        /// <summary>
        /// Worldspace coordinates of this node
        /// </summary>
        public Vector2 Location { get; set; }

        public PathNode backPointer;

        public float DistanceEstimated { get; set; }
        public float DistanceExact { get; set; }
        public float DistanceTotal { get; set; }

        public float Radius { get; set; }

        public NavMap Map { get; set; }

        public bool IsOccupied { get; set; }
        public bool IsVisited { get; set; }

        public bool WasVisitedThisCycle { get; set; }

        public List<PathNode> neighbors = new List<PathNode>();

        /// <summary>
        /// Default constructor
        /// </summary>
        public PathNode(NavMap map)
        {
            Location = Vector2.Zero;
            Map = map;
        }

        /// <summary>
        /// Vector constructor
        /// </summary>
        /// <param name="theLocation">Worldspace coordinates of this node</param>
        public PathNode(NavMap map, Vector2 theLocation, float theRadius = 0, NavMap.MapTypes theType = NavMap.MapTypes.Square)
        {
            Location = theLocation;
            Radius = theRadius;
            Map = map;
        }

        /// <summary>
        /// Point constructor
        /// </summary>
        /// <param name="theLocation">Worldspace coordinates of this node</param>
        public PathNode(NavMap map, Point theLocation, float theRadius = 0, NavMap.MapTypes theType = NavMap.MapTypes.Square)
        {
            Location = new Vector2(theLocation.X, theLocation.Y);
            Radius = theRadius;
            Map = map;
        }

        /// <summary>
        /// Component constructor
        /// </summary>
        /// <param name="theX">Worldspace X-coordinate of this node</param>
        /// <param name="theY">Worldspace Y-coordinate of this node</param>
        public PathNode(NavMap map, float theX, float theY, float theRadius = 0, NavMap.MapTypes theType = NavMap.MapTypes.Square)
        {
            Location = new Vector2(theX, theY);
            Radius = theRadius;
            Map = map;
        }

        public bool Contains(Vector2 point)
        {
            return Contains(new Point((int)point.X, (int)point.Y));
        }

        public bool Contains(int x, int y)
        {
            return Contains(new Vector2(x, y));
        }

        public bool Contains(Point point)
        {
            switch (Map.MapType)
            {
                default:
                case NavMap.MapTypes.Square:
                    Rectangle boundingBox = new Rectangle((int)(Location.X - Radius), (int)(Location.Y - Radius),
                        (int)(Radius * 2), (int)(Radius * 2));

                    if (boundingBox.Contains(point))
                    {
                        return true;
                    }
                    break;

                case NavMap.MapTypes.Hexagonal:
                    if ((new Vector2(point.X, point.Y) - Location).Length() > Radius)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }

                    break;
            }

            return false;
        }

        public bool Intersects(Rectangle rectangle)
        {
            Rectangle boundingBox = new Rectangle((int)(Location.X - Radius), (int)(Location.Y - Radius),
                        (int)(Radius * 2), (int)(Radius * 2));

            if (boundingBox.Intersects(rectangle))
            {
                return true;
            }

            return false;
        }
    }
}
