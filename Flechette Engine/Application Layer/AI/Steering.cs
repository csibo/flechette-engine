﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Flechette_Engine
{
    /// <summary>
    /// Contains functions that calculate agent steering behaviors.
    /// </summary>
    public static class Steering
    {
        /// <summary>
        /// Stores state data usable by this class' steering functions.
        /// </summary>
        public struct SteeringData
        {
            public Kinematics.KinematicData kinematicData;

            /// <summary>
            /// Direction from agent to target
            /// </summary>
            public Vector2 displacementDirection;

            /// <summary>
            /// Total distance from agent to target
            /// </summary>
            public Vector2 distance;

            /// <summary>
            /// For seeking behaviors, this is the maximum distance from target the agent will try to maintain.
            /// </summary>
            public float normalStoppingRadius;

            /// <summary>
            /// For wandering behaviors, this is the maximum distance from target the agent will try to maintain.
            /// </summary>
            public float wanderStoppingRadius;

            /// <summary>
            /// For fleeing behaviors, this is the minimum distance from target the agent will try to maintain.
            /// </summary>
            public float fleeStoppingRadius;

            /// <summary>
            /// Radius wihin which the agent will scale their max linear acceleration to avoid overshooting target.
            /// </summary>
            public float linearAccelerationDampingRadius;

            public SteeringData(int i)
            {
                kinematicData = new Kinematics.KinematicData(1);
                displacementDirection = Vector2.Zero;
                distance = Vector2.Zero;
                normalStoppingRadius = 0f;
                wanderStoppingRadius = 0f;
                fleeStoppingRadius = 0f;
                linearAccelerationDampingRadius = 0f;
            }

            public SteeringData(Vector2 target)
            {
                kinematicData = new Kinematics.KinematicData(1);
                kinematicData.position = target;
                displacementDirection = Vector2.Zero;
                distance = Vector2.Zero;
                normalStoppingRadius = 0f;
                wanderStoppingRadius = 0f;
                fleeStoppingRadius = 0f;
                linearAccelerationDampingRadius = 0f;
            }
        }

        /// <summary>
        /// Calculates straight-line path from agent towards a target.
        /// </summary>
        /// <param name="agent">Agent needing the path</param>
        /// <param name="target">Destination of agent</param>
        /// <param name="stoppingDistance">Distance at which to accept a solved solution.</param>
        /// <param name="reverse">Reverse the output to move away from target</param>
        /// <param name="allowArrivalDamping">Allow the agent to dampen their velocity to avoid overshooting the target</param>
        /// <returns>Necessary state to follow path</returns>
        public static SteeringData Move(ref SteeringData agent, ref SteeringData target, float stoppingDistance, bool reverse = false, bool allowArrivalDamping = true)
        {
            SteeringData output = new SteeringData(1);

            output.distance = target.kinematicData.position - agent.kinematicData.position;
            if (reverse)
            {
                output.distance *= -1;
            }

            output.displacementDirection = output.distance;

            if (output.displacementDirection.Length() > 0)
                output.displacementDirection.Normalize();

            output.kinematicData.maxLinearAcceleration = agent.kinematicData.maxLinearAcceleration;
            output.kinematicData.maxLinearVelocity = agent.kinematicData.maxLinearVelocity;
            output.kinematicData.linearAcceleration = output.displacementDirection * (output.kinematicData.maxLinearAcceleration * (float)GameController.gameTime.ElapsedGameTime.TotalMilliseconds);
            output.kinematicData.linearVelocity = agent.kinematicData.linearVelocity;
            output.kinematicData.position = agent.kinematicData.position;

            float intendedDisplacement = (output.kinematicData.position - target.kinematicData.position).Length();

            // If we're not within stopping distance
            if (reverse ?
                    intendedDisplacement < stoppingDistance
                : intendedDisplacement > stoppingDistance)
            {
                output.kinematicData.linearVelocity = Kinematics.UpdateLinearVelocity(ref output.kinematicData);
                output.kinematicData.position = Kinematics.UpdatePosition(ref output.kinematicData);

                output.kinematicData.orientation = (float)System.Math.Atan2(output.kinematicData.linearVelocity.X, -output.kinematicData.linearVelocity.Y);
            }
            else
            {
                output.kinematicData.linearVelocity = Vector2.Zero;

                output.kinematicData.orientation = agent.kinematicData.orientation;
            }

            output.kinematicData.orientationVector = Kinematics.SolveOrientationVector(ref output.kinematicData);

            if (allowArrivalDamping)
            {
                output = DampenArrivalVelocity(ref agent, ref output, stoppingDistance);
            }

            return output;
        }

        /// <summary>
        /// Calculates a dampened velocity for arriving at a target to avoid overshoot.
        /// </summary>
        /// <param name="agent">Agent needing the path</param>
        /// <param name="input">Unadjusted steering output</param>
        /// <param name="stoppingDistance">radius within which the agent will accept a solved solution</param>
        /// <returns>Necessary state to follow path</returns>
        public static SteeringData DampenArrivalVelocity(ref SteeringData agent, ref SteeringData input, float stoppingDistance)
        {
            SteeringData output = input;

            if (output.distance.Length() < agent.linearAccelerationDampingRadius + stoppingDistance)
            {
                output.kinematicData.linearVelocity *= output.distance.Length() / (agent.linearAccelerationDampingRadius + stoppingDistance);
            }

            return output;
        }
    }
}
