﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Flechette_Engine
{
    /// <summary>
    /// Collection of pathfinding algorithms and utilities
    /// </summary>
    public static class Pathfinding
    {
        public enum Methods
        {
            BreadthFirst,
            Dijkstra,
            BestFirst,
            aStar,
        }

        public static void ResetMap(ref NavMap map)
        {
            if (map == null)
                return;

            for (int i = 0; i < map.nodes.GetLength(0); i++)
            {
                for (int j = 0; j < map.nodes.GetLength(1); j++)
                {
                    map.nodes[i, j].DistanceEstimated = 0;
                    map.nodes[i, j].DistanceExact = 0;
                    map.nodes[i, j].DistanceTotal = 0;
                    map.nodes[i, j].backPointer = null;
                    map.nodes[i, j].IsVisited = false;
                }
            }
        }

        public static PathNode FindPath(Methods method, ref PathNode start, ref PathNode target, ref NavMap map)
        {
            if (start == null
                || target == null
                || map == null)
                return null;

            switch (method)
            {
                case Methods.BreadthFirst:
                    return BreadthFirst(ref start, ref target);
                case Methods.Dijkstra:
                    return Dijkstra(ref start, ref target);
                case Methods.BestFirst:
                    return BestFirst(ref start, ref target);
                default:
                case Methods.aStar:
                    return aStar(ref start, ref target);
            }
        }

        public static PathNode BreadthFirst(PathNode start, PathNode target)
        {
            return BreadthFirst(ref start, ref target);
        }

        public static PathNode BreadthFirst(ref PathNode start, ref PathNode target)
        {
            NavMap map = start.Map;
            Pathfinding.ResetMap(ref map);

            List<PathNode> queuedNodes = new List<PathNode>();

            start.IsVisited = true;
            start.WasVisitedThisCycle = true;
            queuedNodes.Add(start);
            while (queuedNodes.Count() > 0)
            {
                PathNode node = queuedNodes[0];
                queuedNodes.Remove(node);

                if (node == target)
                {
                    return node;
                }

                foreach (PathNode neighbor in node.neighbors)
                {
                    if (!neighbor.IsVisited)
                    {
                        neighbor.IsVisited = true;
                        neighbor.WasVisitedThisCycle = true;

                        neighbor.backPointer = node;

                        if (neighbor == target)
                        {
                            return neighbor;
                        }

                        queuedNodes.Add(neighbor);
                    }
                }
            }

            return null;
        }

        public static PathNode Dijkstra(PathNode start, PathNode target)
        {
            return Dijkstra(ref start, ref target);
        }

        public static PathNode Dijkstra(ref PathNode start, ref PathNode target)
        {
            NavMap map = start.Map;
            Pathfinding.ResetMap(ref map);

            List<PathNode> queuedNodes = new List<PathNode>();

            start.IsVisited = true;
            start.WasVisitedThisCycle = true;
            queuedNodes.Add(start);

            while (queuedNodes.Count() > 0)
            {
                PathNode node = queuedNodes[0];
                queuedNodes.Remove(node);

                if (node == target)
                {
                    return node;
                }

                foreach (PathNode neighbor in node.neighbors)
                {

                    float currentDistance = (node.DistanceExact + (neighbor.Location - node.Location).Length());

                    if (!neighbor.IsVisited)
                    {
                        neighbor.IsVisited = true;
                        neighbor.WasVisitedThisCycle = true;

                        neighbor.DistanceExact = currentDistance;

                        neighbor.backPointer = node;

                        if (neighbor == target)
                        {
                            return neighbor;
                        }

                        queuedNodes.Add(neighbor);
                    }
                    else
                    {
                        if (currentDistance < neighbor.DistanceExact)
                        {
                            neighbor.DistanceExact = currentDistance;

                            neighbor.backPointer = node;
                        }
                    }
                }

                queuedNodes = queuedNodes.OrderBy(e => e.DistanceExact).ToList<PathNode>();
            }

            return null;
        }

        public static PathNode BestFirst(PathNode start, PathNode target)
        {
            return BestFirst(ref start, ref target);
        }

        public static PathNode BestFirst(ref PathNode start, ref PathNode target)
        {
            NavMap map = start.Map;
            Pathfinding.ResetMap(ref map);

            List<PathNode> queuedNodes = new List<PathNode>();

            start.IsVisited = true;
            start.WasVisitedThisCycle = true;
            queuedNodes.Add(start);

            while (queuedNodes.Count() > 0)
            {
                PathNode node = queuedNodes[0];
                queuedNodes.Remove(node);

                if (node == target)
                {
                    return node;
                }

                foreach (PathNode neighbor in node.neighbors)
                {

                    float currentDistance = (target.Location - neighbor.Location).Length();

                    if (!neighbor.IsVisited)
                    {
                        neighbor.IsVisited = true;
                        neighbor.WasVisitedThisCycle = true;

                        neighbor.DistanceEstimated = currentDistance;

                        neighbor.backPointer = node;

                        if (neighbor == target)
                        {
                            return neighbor;
                        }

                        queuedNodes.Add(neighbor);
                    }
                    else
                    {
                        if (currentDistance < neighbor.DistanceEstimated)
                        {
                            neighbor.DistanceEstimated = currentDistance;

                            neighbor.backPointer = node;
                        }
                    }
                }

                queuedNodes = queuedNodes.OrderBy(e => e.DistanceEstimated).ToList<PathNode>();
            }

            return null;
        }

        public static PathNode aStar(PathNode start, PathNode target)
        {
            return aStar(ref start, ref target);
        }

        public static PathNode aStar(ref PathNode start, ref PathNode target)
        {
            NavMap map = start.Map;
            Pathfinding.ResetMap(ref map);

            List<PathNode> queuedNodes = new List<PathNode>();

            start.IsVisited = true;
            start.WasVisitedThisCycle = true;
            queuedNodes.Add(start);

            while (queuedNodes.Count() > 0)
            {
                PathNode node = queuedNodes[0];
                queuedNodes.Remove(node);

                if (node == target)
                    return node;

                foreach (PathNode neighbor in node.neighbors)
                {
                    //float distance_exact = (node.location - neighbor.location).Length() + node.distance_exact;
                    float distance_exact = (start.Location - neighbor.Location).Length();
                    float distance_estimated = (target.Location - neighbor.Location).Length();
                    float distance_total = distance_exact + distance_estimated;

                    if (!neighbor.IsVisited)
                    {
                        neighbor.IsVisited = true;
                        neighbor.WasVisitedThisCycle = true;

                        neighbor.DistanceEstimated = distance_estimated;
                        neighbor.DistanceExact = distance_exact;
                        neighbor.DistanceTotal = distance_total;

                        neighbor.backPointer = node;

                        if (neighbor == target)
                            return neighbor;

                        queuedNodes.Add(neighbor);
                    }
                    else if (distance_total < neighbor.DistanceTotal)
                    {
                        neighbor.DistanceEstimated = distance_estimated;
                        neighbor.DistanceExact = distance_exact;
                        neighbor.DistanceTotal = distance_total;

                        neighbor.backPointer = node;
                    }
                }

                queuedNodes = queuedNodes.OrderBy(e => e.DistanceTotal).ToList<PathNode>();
            }

            return null;
        }

        public static void DrawPath(ref PathNode node)
        {
            if (node.backPointer != null)
            {
                DrawPath(ref node.backPointer);
            }

            Primitives.DrawPoint(node.Location, 20, Color.Red);

            //EngineDaemon.physicsDebugView.BeginCustomDraw(ref EngineDaemon.projectionMatrix, ref EngineDaemon.viewMatrix);
            //EngineDaemon.physicsDebugView.DrawPoint(node.location, 20, Color.Red, false);
            //EngineDaemon.physicsDebugView.EndCustomDraw();
        }
    }
}
