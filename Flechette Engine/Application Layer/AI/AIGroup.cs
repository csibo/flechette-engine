﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using FarseerPhysics;

namespace Flechette_Engine
{
    /// <summary>
    /// Used to define NavAgent flocks.
    /// </summary>
    public class AIGroup : DrawableGameComponent
    {
        /// <summary>
        /// Resistive force applied to member agents to maintain a spacing distance.
        /// </summary>
        public float springForce = 0f;

        /// <summary>
        /// Desired max distance between an agent and at least one of its peers.
        /// </summary>
        public float maxMemberSpacing = 0f;

        /// <summary>
        /// Desired min distance between an agent and all of its peers.
        /// </summary>
        public float minMemberSpacing = 0f;

        /// <summary>
        /// Instructs the flock to maintain the min distance specified.
        /// </summary>
        public bool useMinSpacing = false;

        /// <summary>
        /// Instructs the flock to maintain the max distance specified.
        /// </summary>
        public bool useMaxSpacing = false;

        /// <summary>
        /// List of all the current members of this flock.
        /// </summary>
        public List<NavAgent> memberAgents = new List<NavAgent>();

        public AIGroup()
            : base(GameController.game)
        { }

        public override void Update(GameTime gameTime)
        {
            //EngineDaemon.navGroups++;

            DoFlocking();
        }

        public void DoFlocking()
        {
            // Calculate the group's effect on the this agent
            // only if it wants to be affected.
            foreach (NavAgent agent in memberAgents.Where(e => e.affectedByGroup))
            {
                // Only be affected by agents that are allowed to affect the group.
                foreach (NavAgent peer in memberAgents.Where(e => e.affectsGroup && e != agent))
                {
                    CalculateFlocking(agent, peer);
                }
            }
        }

        private void CalculateFlocking(NavAgent agent, NavAgent peer)
        {
            agent.SyncKinemetics();

            Vector2 difference = agent.currentState.kinematicData.position - peer.currentState.kinematicData.position;

            if (useMaxSpacing
                && difference.Length() > maxMemberSpacing)
            {
                float strength = System.Math.Min(springForce / difference.LengthSquared(), agent.currentState.kinematicData.maxLinearAcceleration);
                strength = springForce / difference.LengthSquared();
                //strength = flock.springForce * ((flock.minMemberSpacing - difference.Length()) / flock.minMemberSpacing);

                // Update state variables
                agent.currentState.kinematicData.linearAcceleration = strength * -Vector2.Normalize(difference);
                agent.currentState.kinematicData.linearVelocity = Kinematics.UpdateLinearVelocity(ref agent.currentState.kinematicData);
            }

            if (useMinSpacing
                && difference.Length() < minMemberSpacing)
            {
                float strength = System.Math.Min(springForce / difference.LengthSquared(), agent.currentState.kinematicData.maxLinearAcceleration);
                strength = springForce / difference.LengthSquared();
                //strength = flock.springForce * ((flock.minMemberSpacing - difference.Length()) / flock.minMemberSpacing);

                // Update state variables
                agent.currentState.kinematicData.linearAcceleration = strength * Vector2.Normalize(difference);
                agent.currentState.kinematicData.linearVelocity = Kinematics.UpdateLinearVelocity(ref agent.currentState.kinematicData);
            }

            agent.SyncToKinemetics();
        }
    }
}
