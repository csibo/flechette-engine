﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Flechette_Engine
{
    /// <summary>
    /// A single node used for AI pathfinding
    /// </summary>
    public class NavMap : GameObject
    {
        public enum MapTypes
        {
            Square,
            Hexagonal,
            Arbitrary,
        }

        public PathNode[,] nodes;

        protected float cellRadius;

        protected MapTypes mapType;

        protected bool allowDiagonal;

        protected Dictionary<PathNode, Vector2> nodeToArrayIndex = new Dictionary<PathNode, Vector2>();

        /// <summary>
        /// Default constructor
        /// </summary>
        public NavMap(Screen containingScreen)
            : base(containingScreen)
        {
            mapType = MapTypes.Square;

            containingScreen.aiController.navMaps.Add(this);
        }

        protected override void Dispose(bool disposing)
        {
 	         base.Dispose(disposing);
        }

        public MapTypes MapType
        {
            get
            {
                return mapType;
            }
        }

        public void GenerateArbitraryMap(PathNode[,] theNodes, Dictionary<PathNode, List<PathNode>> theEdges)
        {
            mapType = MapTypes.Arbitrary;

            for (int i = 0; i < theNodes.Length; i++)
            {
                nodeToArrayIndex.Add(theNodes[i, 0], new Vector2(i, 0));
            }

            nodes = theNodes;

            foreach (var node in theEdges)
            {
                foreach (var edge in node.Value)
                {
                    node.Key.neighbors.Add(edge);
                    edge.neighbors.Add(node.Key);
                }
            }
        }

        public void GenerateRegularMap(MapTypes desiredType, bool theAllowDiagonal, float divisionRadius = 20, float divisionCountWidth = 20)
        {
            Rectangle mapArea = new Rectangle((int)Position.X, (int)Position.Y, (int)Size.X, (int)Size.Y);
            allowDiagonal = theAllowDiagonal;

            nodes = new PathNode[1, 1];
            nodeToArrayIndex.Clear();

            if (divisionRadius > 0)
            {
                cellRadius = divisionRadius;
            }
            else
            {
                cellRadius = (float)mapArea.Width / divisionCountWidth / 2f;
            }

            switch (desiredType)
            {
                default:
                case MapTypes.Square:
                    GenerateSquareMap();
                    break;
                case MapTypes.Hexagonal:
                    GenerateHexagonalMap();
                    break;

            }
        }

        private void GenerateSquareMap()
        {
            mapType = MapTypes.Square;

            int numWidthDivisions = (int)System.Math.Floor(Size.X / (cellRadius * 2.0f));
            int numHeightDivisions = (int)System.Math.Floor(Size.Y / (cellRadius * 2.0f));

            PathNode[,] tempNodes = new PathNode[numWidthDivisions, numHeightDivisions];

            for (int i = 0; i < numWidthDivisions; i++)
            {
                for (int j = 0; j < numHeightDivisions; j++)
                {
                    PathNode tempNode = new PathNode(this,
                        Position.X + (cellRadius * 2 * i) + cellRadius, Position.Y + (cellRadius * 2 * j) + cellRadius,
                        cellRadius, MapTypes.Square);

                    tempNodes[i, j] = tempNode;
                    nodeToArrayIndex.Add(tempNode, new Vector2(i, j));
                }
            }

            nodes = tempNodes;

            RebuildEdges();
        }

        private void GenerateHexagonalMap()
        {
            mapType = MapTypes.Hexagonal;

            float xOff = (float)(System.Math.Sqrt(3) / 2) * cellRadius;
            float yOff = (3f / 4f) * cellRadius;

            int numWidthDivisions = (int)System.Math.Floor((float)(Size.X - xOff) / (xOff * 2f));
            int numHeightDivisions = (int)System.Math.Floor((float)(Size.Y - yOff) / (yOff * 2f));

            PathNode[,] tempNodes = new PathNode[numWidthDivisions, numHeightDivisions];

            for (int i = 0; i < numWidthDivisions; i++)
            {
                for (int j = 0; j < numHeightDivisions; j++)
                {
                    PathNode tempNode = null;

                    float xPos = Position.X + (xOff * 2 * i) + cellRadius;
                    float yPos = Position.Y + (yOff * 2 * j) + cellRadius;

                    if (j % 2 == 1)
                    {
                        xPos += xOff;
                    }

                    tempNode = new PathNode(this, xPos, yPos, cellRadius, MapTypes.Hexagonal);

                    tempNodes[i, j] = tempNode;
                    nodeToArrayIndex.Add(tempNode, new Vector2(i, j));
                }
            }

            nodes = tempNodes;

            RebuildEdges();
        }

        public void RebuildEdges()
        {
            foreach (PathNode node in nodes)
            {
                node.neighbors.Clear();
            }

            for (int i = 0; i < nodes.GetLength(0); i++)
            {
                for (int j = 0; j < nodes.GetLength(1); j++)
                {
                    WrapEdgeBuilder(i, j);
                }
            }

            RemoveOccupiedEdges();
        }

        private void WrapEdgeBuilder(int i, int j)
        {
            if (nodes[i, j].IsOccupied)
                return;

            bool hasLeft = false;
            bool hasRight = false;
            bool hasUp = false;
            bool hasDown = false;

            if (i > 0)
            {
                hasLeft = true;
            }

            if (i < nodes.GetLength(0) - 1)
            {
                hasRight = true;
            }

            if (j > 0)
            {
                hasUp = true;
            }

            if (j < nodes.GetLength(1) - 1)
            {
                hasDown = true;
            }

            switch (mapType)
            {
                default:
                case MapTypes.Square:
                    BuildSquareEdges(i, j, hasLeft, hasRight, hasUp, hasDown);
                    break;
                case MapTypes.Hexagonal:
                    BuildHexagonalEdges(i, j, hasLeft, hasRight, hasUp, hasDown);
                    break;
            }
        }

        private void BuildSquareEdges(int i, int j, bool hasLeft, bool hasRight, bool hasUp, bool hasDown)
        {
            PathNode node = nodes[i, j];

            if (hasLeft
                && !nodes[i - 1, j].IsOccupied)
            {
                node.neighbors.Add(nodes[i - 1, j]);
            }

            if (hasRight)
            {
                if (!nodes[i + 1, j].IsOccupied)
                    node.neighbors.Add(nodes[i + 1, j]);

                if (allowDiagonal)
                {
                    if (hasUp
                        && !nodes[i + 1, j - 1].IsOccupied)
                    {
                        node.neighbors.Add(nodes[i + 1, j - 1]);
                    }

                    if (hasDown
                        && !nodes[i + 1, j + 1].IsOccupied)
                    {
                        node.neighbors.Add(nodes[i + 1, j + 1]);
                    }
                }
            }

            if (hasUp
                && !nodes[i, j - 1].IsOccupied)
            {
                node.neighbors.Add(nodes[i, j - 1]);
            }

            if (hasDown
                && !nodes[i, j + 1].IsOccupied)
            {
                node.neighbors.Add(nodes[i, j + 1]);
            }
        }

        private void BuildHexagonalEdges(int i, int j, bool hasLeft, bool hasRight, bool hasUp, bool hasDown)
        {
            PathNode node = nodes[i, j];

            if (hasLeft
                && !nodes[i - 1, j].IsOccupied)
            {
                node.neighbors.Add(nodes[i - 1, j]);
            }

            if (hasRight)
            {
                if (!nodes[i + 1, j].IsOccupied)
                    node.neighbors.Add(nodes[i + 1, j]);

                if (allowDiagonal
                    && j % 2 == 1)
                {
                    if (hasUp
                        && !nodes[i + 1, j - 1].IsOccupied)
                    {
                        node.neighbors.Add(nodes[i + 1, j - 1]);
                    }

                    if (hasDown
                        && !nodes[i + 1, j + 1].IsOccupied)
                    {
                        node.neighbors.Add(nodes[i + 1, j + 1]);
                    }
                }
            }

            if (hasUp
                && !nodes[i, j - 1].IsOccupied)
            {
                node.neighbors.Add(nodes[i, j - 1]);
            }

            if (hasDown
                && !nodes[i, j + 1].IsOccupied)
            {
                node.neighbors.Add(nodes[i, j + 1]);
            }
        }

        public void RemoveOccupiedEdges()
        {
            for (int i = 0; i < nodes.GetLength(0); i++)
            {
                for (int j = 0; j < nodes.GetLength(1); j++)
                {
                    if (nodes[i, j].IsOccupied)
                    {
                        nodes[i, j].neighbors.Clear();
                    }
                }
            }
        }

        public void ReaddUnoccupiedEdges()
        {
            for (int i = 0; i < nodes.GetLength(0); i++)
            {
                for (int j = 0; j < nodes.GetLength(1); j++)
                {
                    if (!nodes[i, j].IsOccupied)
                    {
                        WrapEdgeBuilder(i, j);
                    }
                }
            }
        }

        public PathNode PointToNode(Vector2 point, bool allowClosest = false, bool forceUnoccupied = false)
        {
            float xOff;
            float yOff;

            if (mapType == MapTypes.Square)
            {
                xOff = cellRadius * 2;
                yOff = cellRadius * 2;
            }
            else
            {
                xOff = (float)(System.Math.Sqrt(3) / 2) * cellRadius * 2;
                yOff = (3f / 4f) * cellRadius * 2;
            }

            // Account for map offsets
            Vector2 adjustedPoint = point - Position;

            // Rows are never offset so can be calculated as a straight percentage
            int row = (int)System.Math.Floor(adjustedPoint.Y / yOff);

            // Columns might be offset so must be calculated as a percentage + offset
            float fcol = (adjustedPoint.X / xOff);

            if (mapType == MapTypes.Hexagonal)
            {
                fcol -= (row % 2 == 1 ? 0.5f : 0f);
            }

            int col = (int)System.Math.Floor(fcol);
            col = System.Math.Max(col, 0);

            //Console.WriteLine("Point=("+point.X+","+point.Y+"), Node=["+col+","+row+"]");

            if (row < nodes.GetLength(1) && row >= 0
                && col < nodes.GetLength(0) && col >= 0)
            {
                if (!(forceUnoccupied && nodes[col, row].IsOccupied))
                    return nodes[col, row];
            }

            if (allowClosest)
                return FindClosestNode(point, forceUnoccupied);

            return null;
        }

        public Vector2 NodeToArrayIndex(PathNode node)
        {
            if (node != null
                && nodeToArrayIndex.ContainsKey(node))
            {
                return nodeToArrayIndex[node];
            }

            return new Vector2(float.NegativeInfinity, float.NegativeInfinity);
        }

        public PathNode FindClosestNode(Vector2 point, bool forceUnoccupied = false)
        {
            float minDistance = float.PositiveInfinity;
            PathNode closest = null;

            foreach (PathNode node in nodes)
            {
                if ((point - node.Location).Length() < minDistance + cellRadius)
                {
                    if (forceUnoccupied && node.IsOccupied)
                        continue;

                    minDistance = (point - node.Location).Length();
                    closest = node;
                }
            }

            return closest;
        }

        public PathNode FindClosestReachableNode(PathNode start, PathNode target, bool forceUnoccupied = false)
        {
            float minDistance = float.PositiveInfinity;
            PathNode closest = null;

            return closest;
        }

        public List<PathNode> Footprint(Rectangle rectangle)
        {
            List<PathNode> footprint = new List<PathNode>();

            Vector2 topLeftCorner = NodeToArrayIndex(PointToNode(new Vector2(rectangle.X, rectangle.Y), false, false));
            Vector2 bottomRightCorner = NodeToArrayIndex(PointToNode(new Vector2(rectangle.X + rectangle.Width, rectangle.Y + rectangle.Height), false, false));

            if (topLeftCorner.X == float.NegativeInfinity)
            {
                topLeftCorner = Vector2.Zero;
            }
            if (bottomRightCorner.X == float.NegativeInfinity)
            {
                bottomRightCorner = new Vector2(nodes.GetLength(0) - 1, nodes.GetLength(1) - 1);
            }

            if (topLeftCorner.X != float.NegativeInfinity
                && bottomRightCorner.X != float.NegativeInfinity)
            {
                for (int i = (int)System.Math.Max(topLeftCorner.X - 1, 0); i <= bottomRightCorner.X; i++)
                {
                    for (int j = (int)System.Math.Max(topLeftCorner.Y - 1, 0); j <= bottomRightCorner.Y; j++)
                    {
                        if (nodes[i, j].Intersects(rectangle))
                        {
                            footprint.Add(nodes[i, j]);
                        }
                    }
                }
            }

            return footprint;
        }

        public override void UpdatePrepass()
        {
            if (Enabled)
            {
                if (mapType != MapTypes.Arbitrary)
                    RebuildEdges();
            }

            base.UpdatePrepass();
        }

        public override void Draw()
        {
            if (Visible)
            {
                if (ContainingScreen.aiController.DebugEnabled)
                {
                    Vector2[] vertices = null;

                    foreach (PathNode node in nodes)
                    {
                        if (mapType == MapTypes.Square)
                        {
                            bool useTriangles = false;

                            if (useTriangles)
                            {
                                vertices = new Vector2[6];

                                vertices[0] = new Vector2(node.Location.X - cellRadius, node.Location.Y - cellRadius);
                                vertices[1] = new Vector2(node.Location.X + cellRadius, node.Location.Y - cellRadius);
                                vertices[2] = new Vector2(node.Location.X + cellRadius, node.Location.Y + cellRadius);

                                vertices[3] = new Vector2(node.Location.X - cellRadius, node.Location.Y - cellRadius);
                                vertices[4] = new Vector2(node.Location.X + cellRadius, node.Location.Y + cellRadius);
                                vertices[5] = new Vector2(node.Location.X - cellRadius, node.Location.Y + cellRadius);
                            }
                            else
                            {
                                vertices = new Vector2[4];

                                vertices[0] = new Vector2(node.Location.X - cellRadius, node.Location.Y - cellRadius);
                                vertices[1] = new Vector2(node.Location.X + cellRadius, node.Location.Y - cellRadius);
                                vertices[2] = new Vector2(node.Location.X + cellRadius, node.Location.Y + cellRadius);
                                vertices[3] = new Vector2(node.Location.X - cellRadius, node.Location.Y + cellRadius);
                            }
                        }
                        else if (mapType == MapTypes.Hexagonal)
                        {
                            vertices = new Vector2[6];

                            for (int i = 0; i < vertices.Count(); i++)
                            {
                                float angle = (float)(2f * System.Math.PI * ((float)i + 0.5f) / 6f);

                                vertices[i] = new Vector2(node.Location.X + (cellRadius * (float)System.Math.Cos(angle)),
                                    node.Location.Y + (cellRadius * (float)System.Math.Sin(angle)));
                            }
                        }

                        if(vertices != null)
                        {
                            Primitives.DrawPolygon(vertices, node.WasVisitedThisCycle ? Color.Yellow : Color.Cyan);
                        }

                        foreach (PathNode neighbor in node.neighbors)
                        {
                            Primitives.DrawSegment(node.Location, neighbor.Location, Color.Purple);
                        }
                    }
                }

                foreach (PathNode node in nodes)
                {
                    node.WasVisitedThisCycle = false;
                }
            }
        }
    }
}
