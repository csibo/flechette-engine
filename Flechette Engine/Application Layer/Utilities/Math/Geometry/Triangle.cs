﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Flechette_Engine
{
    public class Triangle
    {
        public enum Vertices
        {
            Vertex1,
            Vertex2,
            Vertex3,
        }

        public Vector2 Vertex1 { get; set; }

        public Vector2 Vertex2 { get; set; }

        public Vector2 Vertex3 { get; set; }

        public Triangle()
        {
            Vertex1 = Vector2.Zero;
            Vertex2 = Vector2.Zero;
            Vertex3 = Vector2.Zero;
        }

        public Triangle(Vector2 point1, Vector2 point2, Vector2 point3)
        {
            Vertex1 = point1;
            Vertex2 = point2;
            Vertex3 = point3;
        }

        public float MeasureAngle(Vertices vertex)
        {
            Vector2 A = Vector2.Zero;
            Vector2 B = Vector2.Zero;

            switch(vertex)
            {
                case Vertices.Vertex1:
                    A = Vertex2 - Vertex1;
                    B = Vertex3 - Vertex1;
                    break;
                case Vertices.Vertex2:
                    A = Vertex1 - Vertex2;
                    B = Vertex3 - Vertex2;
                    break;
                case Vertices.Vertex3:
                    A = Vertex1 - Vertex3;
                    B = Vertex2 - Vertex3;
                    break;
            }

            float xi = A.X * B.X;
            float yi = A.Y * B.Y;
            float AB = xi + yi;

            float Amag = A.Length();
            float Bmag = B.Length();
            float Amag_BMag = Amag * Bmag;

            return (float)Math.Acos(AB / Amag_BMag);
        }
    }
}
