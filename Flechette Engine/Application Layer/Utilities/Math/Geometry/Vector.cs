﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flechette_Engine
{
    /// <summary>
    /// Vector of arbitrary order.
    /// </summary>
    public class Vector : IEnumerable
    {
        #region Component Properties

        /// <summary>
        /// Component 0
        /// </summary>
        public float X { get { return At(0); } set { At(0, value); } }

        /// <summary>
        /// Component 1
        /// </summary>
        public float Y { get { return At(1); } set { At(1, value); } }

        /// <summary>
        /// Component 2
        /// </summary>
        public float Z { get { return At(2); } set { At(2, value); } }

        /// <summary>
        /// Component 3
        /// </summary>
        public float U { get { return At(3); } set { At(3, value); } }

        /// <summary>
        /// Component 4
        /// </summary>
        public float V { get { return At(4); } set { At(4, value); } }

        /// <summary>
        /// Component 5
        /// </summary>
        public float W { get { return At(5); } set { At(5, value); } }

        #endregion

        /// <summary>
        /// List of the member components.
        /// </summary>
        public List<float> Components { get; set; }

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public Vector()
        {
            Components = new List<float>();
        }

        /// <summary>
        /// Intialized constructor
        /// </summary>
        /// <param name="componenets">Components to assign this vector.</param>
        public Vector(List<float> componenets)
        {
            Components = componenets;
        }

        /// <summary>
        /// Copy Constructor.
        /// </summary>
        /// <param name="vector">Vector to copy.</param>
        public Vector(Vector vector)
        {
            Components = vector.Components;
        }

        /// <summary>
        /// Order constructor. Components initialize to 0.
        /// </summary>
        /// <param name="order">Number of components to initialize.</param>
        public Vector(float order)
        {
            Components = new List<float>();
            
            for(int i=0;i<order;i++)
            {
                Components.Add(0f);
            }
        }

        #endregion

        #region Measurements

        /// <summary>
        /// Gets the magnitude of the vector.
        /// </summary>
        /// <returns>Magnitude of the vector.</returns>
        public float Length()
        {
            return (float)Math.Sqrt(LengthSquared());
        }

        /// <summary>
        /// Gets the magnitude of the vector squared.
        /// </summary>
        /// <returns>Magnitude of the vector squared.</returns>
        public float LengthSquared()
        {
            // Length^2 = the sum of the square of each component
            return CumulativeOperation(this, 0f, (result, comp) => { return result += comp * comp; });
        }

        /// <summary>
        /// Returns the number of components in the vector.
        /// </summary>
        /// <returns>Number of components in the vector.</returns>
        public int Count()
        {
            if (Components != null)
            {
                return Components.Count();
            }

            return 0;
        }

        /// <summary>
        /// Gets the value of the component at the specified index.
        /// Returns 0 if the index doesn't exist.
        /// </summary>
        /// <param name="index">Index of the component to return.</param>
        /// <returns>Value of the index.</returns>
        public float At(int index)
        {
            if(index < Count())
            {
                return Components[index];
            }

            return 0f;
        }

        /// <summary>
        /// Sets the component at the specified index to the specified value.
        /// Expands the vector to match the order of the index if necessary.
        /// Intermediate values will be initialized to 0.
        /// </summary>
        /// <param name="index">Index of the component to set.</param>
        /// <param name="value">Value at which to set the component.</param>
        public void At(int index, float value)
        {
            if (index < Count())
            {
                Components[index] = value;
            }
            else
            {
                while(index > Count())
                {
                    Components.Add(0f);
                }

                Components.Add(value);
            }
        }

        /// <summary>
        /// Forwarder for Components.GetEnumerator()
        /// </summary>
        /// <returns>Components.GetEnumerator()</returns>
        public IEnumerator GetEnumerator()
        {
            return Components.GetEnumerator();
        }

        #endregion

        /// <summary>
        /// Normalizes a vector.
        /// </summary>
        /// <param name="vector">The input vector.</param>
        /// <returns>Normalized vector.</returns>
        public static Vector Normalize(Vector vector)
        {
            float mag = vector.Length();

            if (mag != 0)
            {
                return IncrementalOperation(vector, (comp) => { return comp / mag; });
            }

            return new Vector(vector.Count());
        }

        /// <summary>
        /// Normalizes this vector.
        /// </summary>
        /// <returns>Normalized vector.</returns>
        public Vector Normalize()
        {
            return Vector.Normalize(this);
        }

        /// <summary>
        /// Restricts the vector's values between the specified limits.
        /// </summary>
        /// <param name="min">Minimum for respective components.</param>
        /// <param name="max">Maximum for respective components.</param>
        /// <returns>Gated vector.</returns>
        public void Clamp(Vector min, Vector max)
        {
            Components = MathHelper.Clamp(this, min, max).Components;
        }

        /// <summary>
        /// Restricts the vector's values between the specified limits.
        /// </summary>
        /// <param name="min">Minimum for every component.</param>
        /// <param name="max">Maximum for every component.</param>
        /// <returns>Gated vector.</returns>
        public void Clamp(float min, float max)
        {
            Components = MathHelper.Clamp(this, min, max).Components;
        }

        #region Component-Wise Operations

        /// <summary>
        /// Applies a user-supplied operation to each component from a vector.
        /// </summary>
        /// <param name="subject">Input vector.</param>
        /// <param name="operation">Code to execute on each component with (component) as parameter.</param>
        /// <returns>The resultant vector of the operations.</returns>
        public static Vector IncrementalOperation(Vector subject, Func<float, float> operation)
        {
            Vector result = new Vector();

            if (subject.Components != null)
            {
                for (int i = 0; i < subject.Count(); i++)
                {
                    result.Components.Add(operation(subject.Components[i]));
                }
            }

            return result;
        }

        /// <summary>
        /// Applies a user-supplied operation to each component from a vector and a running result.
        /// </summary>
        /// <param name="subject">Input vector.</param>
        /// <param name="initialResult">The initial value for the running result.</param>
        /// <param name="operation">Code to execute on the component with (running result, component) as parameters.</param>
        /// <returns>The running result of the operations.</returns>
        public static T CumulativeOperation<T>(Vector subject, T initialResult, Func<T, float, T> operation)
        {
            T result = initialResult;

            if (subject.Components != null)
            {
                for (int i = 0; i < subject.Count(); i++)
                {
                    result = operation(result, subject.Components[i]);
                }
            }

            return result;
        }

        /// <summary>
        /// Applies a user-supplied operation to each pair of components from
        /// two vectors. The resultant vector will have the same number of
        /// components as the larger vector. Missing components from the smaller
        /// will be replaced with 0's for the operation.
        /// </summary>
        /// <param name="left">Input vector of any order.</param>
        /// <param name="right">Input vector of any order.</param>
        /// <param name="operation">Code to execute on each pair with (left component, right component) as parameters.</param>
        /// <returns>The resultant vector of the operations.</returns>
        public static Vector PairOperation(Vector left, Vector right, Func<float, float, float> operation)
        {
            Vector result = new Vector();

            if (left.Components != null
                && right.Components != null)
            {
                for (int i = 0; i < Math.Max(left.Count(), right.Count()); i++)
                {
                    float leftComp = 0;
                    float rightComp = 0;

                    if (left.Count() > i)
                    {
                        leftComp = left.Components[i];
                    }

                    if (right.Count() > i)
                    {
                        rightComp = right.Components[i];
                    }

                    result.Components.Add(operation(leftComp, rightComp));
                }
            }

            return result;
        }

        /// <summary>
        /// Applies a user-supplied operation to each pair of components from
        /// two vectors. The resultant vector will have the same number of
        /// components as the larger vector. Missing components from the smaller
        /// will be replaced with 0's for the operation.
        /// </summary>
        /// <param name="left">Input vector of any order.</param>
        /// <param name="right">Input vector of any order.</param>
        /// <param name="operation">Code to execute on each pair with (left component, right component) as parameters.</param>
        /// <returns>The resultant vector of the operations.</returns>
        public static T PairOperation<T>(Vector left, Vector right, T initialResult, T exitResult, Func<float, float, T> operation) where T : IEquatable<T>
        {
            T result = initialResult;

            if (left.Components != null
                && right.Components != null)
            {
                for (int i = 0; i < Math.Max(left.Count(), right.Count()); i++)
                {
                    float leftComp = 0;
                    float rightComp = 0;

                    if (left.Count() > i)
                    {
                        leftComp = left.Components[i];
                    }

                    if (right.Count() > i)
                    {
                        rightComp = right.Components[i];
                    }

                    result = operation(leftComp, rightComp);

                    if(result.Equals(exitResult))
                    {
                        return result;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Applies a user-supplied operation to each pair of components from
        /// two vectors with a running result.
        /// </summary>
        /// <param name="left">Input vector of any order.</param>
        /// <param name="right">Input vector of any order.</param>
        /// <param name="operation">Code to execute on each pair with (running result, left component, right component) as parameters.</param>
        /// <returns>The running result of the operations.</returns>
        public static float CumulativePairOperation(Vector left, Vector right, Func<float, float, float, float> operation)
        {
            float result = 0f;

            if (left.Components != null
                && right.Components != null)
            {
                for (int i = 0; i < Math.Max(left.Count(), right.Count()); i++)
                {
                    float leftComp = 0;
                    float rightComp = 0;

                    if (left.Count() > i)
                    {
                        leftComp = left.Components[i];
                    }

                    if (right.Count() > i)
                    {
                        rightComp = right.Components[i];
                    }

                    result = operation(result, leftComp, rightComp);
                }
            }

            return result;
        }

        #endregion

        #region Operators

        #region Operator *

        /// <summary>
        /// Calculates the dot product of two Vectors. 
        /// The resultant vector will have the same number of components
        /// as the larger of the input vectors. Missing components will
        /// be replaced with 0.
        /// </summary>
        /// <param name="left">Input vector of any order.</param>
        /// <param name="right">Input vector of any order.</param>
        /// <returns>Dot product.</returns>
        public static float operator *(Vector left, Vector right)
        {
            return CumulativePairOperation(left, right, (result, comp1, comp2) => { return result += comp1 * comp2; });
        }

        /// <summary>
        /// Multiplies a vector by a scalar.
        /// </summary>
        /// <param name="left">Input vector.</param>
        /// <param name="right">Input scalar.</param>
        /// <returns>Scaled vector.</returns>
        public static Vector operator *(Vector vector, float scalar)
        {
            return IncrementalOperation(vector, (comp) => { return comp * scalar; });
        }

        /// <summary>
        /// Multiplies a vector by a scalar.
        /// </summary>
        /// <param name="left">Input vector.</param>
        /// <param name="right">Input scalar.</param>
        /// <returns>Scaled vector.</returns>
        public static Vector operator *(Vector vector, int scalar)
        {
            return IncrementalOperation(vector, (comp) => { return comp * scalar; });
        }

        /// <summary>
        /// Multiplies a vector by a scalar.
        /// </summary>
        /// <param name="left">Input vector.</param>
        /// <param name="right">Input scalar.</param>
        /// <returns>Scaled vector.</returns>
        public static Vector operator *(Vector vector, double scalar)
        {
            return IncrementalOperation(vector, (comp) => { return (float)(comp * scalar); });
        }

        #endregion

        #region Operator /

        /// <summary>
        /// Divides each pair of components of two vectors.
        /// The resultant vector will have the same number of components
        /// as the larger of the input vectors. Missing components will
        /// be replaced with 0.
        /// </summary>
        /// <param name="left">Input vector of any order.</param>
        /// <param name="right">Input vector of any order.</param>
        /// <returns>Resultant vector.</returns>
        public static Vector operator /(Vector left, Vector right)
        {
            return PairOperation(left, right, (comp1, comp2) => { return comp1 / comp2; });
        }

        /// <summary>
        /// Divides a vector by a scalar.
        /// </summary>
        /// <param name="left">Input vector.</param>
        /// <param name="right">Input scalar.</param>
        /// <returns>Scaled vector.</returns>
        public static Vector operator /(Vector vector, float scalar)
        {
            return IncrementalOperation(vector, (comp) => { return comp / scalar; });
        }

        /// <summary>
        /// Divides a vector by a scalar.
        /// </summary>
        /// <param name="left">Input vector.</param>
        /// <param name="right">Input scalar.</param>
        /// <returns>Scaled vector.</returns>
        public static Vector operator /(Vector vector, int scalar)
        {
            return IncrementalOperation(vector, (comp) => { return comp / (float)scalar; });
        }

        /// <summary>
        /// Divides a vector by a scalar.
        /// </summary>
        /// <param name="left">Input vector.</param>
        /// <param name="right">Input scalar.</param>
        /// <returns>Scaled vector.</returns>
        public static Vector operator /(Vector vector, double scalar)
        {
            return IncrementalOperation(vector, (comp) => { return (float)(comp / scalar); });
        }

        #endregion

        #region Operator +

        public static Vector operator +(Vector left, Vector right)
        {
            return PairOperation(left, right, (comp1, comp2) => { return comp1 + comp2; });
        }

        /// <summary>
        /// Adds a scalar to each component of a vector.
        /// </summary>
        /// <param name="left">Input vector.</param>
        /// <param name="right">Input scalar.</param>
        /// <returns>Resultant vector.</returns>
        public static Vector operator +(Vector vector, float scalar)
        {
            return IncrementalOperation(vector, (comp) => { return comp + scalar; });
        }

        /// <summary>
        /// Adds a scalar to each component of a vector.
        /// </summary>
        /// <param name="left">Input vector.</param>
        /// <param name="right">Input scalar.</param>
        /// <returns>Resultant vector.</returns>
        public static Vector operator +(Vector vector, double scalar)
        {
            return IncrementalOperation(vector, (comp) => { return comp + (float)scalar; });
        }

        /// <summary>
        /// Adds a scalar to each component of a vector.
        /// </summary>
        /// <param name="left">Input vector.</param>
        /// <param name="right">Input scalar.</param>
        /// <returns>Resultant vector.</returns>
        public static Vector operator +(Vector vector, int scalar)
        {
            return IncrementalOperation(vector, (comp) => { return comp + scalar; });
        }

        #endregion

        #region Operator -

        public static Vector operator -(Vector left, Vector right)
        {
            return PairOperation(left, right, (comp1, comp2) => { return comp1 - comp2; });
        }

        /// <summary>
        /// Subtracts a scalar from each component of a vector.
        /// </summary>
        /// <param name="left">Input vector.</param>
        /// <param name="right">Input scalar.</param>
        /// <returns>Resultant vector.</returns>
        public static Vector operator -(Vector vector, float scalar)
        {
            return IncrementalOperation(vector, (comp) => { return comp - scalar; });
        }

        /// <summary>
        /// Subtracts a scalar from each component of a vector.
        /// </summary>
        /// <param name="left">Input vector.</param>
        /// <param name="right">Input scalar.</param>
        /// <returns>Resultant vector.</returns>
        public static Vector operator -(Vector vector, double scalar)
        {
            return IncrementalOperation(vector, (comp) => { return comp - (float)scalar; });
        }

        /// <summary>
        /// Subtracts a scalar from each component of a vector.
        /// </summary>
        /// <param name="left">Input vector.</param>
        /// <param name="right">Input scalar.</param>
        /// <returns>Resultant vector.</returns>
        public static Vector operator -(Vector vector, int scalar)
        {
            return IncrementalOperation(vector, (comp) => { return comp - scalar; });
        }

        /// <summary>
        /// Inverts a vector.
        /// </summary>
        /// <param name="vector">Input vector.</param>
        /// <returns>Resultant vector.</returns>
        public static Vector operator -(Vector vector)
        {
            return IncrementalOperation(vector, (comp) => { return 0 - comp; });
        }

        #endregion

        #region Operator ==

        public static bool operator ==(Vector left, Vector right)
        {
            return PairOperation(left, right, false, false, (comp1, comp2) => { return comp1 == comp2; });
        }

        /// <summary>
        /// Checks if a vector's length is equal to a scalar.
        /// </summary>
        /// <param name="left">Input vector.</param>
        /// <param name="right">Input scalar.</param>
        /// <returns>True if vector length equals the scalar.</returns>
        public static bool operator ==(Vector vector, float scalar)
        {
            return vector.LengthSquared() == scalar * scalar;
        }

        /// <summary>
        /// Checks if a vector's length is equal to a scalar.
        /// </summary>
        /// <param name="left">Input vector.</param>
        /// <param name="right">Input scalar.</param>
        /// <returns>True if vector length equals the scalar.</returns>
        public static bool operator ==(Vector vector, int scalar)
        {
            return vector.LengthSquared() == scalar * scalar;
        }

        /// <summary>
        /// Checks if a vector's length is equal to a scalar.
        /// </summary>
        /// <param name="left">Input vector.</param>
        /// <param name="right">Input scalar.</param>
        /// <returns>True if vector length equals the scalar.</returns>
        public static bool operator ==(Vector vector, double scalar)
        {
            return vector.LengthSquared() == (float)(scalar * scalar);
        }

        public override bool Equals(object obj)
        {
            return this == obj;
        }

        #endregion

        #region Operator !=

        public static bool operator !=(Vector left, Vector right)
        {
            return !(left == right);
        }

        /// <summary>
        /// Checks if a vector's length is not equal to a scalar.
        /// </summary>
        /// <param name="left">Input vector.</param>
        /// <param name="right">Input scalar.</param>
        /// <returns>True if vector length equals the scalar.</returns>
        public static bool operator !=(Vector vector, float scalar)
        {
            return !(vector == scalar);
        }

        /// <summary>
        /// Checks if a vector's length is not equal to a scalar.
        /// </summary>
        /// <param name="left">Input vector.</param>
        /// <param name="right">Input scalar.</param>
        /// <returns>True if vector length equals the scalar.</returns>
        public static bool operator !=(Vector vector, int scalar)
        {
            return !(vector == scalar);
        }

        /// <summary>
        /// Checks if a vector's length is not equal to a scalar.
        /// </summary>
        /// <param name="left">Input vector.</param>
        /// <param name="right">Input scalar.</param>
        /// <returns>True if vector length equals the scalar.</returns>
        public static bool operator !=(Vector vector, double scalar)
        {
            return !(vector == scalar);
        }

        #endregion

        #region Operator ^

        /// <summary>
        /// Returns the angle between two vectors in the plane containing them both.
        /// </summary>
        /// <param name="A">Vector A.</param>
        /// <param name="B">Vector B.</param>
        /// <returns>Angle in radians.</returns>
        public static float operator ^(Vector A, Vector B)
        {
            float AB = A * B;

            float Amag_BMag = A.Length() * B.Length();

            // theta = acos( (A*B) / (|A|*|B|) )
            return (float)Math.Acos(AB / Amag_BMag);
        }

        #endregion

        #region Operator <

        /// <summary>
        /// Checks if one vector's length is less than another's.
        /// </summary>
        /// <param name="left">Vector of any order.</param>
        /// <param name="right">Vector of any order.</param>
        /// <returns>True if left.Length() < right.Length()</returns>
        public static bool operator <(Vector left, Vector right)
        {
            return (left.LengthSquared() < right.LengthSquared());
        }

        /// <summary>
        /// Checks if one vector's length is less than a scalar.
        /// </summary>
        /// <param name="vector">Vector of any order.</param>
        /// <param name="scalar">Scalar.</param>
        /// <returns>True if vector.Length() < scalar</returns>
        public static bool operator <(Vector vector, float scalar)
        {
            return (vector.LengthSquared() < (scalar * scalar));
        }

        /// <summary>
        /// Checks if one vector's length is less than a scalar.
        /// </summary>
        /// <param name="vector">Vector of any order.</param>
        /// <param name="scalar">Scalar.</param>
        /// <returns>True if vector.Length() < scalar</returns>
        public static bool operator <(Vector vector, int scalar)
        {
            return (vector.LengthSquared() < (scalar * scalar));
        }

        /// <summary>
        /// Checks if one vector's length is less than a scalar.
        /// </summary>
        /// <param name="vector">Vector of any order.</param>
        /// <param name="scalar">Scalar.</param>
        /// <returns>True if vector.Length() < double</returns>
        public static bool operator <(Vector vector, double scalar)
        {
            return (vector.LengthSquared() < (float)(scalar * scalar));
        }

        #endregion

        #region Operator >

        /// <summary>
        /// Checks if one vector's length is greater than another's.
        /// </summary>
        /// <param name="left">Vector of any order.</param>
        /// <param name="right">Vector of any order.</param>
        /// <returns>True if left.Length() > right.Length()</returns>
        public static bool operator >(Vector left, Vector right)
        {
            return (left.LengthSquared() > right.LengthSquared());
        }

        /// <summary>
        /// Checks if one vector's length is greater than a scalar.
        /// </summary>
        /// <param name="vector">Vector of any order.</param>
        /// <param name="scalar">Scalar.</param>
        /// <returns>True if vector.Length() > scalar</returns>
        public static bool operator >(Vector vector, float scalar)
        {
            return (vector.LengthSquared() > (scalar * scalar));
        }

        /// <summary>
        /// Checks if one vector's length is greater than a scalar.
        /// </summary>
        /// <param name="vector">Vector of any order.</param>
        /// <param name="scalar">Scalar.</param>
        /// <returns>True if vector.Length() > scalar</returns>
        public static bool operator >(Vector vector, int scalar)
        {
            return (vector.LengthSquared() > (scalar * scalar));
        }

        /// <summary>
        /// Checks if one vector's length is greater than a scalar.
        /// </summary>
        /// <param name="vector">Vector of any order.</param>
        /// <param name="scalar">Scalar.</param>
        /// <returns>True if vector.Length() > double</returns>
        public static bool operator >(Vector vector, double scalar)
        {
            return (vector.LengthSquared() > (float)(scalar * scalar));
        }

        #endregion

        #region Operator <=

        /// <summary>
        /// Checks if one vector's length is less than or equal to another's.
        /// </summary>
        /// <param name="left">Vector of any order.</param>
        /// <param name="right">Vector of any order.</param>
        /// <returns>True if left.Length() <= right.Length()</returns>
        public static bool operator <=(Vector left, Vector right)
        {
            return (left.LengthSquared() <= right.LengthSquared());
        }

        /// <summary>
        /// Checks if one vector's length is less than or equal to a scalar.
        /// </summary>
        /// <param name="vector">Vector of any order.</param>
        /// <param name="scalar">Scalar.</param>
        /// <returns>True if vector.Length() <= scalar</returns>
        public static bool operator <=(Vector vector, float scalar)
        {
            return (vector.LengthSquared() <= (scalar * scalar));
        }

        /// <summary>
        /// Checks if one vector's length is less than or equal to a scalar.
        /// </summary>
        /// <param name="vector">Vector of any order.</param>
        /// <param name="scalar">Scalar.</param>
        /// <returns>True if vector.Length() <= scalar</returns>
        public static bool operator <=(Vector vector, int scalar)
        {
            return (vector.LengthSquared() <= (scalar * scalar));
        }

        /// <summary>
        /// Checks if one vector's length is less than or equal to a scalar.
        /// </summary>
        /// <param name="vector">Vector of any order.</param>
        /// <param name="scalar">Scalar.</param>
        /// <returns>True if vector.Length() <= double</returns>
        public static bool operator <=(Vector vector, double scalar)
        {
            return (vector.LengthSquared() <= (float)(scalar * scalar));
        }

        #endregion

        #region Operator >=

        /// <summary>
        /// Checks if one vector's length is greater than or equal to another's.
        /// </summary>
        /// <param name="left">Vector of any order.</param>
        /// <param name="right">Vector of any order.</param>
        /// <returns>True if left.Length() >= right.Length()</returns>
        public static bool operator >=(Vector left, Vector right)
        {
            return (left.LengthSquared() >= right.LengthSquared());
        }

        /// <summary>
        /// Checks if one vector's length is greater than or equal to a scalar.
        /// </summary>
        /// <param name="vector">Vector of any order.</param>
        /// <param name="scalar">Scalar.</param>
        /// <returns>True if vector.Length() >= scalar</returns>
        public static bool operator >=(Vector vector, float scalar)
        {
            return (vector.LengthSquared() >= (scalar * scalar));
        }

        /// <summary>
        /// Checks if one vector's length is greater than or equal to a scalar.
        /// </summary>
        /// <param name="vector">Vector of any order.</param>
        /// <param name="scalar">Scalar.</param>
        /// <returns>True if vector.Length() >= scalar</returns>
        public static bool operator >=(Vector vector, int scalar)
        {
            return (vector.LengthSquared() >= (scalar * scalar));
        }

        /// <summary>
        /// Checks if one vector's length is greater than or equal to a scalar.
        /// </summary>
        /// <param name="vector">Vector of any order.</param>
        /// <param name="scalar">Scalar.</param>
        /// <returns>True if vector.Length() >= double</returns>
        public static bool operator >=(Vector vector, double scalar)
        {
            return (vector.LengthSquared() >= (float)(scalar * scalar));
        }

        #endregion

        #endregion
    }
}
