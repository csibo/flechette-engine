﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Flechette_Engine
{
    public enum Axis
    {
        X,
        Y,
        Z,
    }

    public enum Shapes
    {
        Point,
        Line,
        Cross,
        Arrow,
        Oval,
        Triangle,
        Quad,
        Pentagon,
        Hexagon,
        Octogon,
    }

    public enum TriangleTypes
    {
        Isosceles,
        Equilateral,
        Scalene,
    }

    public enum QuadTypes
    {
        Rectangle,
        Square,
        Rhombus,
        Parallelogram,
    }

    public enum ClockDirections
    {
        CW,
        CCW,
    }

    public static class Geometry
    {
        public static Vector3 Vector2To3(Vector2 vector)
        {
            return new Vector3(vector.X, vector.Y, 0);
        }

        public static Vector2 Vector3To2(Vector3 vector, Axis axisToCut)
        {
            switch(axisToCut)
            {
                case Axis.X:
                    return new Vector2(vector.Y, vector.Z);
                case Axis.Y:
                    return new Vector2(vector.X, vector.Z);
                case Axis.Z:
                    return new Vector2(vector.X, vector.Y);
            }

            return Vector2.Zero;
        }

        public static Vector2 Vector3To2(Vector3 vector)
        {
            return Vector3To2(vector, Axis.Z);
        }

        public static Vector2[] CreateRectangle(Vector2 topLeft, Vector2 bottomRight, bool closed = false, ClockDirections direction = ClockDirections.CW)
        {
            Vector2[] vertices = null;

            if (direction == ClockDirections.CW)
            {
                vertices = new Vector2[]
                {
                    topLeft,
                    new Vector2(bottomRight.X, topLeft.Y),
                    bottomRight,
                    new Vector2(topLeft.X, bottomRight.Y),
                };
            }
            else
            {
                vertices = new Vector2[]
                {
                    topLeft,
                    new Vector2(topLeft.X, bottomRight.Y),
                    bottomRight,
                    new Vector2(bottomRight.X, topLeft.Y),
                };
            }

            if(closed)
            {
                vertices.Concat(new Vector2[] { vertices[0] });
            }

            return vertices;
        }

        public static Vector2[] CreateSquare(Vector2 topLeft, float sideLength, bool closed = false, ClockDirections direction = ClockDirections.CW)
        {
            return CreateRectangle(topLeft, new Vector2(topLeft.X + sideLength, topLeft.Y + sideLength), closed, direction);
        }
    }
}
