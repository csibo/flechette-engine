﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flechette_Engine
{
    public struct Fraction
    {
        public int Numerator;
        public int Denominator;

        public Fraction(int num, int den)
        {
            Numerator = num;
            Denominator = den;
        }
    }

    public static class MathHelper
    {
        public const double PI = Math.PI;
        public const double TwoPI = Math.PI * 2.0;
        public const double PIOverTwo = Math.PI / 2.0;
        public const double PiOverFour = Math.PI / 4.0;

        public const float DegreesInRadians = (float)(PI / 180.0);
        public const float RadiansInDegrees = (float)(180.0 / PI);

        public static Random GlobalRandom = new Random(System.DateTime.Now.Millisecond);

        // Stern-Brocot tree method for fractional reduction.
        // http://stackoverflow.com/a/5128558
        public static Fraction FloatToFraction(float inputFloat, float error = 0.000001f)
        {
            int n = (int)System.Math.Floor(inputFloat);
            inputFloat -= n;

            if (inputFloat < error)
            {
                return new Fraction(n, 1);
            }
            else if (1 - error < inputFloat)
            {
                return new Fraction(n+1, 1);
            }

            // The lower fraction is 0/1
            int lower_n = 0;
            int lower_d = 1;

            // The upper fraction is 1/1
            int upper_n = 1;
            int upper_d = 1;

            while(true)
            {
                // The middle inputFloat is (lower_n + upper_n) / (lower_d + upper_d)
                int middle_n = lower_n + upper_n;
                int middle_d = lower_d + upper_d;

                // If inputFloat + error < middle
                if (middle_d * (inputFloat + error) < middle_n)
                {
                    // middle is our new upper
                    upper_n = middle_n;
                    upper_d = middle_d;
                }
                // Else If middle < inputFloat - error
                else if (middle_n < (inputFloat - error) * middle_d)
                {
                    // middle is our new lower
                    lower_n = middle_n;
                    lower_d = middle_d;
                }
                // Else middle is our best fraction
                else
                {
                    return new Fraction(n * middle_d + middle_n, middle_d);
                }
            }
        }

        public static float RadiansToDegrees(float radians)
        {
            return (float)(radians * RadiansInDegrees);
        }

        public static float DegreesToRadians(float degrees)
        {
            return (float)(degrees * DegreesInRadians);
        }

        #region Clamp

        /// <summary>
        /// Restricts the value between the min and max.
        /// </summary>
        /// <param name="value">The value to restrict.</param>
        /// <param name="min">The minimum value allowed.</param>
        /// <param name="max">The maximum value allowed.</param>
        /// <returns>Bounded value</returns>
        public static float Clamp(float value, float min, float max)
        {
            value = Math.Max(value, min);
            value = Math.Min(value, max);

            return value;
        }

        /// <summary>
        /// Restricts the value between the min and max.
        /// </summary>
        /// <param name="value">The value to restrict.</param>
        /// <param name="min">The minimum value allowed.</param>
        /// <param name="max">The maximum value allowed.</param>
        /// <returns>Bounded value</returns>
        public static int Clamp(int value, int min, int max)
        {
            value = Math.Max(value, min);
            value = Math.Min(value, max);

            return value;
        }

        /// <summary>
        /// Restricts the value between the min and max.
        /// </summary>
        /// <param name="value">The value to restrict.</param>
        /// <param name="min">The minimum value allowed.</param>
        /// <param name="max">The maximum value allowed.</param>
        /// <returns>Bounded value</returns>
        public static double Clamp(double value, double min, double max)
        {
            value = Math.Max(value, min);
            value = Math.Min(value, max);

            return value;
        }

        /// <summary>
        /// Restricts the value between the min and max.
        /// </summary>
        /// <param name="value">The value to restrict.</param>
        /// <param name="min">The minimum value allowed.</param>
        /// <param name="max">The maximum value allowed.</param>
        /// <returns>Bounded value</returns>
        public static long Clamp(long value, long min, long max)
        {
            value = Math.Max(value, min);
            value = Math.Min(value, max);

            return value;
        }

        /// <summary>
        /// Restricts the value between the min and max.
        /// </summary>
        /// <param name="value">The value to restrict.</param>
        /// <param name="min">The minimum value allowed.</param>
        /// <param name="max">The maximum value allowed.</param>
        /// <returns>Bounded value</returns>
        public static byte Clamp(byte value, byte min, byte max)
        {
            value = Math.Max(value, min);
            value = Math.Min(value, max);

            return value;
        }

        /// <summary>
        /// Restricts the value between the min and max.
        /// </summary>
        /// <param name="value">The value to restrict.</param>
        /// <param name="min">The minimum value allowed.</param>
        /// <param name="max">The maximum value allowed.</param>
        /// <returns>Bounded value</returns>
        public static sbyte Clamp(sbyte value, sbyte min, sbyte max)
        {
            value = Math.Max(value, min);
            value = Math.Min(value, max);

            return value;
        }

        /// <summary>
        /// Restricts the value between the min and max.
        /// </summary>
        /// <param name="value">The value to restrict.</param>
        /// <param name="min">The minimum value allowed.</param>
        /// <param name="max">The maximum value allowed.</param>
        /// <returns>Bounded value</returns>
        public static decimal Clamp(decimal value, decimal min, decimal max)
        {
            value = Math.Max(value, min);
            value = Math.Min(value, max);

            return value;
        }

        /// <summary>
        /// Restricts the value between the min and max.
        /// </summary>
        /// <param name="value">The value to restrict.</param>
        /// <param name="min">The minimum value allowed.</param>
        /// <param name="max">The maximum value allowed.</param>
        /// <returns>Bounded value</returns>
        public static short Clamp(short value, short min, short max)
        {
            value = Math.Max(value, min);
            value = Math.Min(value, max);

            return value;
        }

        /// <summary>
        /// Restricts the value between the min and max.
        /// </summary>
        /// <param name="value">The value to restrict.</param>
        /// <param name="min">The minimum value allowed.</param>
        /// <param name="max">The maximum value allowed.</param>
        /// <returns>Bounded value</returns>
        public static uint Clamp(uint value, uint min, uint max)
        {
            value = Math.Max(value, min);
            value = Math.Min(value, max);

            return value;
        }

        /// <summary>
        /// Restricts the value between the min and max.
        /// </summary>
        /// <param name="value">The value to restrict.</param>
        /// <param name="min">The minimum value allowed.</param>
        /// <param name="max">The maximum value allowed.</param>
        /// <returns>Bounded value</returns>
        public static ulong Clamp(ulong value, ulong min, ulong max)
        {
            value = Math.Max(value, min);
            value = Math.Min(value, max);

            return value;
        }

        /// <summary>
        /// Restricts the value between the min and max.
        /// </summary>
        /// <param name="value">The value to restrict.</param>
        /// <param name="min">The minimum value allowed.</param>
        /// <param name="max">The maximum value allowed.</param>
        /// <returns>Bounded value</returns>
        public static ushort Clamp(ushort value, ushort min, ushort max)
        {
            value = Math.Max(value, min);
            value = Math.Min(value, max);

            return value;
        }

        /// <summary>
        /// Restricts the value between the min and max.
        /// </summary>
        /// <param name="value">The value to restrict.</param>
        /// <param name="min">The minimum value allowed.</param>
        /// <param name="max">The maximum value allowed.</param>
        /// <returns>Bounded value</returns>
        public static Vector Clamp(Vector value, Vector min, Vector max)
        {
            value = Vector.PairOperation(value, min, (val, gate) => { return Math.Max(val, gate); });
            value = Vector.PairOperation(value, max, (val, gate) => { return Math.Min(val, gate); });

            return value;
        }

        /// <summary>
        /// Restricts the value between the min and max.
        /// </summary>
        /// <param name="value">The value to restrict.</param>
        /// <param name="min">The minimum value allowed.</param>
        /// <param name="max">The maximum value allowed.</param>
        /// <returns>Bounded value</returns>
        public static Vector Clamp(Vector value, float min, float max)
        {
            value = Vector.IncrementalOperation(value, (comp) => { return Math.Max(comp, min); });
            value = Vector.IncrementalOperation(value, (comp) => { return Math.Min(comp, max); });

            return value;
        }

        #endregion
    }
}
