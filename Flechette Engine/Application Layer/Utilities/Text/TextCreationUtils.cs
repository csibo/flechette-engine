﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Flechette_Engine
{
    /// <summary>
    /// Collection of utilities to handle the creation and manipulation of strings.
    /// </summary>
    public static class TextCreationUtils
    {
        public static string Replace(string original, List<string> patternPool, List<char> replacementPool)
        {
            foreach(string pattern in patternPool)
            {
                string replacement = "";

                for(int i=0;i<pattern.Length;i++)
                {
                    replacement += replacementPool[MathHelper.GlobalRandom.Next(0, replacementPool.Count())];
                }

                original = original.Replace(pattern, replacement);
            }

            return original;
        }

        public static string Replace(string original, List<char> replacementPool)
        {
            return TextCreationUtils.Replace(original, new List<string>() { original }, replacementPool);
        }

        public static string Replace(string original, char replacementChar)
        {
            return TextCreationUtils.Replace(original, new List<string>() { original }, new List<char>() { replacementChar });
        }
    }
}
