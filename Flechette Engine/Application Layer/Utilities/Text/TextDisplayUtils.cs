﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Flechette_Engine
{
    /// <summary>
    /// Methods to handle text overflow.
    /// </summary>
    public enum TextOverflowMethods
    {
        Allow,
        Hide,
        Ellipsis,
    }

    /// <summary>
    /// Collection of utilities to handle displaying of strings.
    /// </summary>
    public static class TextDisplayUtils
    {
        /// <summary>
        /// Measures a string within a given font.
        /// </summary>
        /// <param name="text">String to measure</param>
        /// <param name="font">Font with which to measure</param>
        /// <returns>Vector2 string dimensions</returns>
        public static Vector2 MeasureText(string text, SpriteFont font)
        {
            if (font != null)
                return font.MeasureString(text);

            return Vector2.Zero;
        }

        /// <summary>
        /// Measures a string within a given font.
        /// </summary>
        /// <param name="sprite">TextSprite to measure</param>
        /// <returns>Vector2 string dimensions</returns>
        public static Vector2 MeasureText(TextSprite sprite)
        {
            if(sprite != null && sprite.Font != null)
                return TextDisplayUtils.MeasureText(sprite.Text, sprite.Font);

            return Vector2.Zero;
        }

        /// <summary>
        /// Measures a string within a given font.
        /// </summary>
        /// <param name="label">Label to measure</param>
        /// <returns>Vector2 string dimensions</returns>
        public static Vector2 MeasureText(Label label)
        {
            if (label != null && label.Font != null)
                return TextDisplayUtils.MeasureText(label.Text, label.Font);

            return Vector2.Zero;
        }

        /// <summary>
        /// Measures a string within a given font.
        /// </summary>
        /// <param name="textbox">Textbox to measure</param>
        /// <returns>Vector2 string dimensions</returns>
        public static Vector2 MeasureText(TextBox textbox)
        {
            if (textbox != null && textbox.ActiveSkin.FontProfile.Font != null)
                return TextDisplayUtils.MeasureText(textbox.Text, textbox.ActiveSkin.FontProfile.Font);

            return Vector2.Zero;
        }

        /// <summary>
        /// Finds the index of the last character that fits in the width.
        /// </summary>
        /// <param name="text">String to fit</param>
        /// <param name="font">SpriteFont with which to measure</param>
        /// <param name="width">Max width allowed</param>
        /// <returns>Index of the last character that will fit within the width</returns>
        public static int FindOverflowPosition(string text, SpriteFont font, int width)
        {
            int min = 0;
            int max = text.Length;
            int pos = ((max - min) / 2) + min;

            // While we're still zeroing in
            while (max - min > 1)
            {
                if (MeasureText(text.Substring(0, pos), font).X > width)
                {
                    max = pos;
                }
                else if (MeasureText(text.Substring(0, pos), font).X < width)
                {
                    min = pos;
                }
                else
                {
                    break;
                }

                // Reset the binary search
                pos = ((max - min) / 2) + min;
            }

            // Adjust for 0-indexing
            return pos - 1;
        }

        /// <summary>
        /// Fits a string to a rectangle using an ellipsis.
        /// </summary>
        /// <param name="text">String to measure</param>
        /// <param name="font">Font with which to measure</param>
        /// <param name="width">Maximum width allowed</param>
        /// <returns>String that fits within the max width</returns>
        public static string FitTextWithEllipsis(string text, SpriteFont font, int width)
        {
            // Get the truncated string that will fit in the width
            text = TextDisplayUtils.FitTextWithHide(text, font, width);

            // Then replace the last three letters with an ellipsis
            text = text.Substring(0, text.Length - 3) + "...";

            return text;
        }

        /// <summary>
        /// Fits a string to a rectangle by hiding any overflow.
        /// </summary>
        /// <param name="text">String to measure</param>
        /// <param name="font">Font with which to measure</param>
        /// <param name="width">Maximum width allowed</param>
        /// <returns>String that fits within the max width</returns>
        public static string FitTextWithHide(string text, SpriteFont font, int width)
        {
            int cutPosition = TextDisplayUtils.FindOverflowPosition(text, font, width);

            text = text.Substring(0, cutPosition + 1); // +1 to go from index to length

            return text;
        }
    }
}
