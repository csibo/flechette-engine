﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Flechette_Engine
{
    public static class DisplayMechanics
    {
        /// <summary>
        /// Applies a new window resolution.
        /// </summary>
        /// <param name="width">Pixel width</param>
        /// <param name="height">Pixel height</param>
        public static void ChangeResolution(int width, int height)
        {
            GameController.graphics.PreferredBackBufferWidth = width;
            GameController.graphics.PreferredBackBufferHeight = height;

            ResetGraphicsState();
        }

        /// <summary>
        /// Resets the graphics state to account for changes such as window resizes.
        /// </summary>
        public static void ResetGraphicsState()
        {
            GameController.graphics.PreparingDeviceSettings += new EventHandler<PreparingDeviceSettingsEventArgs>(delegate(object sender, PreparingDeviceSettingsEventArgs e)
            {
                e.GraphicsDeviceInformation.PresentationParameters.RenderTargetUsage = RenderTargetUsage.PreserveContents;
            });

            GameController.graphics.ApplyChanges();
        }
    }
}
