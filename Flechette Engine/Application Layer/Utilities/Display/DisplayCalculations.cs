﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Flechette_Engine
{
    public struct Resolution
    {
        public int Width;
        public int Height;
        public Fraction aspectRatio;

        public Resolution(int theWidth, int theHeight, Fraction theRatio = new Fraction())
        {
            Width = theWidth;
            Height = theHeight;

            if(theRatio.Numerator > 0)
            {
                aspectRatio = theRatio;
            }
            else
            {
                aspectRatio = DisplayCalculations.AspectRatio(Width, Height);
            }
        }
    }

    public static class DisplayCalculations
    {
        public static readonly Dictionary<string, Resolution> SupportedResolutions = new Dictionary<string, Resolution>();

        static DisplayCalculations()
        {
            foreach (DisplayMode mode in GraphicsAdapter.DefaultAdapter.SupportedDisplayModes)
            {
                if (mode.Format == SurfaceFormat.Color)
                {
                    SupportedResolutions.Add(mode.Width+"x"+mode.Height, new Resolution(mode.Width, mode.Height));
                }
            }
        }

        public static Resolution ResolutionStringToInts(string resolution)
        {
            List<int> dims = new List<int>();

            foreach (Match match in Regex.Matches(resolution, @"\d+"))
            {
                dims.Add(int.Parse(match.Value));
            }

            return new Resolution(dims[0], dims[1]);
        }

        public static Fraction AspectRatio(Resolution resolution)
        {
            return DisplayCalculations.AspectRatio(resolution.Width, resolution.Height);
        }

        public static Fraction AspectRatio(int width, int height)
        {
            return MathHelper.FloatToFraction(width / height);
        }
    }
}
