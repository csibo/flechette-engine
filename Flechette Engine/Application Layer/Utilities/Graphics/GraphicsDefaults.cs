﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Flechette_Engine
{
    public static class GraphicsDefaults
    {
        /// <summary>
        /// A single white pixel for drawing solid, colored textures.
        /// </summary>
        public static Texture2D WhitePixel { get; private set; }

        /// <summary>
        /// Checkerboard texture for non-loaded assets
        /// </summary>
        public static Texture2D DefaultTexture { get; private set; }

        static GraphicsDefaults()
        {
            WhitePixel = GenerateWhitePixel();

            DefaultTexture = GenerateDefaultTexture();
        }

        /// <summary>
        /// Creates a checkerboard replacement texture for use when other texture loads fail 
        /// </summary>
        /// <returns>Debug checkerboard texture</returns>
        private static Texture2D GenerateDefaultTexture()
        {
            int xElements = 10;
            int yElements = 10;

            float xBoxes = 5;
            float yBoxes = 5;

            Texture2D defaultTex = new Texture2D(GameController.game.GraphicsDevice, xElements, yElements);
            Color[] colorMap = new Color[xElements * yElements];

            Color color1 = Color.Black;
            Color color2 = Color.White;

            int currX = 0;
            int currY = 0;

            for (currX = 0; currX < xElements; currX++)
            {
                for (currY = 0; currY < yElements; currY++)
                {
                    int currLoc = GraphicsDefaults.Map2Dto1D(currX, currY, xElements);

                    if (currX < ((float)xElements / (4 * xBoxes))
                        || currX > xElements - ((float)xElements / (4 * xBoxes))
                        || currY < ((float)yElements / (4 * yBoxes))
                        || currY > yElements - ((float)yElements / (4 * yBoxes)))
                    {
                        colorMap[currLoc] = Color.Red;
                    }
                    else
                    {

                        if (currX % ((float)(2 * xElements) / xBoxes) <= ((float)xElements / xBoxes))
                        {
                            color1 = Color.Black;
                            color2 = Color.White;
                        }
                        else
                        {
                            color1 = Color.White;
                            color2 = Color.Black;
                        }

                        if (currY % ((float)(2 * yElements) / yBoxes) <= ((float)yElements / yBoxes))
                        {
                            colorMap[currLoc] = color1;
                        }
                        else
                        {
                            colorMap[currLoc] = color2;
                        }
                    }
                }
            }

            for (int i = 0; i < colorMap.Count();i++)
            {
                colorMap[i] = ColorUtils.GenerateRandomColor(Color.White);
            }

            defaultTex.SetData<Color>(colorMap);

            return defaultTex;
        }

        /// <summary>
        /// Creates a single white pixel texture
        /// </summary>
        /// <returns>1x1 white texture</returns>
        private static Texture2D GenerateWhitePixel()
        {
            Texture2D pixel = new Texture2D(GameController.game.GraphicsDevice, 1, 1);

            pixel.SetData(new[] { Color.White });

            return pixel;
        }

        /// <summary>
        /// Maps a 2D array location to a 1D array location
        /// </summary>
        /// <param name="currX">The column location</param>
        /// <param name="currY">The row location</param>
        /// <param name="xWidth">2D array width (first dimension)</param>
        /// <returns></returns>
        public static int Map2Dto1D(int currX, int currY, int xWidth)
        {
            return (currY * xWidth) + currX;
        }
    }
}
