﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flechette_Engine
{
    public static class AnimationUtils
    {
        public enum Directions
        {
            None,
            Horizontal,
            Vertical,
            LeftToRight,
            RightToLeft,
            TopToBottom,
            BottomToTop
        };
    }
}
