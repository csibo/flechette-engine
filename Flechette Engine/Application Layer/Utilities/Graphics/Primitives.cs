﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Flechette_Engine
{
    public static class Primitives
    {
        private static Matrix viewMatrix;

        private static Matrix projectionMatrix;

        public const int CircleSegments = 32;

        public static PrimitiveBatch Batch { get; set; }

        public static Matrix ViewMatrix { get { return viewMatrix; } set { viewMatrix = value; UpdateBatch(); } }

        public static Matrix ProjectionMatrix { get { return projectionMatrix; } set { projectionMatrix = value; UpdateBatch(); } }

        static Primitives()
        {
            ProjectionMatrix = ScreenController.screens[ScreenController.screenStack.Peek()].activeCamera.ProjectionMatrix;
            ViewMatrix = ScreenController.screens[ScreenController.screenStack.Peek()].activeCamera.ViewMatrix;
        }

        public static void UpdateBatch()
        {
            Batch = new PrimitiveBatch(
                GameController.graphics.GraphicsDevice,
                ScreenController.screens[ScreenController.screenStack.Peek()],
                projectionMatrix,
                viewMatrix);
        }

        public static void ResetBatch()
        {
            ProjectionMatrix = ScreenController.screens[ScreenController.screenStack.Peek()].activeCamera.ProjectionMatrix;
            ViewMatrix = ScreenController.screens[ScreenController.screenStack.Peek()].activeCamera.ViewMatrix;
        }

        public static void DrawPoint(Vector2 point, float size, Color color)
        {
            Vector2[] verts = new Vector2[4];
            float hs = size / 2.0f;
            verts[0] = point + new Vector2(-hs, -hs);
            verts[1] = point + new Vector2(hs, -hs);
            verts[2] = point + new Vector2(hs, hs);
            verts[3] = point + new Vector2(-hs, hs);

            DrawSolidPolygon(verts, color, true);
        }

        public static void DrawSegment(Vector2 start, Vector2 end, Color color)
        {
            Batch.Begin(PrimitiveType.LineList);

            Batch.AddVertex(start, color);
            Batch.AddVertex(end, color);

            Batch.End();
        }

        static public void DrawCircle(Vector2 center, float radius, Color color)
        {
            const double increment = System.Math.PI * 2.0 / CircleSegments;
            double theta = 0.0;

            Batch.Begin(PrimitiveType.LineList);

            for (int i = 0; i < CircleSegments; i++)
            {
                Vector2 v1 = center + radius * new Vector2((float)System.Math.Cos(theta), (float)System.Math.Sin(theta));
                Vector2 v2 = center + radius * new Vector2((float)System.Math.Cos(theta + increment), (float)System.Math.Sin(theta + increment));

                Batch.AddVertex(v1, color);
                Batch.AddVertex(v2, color);

                theta += increment;
            }

            Batch.End();
        }

        static public void DrawSolidCircle(Vector2 center, float radius, Vector2 axis, Color color)
        {
            const double increment = System.Math.PI * 2.0 / CircleSegments;
            double theta = 0.0;

            Color colorFill = color * 0.5f;

            Vector2 v0 = center + radius * new Vector2((float)System.Math.Cos(theta), (float)System.Math.Sin(theta));
            theta += increment;

            Batch.Begin(PrimitiveType.TriangleList);

            for (int i = 1; i < CircleSegments - 1; i++)
            {
                Vector2 v1 = center + radius * new Vector2((float)System.Math.Cos(theta), (float)System.Math.Sin(theta));
                Vector2 v2 = center + radius * new Vector2((float)System.Math.Cos(theta + increment), (float)System.Math.Sin(theta + increment));

                Batch.AddVertex(v0, colorFill);
                Batch.AddVertex(v1, colorFill);
                Batch.AddVertex(v2, colorFill);

                theta += increment;
            }

            Batch.End();

            DrawCircle(center, radius, color);
            DrawSegment(center, center + axis * radius, color);
        }

        public static void DrawPolygon(Vector2[] vertices, Color color, bool closed = true)
        {
            Batch.Begin(PrimitiveType.LineList);

            for (int i = 0; i < vertices.Count() - 1; i++)
            {
                Batch.AddVertex(vertices[i], color);
                Batch.AddVertex(vertices[i + 1], color);
            }

            if (closed)
            {
                Batch.AddVertex(vertices[vertices.Count() - 1], color);
                Batch.AddVertex(vertices[0], color);
            }

            Batch.End();
        }

        public static void DrawSolidPolygon(Vector2[] vertices, Color color, bool outline = true)
        {
            if (vertices.Count() == 2)
            {
                DrawPolygon(vertices, color);
                return;
            }

            Color colorFill = color * (outline ? 0.5f : 1.0f);

            Batch.Begin(PrimitiveType.TriangleList);

            for (int i = 1; i < vertices.Count() - 1; i++)
            {
                Batch.AddVertex(vertices[0], colorFill);
                Batch.AddVertex(vertices[i], colorFill);
                Batch.AddVertex(vertices[i + 1], colorFill);
            }

            Batch.End();

            if (outline)
                DrawPolygon(vertices, color);
        }

        static public void DrawArrow(Vector2 start, Vector2 end, float length, float width, bool drawStartIndicator, Color color)
        {
            // Draw connection segment between start- and end-point
            DrawSegment(start, end, color);

            // Precalculate halfwidth
            float halfWidth = width / 2;

            // Create directional reference
            Vector2 rotation = (start - end);
            rotation.Normalize();

            // Calculate angle of directional vector
            float angle = (float)System.Math.Atan2(rotation.X, -rotation.Y);
            // Create matrix for rotation
            Matrix rotMatrix = Matrix.CreateRotationZ(angle);
            // Create translation matrix for end-point
            Matrix endMatrix = Matrix.CreateTranslation(end.X, end.Y, 0);

            // Setup arrow end shape
            Vector2[] verts = new Vector2[3];
            verts[0] = new Vector2(0, 0);
            verts[1] = new Vector2(-halfWidth, -length);
            verts[2] = new Vector2(halfWidth, -length);

            // Rotate end shape
            Vector2.Transform(verts, ref rotMatrix, verts);
            // Translate end shape
            Vector2.Transform(verts, ref endMatrix, verts);

            // Draw arrow end shape
            DrawSolidPolygon(verts, color, false);

            if (drawStartIndicator)
            {
                // Create translation matrix for start
                Matrix startMatrix = Matrix.CreateTranslation(start.X, start.Y, 0);
                // Setup arrow start shape
                Vector2[] baseVerts = new Vector2[4];
                baseVerts[0] = new Vector2(-halfWidth, length / 4);
                baseVerts[1] = new Vector2(halfWidth, length / 4);
                baseVerts[2] = new Vector2(halfWidth, 0);
                baseVerts[3] = new Vector2(-halfWidth, 0);

                // Rotate start shape
                Vector2.Transform(baseVerts, ref rotMatrix, baseVerts);
                // Translate start shape
                Vector2.Transform(baseVerts, ref startMatrix, baseVerts);
                // Draw start shape
                DrawSolidPolygon(baseVerts, color, false);
            }
        }

        public static void DrawRectangle(Rectangle rect, Color color, bool closed)
        {
            Primitives.DrawPolygon(
                new Vector2[] {
                    new Vector2(rect.X, rect.Y),
                    new Vector2(rect.X + rect.Width, rect.Y),
                    new Vector2(rect.X + rect.Width, rect.Y + rect.Height),
                    new Vector2(rect.X, rect.Y + rect.Height),
                },
                color, closed);
        }

        public static void DrawSolidRectangle(Rectangle rect, Color color, bool closed)
        {
            Primitives.DrawSolidPolygon(
                new Vector2[] {
                    new Vector2(rect.X, rect.Y),
                    new Vector2(rect.X + rect.Width, rect.Y),
                    new Vector2(rect.X + rect.Width, rect.Y + rect.Height),
                    new Vector2(rect.X, rect.Y + rect.Height),
                },
                color, closed);
        }
    }
}
