﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Flechette_Engine
{
    public static class ColorUtils
    {
        public static Color GenerateRandomColor(Color mix)
        {
            return GenerateRandomColor(mix, Color.Black, Color.White);
        }

        public static Color GenerateRandomColor(Color mix, Color min, Color max)
        {
            Color color = Color.Black;

            do
            {
                int red = MathHelper.GlobalRandom.Next(0, 256);
                int green = MathHelper.GlobalRandom.Next(0, 256);
                int blue = MathHelper.GlobalRandom.Next(0, 256);

                // mix the color
                if (mix != null)
                {
                    red = (red + mix.R) / 2;
                    green = (green + mix.G) / 2;
                    blue = (blue + mix.B) / 2;
                }

                color = new Color(red, green, blue);
            } while (ColorLessThan(color, min) || ColorGreaterThan(color, max));

            return color;
        }

        public static bool ColorLessThan(Color left, Color right)
        {
            if (left.R >= right.R)
                return false;

            if (left.G >= right.G)
                return false;

            if (left.B >= right.B)
                return false;

            return true;
        }

        public static bool ColorGreaterThan(Color left, Color right)
        {
            if (left.R <= right.R)
                return false;

            if (left.G <= right.G)
                return false;

            if (left.B <= right.B)
                return false;

            return true;
        }
    }
}
