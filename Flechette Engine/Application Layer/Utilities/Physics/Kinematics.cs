﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Flechette_Engine
{
    public static class Kinematics
    {
        /// <summary>
        /// Stores state data.
        /// </summary>
        public struct KinematicData
        {
            /// <summary>
            /// Current pixel location
            /// </summary>
            public Vector2 position;

            /// <summary>
            /// Radian value of the current orientation
            /// </summary>
            public float orientation;

            /// <summary>
            /// Normalized Vector of the current orientation
            /// </summary>
            public Vector2 orientationVector;

            /// <summary>
            /// Mass units
            /// </summary>
            public float mass;

            /// <summary>
            /// Relative location of the CoM
            /// </summary>
            public Vector2 centerOfMass;

            /// <summary>
            /// Current linear forces
            /// </summary>
            public Vector2 linearForce;

            /// <summary>
            /// Current linear velocity
            /// </summary>
            public Vector2 linearVelocity;

            /// <summary>
            /// Current linear velocity
            /// </summary>
            public Vector2 linearAcceleration;

            /// <summary>
            /// Current angular velocity
            /// </summary>
            public Vector2 angularVelocity;

            /// <summary>
            /// Current angular acceleration
            /// </summary>
            public Vector2 angularAcceleration;

            /// <summary>
            /// Current angular forces
            /// </summary>
            public Vector2 angularForce;

            /// <summary>
            /// Maximum linear acceleration allowed
            /// </summary>
            public float maxLinearAcceleration;

            /// <summary>
            /// Maximum linear velocity allowed
            /// </summary>
            public float maxLinearVelocity;

            /// <summary>
            /// Minimum linear velocity allowed when travelling to target
            /// </summary>
            public float minLinearVelocity;

            /// <summary>
            /// Maximum angular acceleration allowed
            /// </summary>
            public float maxAngularAcceleration;

            /// <summary>
            /// Maximum angular velocity allowed
            /// </summary>
            public float maxAngularVelocity;

            public KinematicData(int i)
            {
                position = Vector2.Zero;
                orientation = 0f;
                orientationVector = Vector2.Zero;
                mass = 0f;
                centerOfMass = Vector2.Zero;
                linearVelocity = Vector2.Zero;
                linearAcceleration = Vector2.Zero;
                linearForce = Vector2.Zero;
                angularVelocity = Vector2.Zero;
                angularAcceleration = Vector2.Zero;
                angularForce = Vector2.Zero;
                maxLinearVelocity = 0f;
                minLinearVelocity = 0f;
                maxLinearAcceleration = 0f;
                maxAngularVelocity = 0f;
                maxAngularAcceleration = 0f;
            }
        }

        public static KinematicData ApplyLinearForce(ref KinematicData input)
        {
            KinematicData output = input;



            return output;
        }

        public static Vector2 UpdateLinearAcceleration(ref KinematicData input)
        {
            Vector2 output = input.linearAcceleration;

            // a = F/m
            output = input.linearForce / input.mass;

            // If we're going too fast
            if (output.Length() > input.maxLinearAcceleration)
            {
                //output.Normalize();
                //output *= input.maxLinearAcceleration;
            }

            return output;
        }

        public static Vector2 UpdateLinearVelocity(ref KinematicData input, float time = 0)
        {
            Vector2 output = input.linearVelocity;

            if (time == 0)
            {
                time = (float)GameController.gameTime.ElapsedGameTime.TotalMilliseconds;
            }

            // v = at
            output += input.linearAcceleration * time;

            // If we're going too fast
            if (output.Length() > input.maxLinearVelocity)
            {
                output.Normalize();
                output *= input.maxLinearVelocity;
            }

            return output;
        }

        public static Vector2 UpdatePosition(ref KinematicData input, float time = 0)
        {
            Vector2 output = input.position;

            if (time == 0)
            {
                time = (float)GameController.gameTime.ElapsedGameTime.TotalMilliseconds;
            }

            // d = vt + 0.5at^2
            output += (input.linearVelocity * time)
                 + (0.5f * input.linearAcceleration * time * time);

            return output;
        }

        public static Vector2 SolveOrientationVector(ref KinematicData input)
        {
            return SolveOrientationVector(input.orientation);
        }

        public static Vector2 SolveOrientationVector(float input)
        {
            Vector2 output = Vector2.Zero;

            output.X = (float)System.Math.Sin(input);
            output.Y = -(float)System.Math.Cos(input);

            if (output.Length() > 0)
                output.Normalize();

            return output;
        }
    }
}
