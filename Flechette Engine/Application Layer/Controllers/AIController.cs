﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flechette_Engine
{
    /// <summary>
    /// Controls the AI engine.
    /// </summary>
    public class AIController : Controller
    {
        /// <summary>
        /// All of the AI agents.
        /// </summary>
        public List<NavAgent> navAgents;

        /// <summary>
        /// All of the navigational maps.
        /// </summary>
        public List<NavMap> navMaps;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public AIController()
            : base()
        {
            navAgents = new List<NavAgent>();
            navMaps = new List<NavMap>();
        }

        /// <summary>
        /// Update all maps and agents.
        /// </summary>
        public override void Update()
        {
            foreach (NavMap map in navMaps)
            {
                //map.Update();
            }

            //SyncAgentsToPhysics();

            foreach (NavAgent agent in navAgents)
            {
                //agent.Update();
            }

            //SyncPhysicsToAgents();
        }

        public override void Draw()
        {
            foreach (NavMap map in navMaps)
            {
                map.Draw();
            }
        }

        public void SyncAgentsToPhysics()
        {
            foreach (NavAgent agent in navAgents)
            {
                agent.SyncKinemetics();
            }
        }

        public void SyncPhysicsToAgents()
        {
            foreach (NavAgent agent in navAgents)
            {
                agent.SyncToKinemetics();
            }
        }
    }
}
