﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using FarseerPhysics;
using FarseerPhysics.Common;
using FarseerPhysics.Common.PolygonManipulation;
using FarseerPhysics.Common.Decomposition;
using FarseerPhysics.Collision;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using FarseerPhysics.DebugView;

namespace Flechette_Engine
{
    /// <summary>
    /// Controls the physics engine.
    /// </summary>
    public class PhysicsController : Controller
    {
        /// <summary>
        /// Farseer world instance.
        /// </summary>
        public World world;

        Stopwatch UpdateTimer = new Stopwatch();

        Stopwatch UpdateRateTimer = new Stopwatch();

        long ElapsedTime = 0;

        protected int UpdateCounter { get; set; }

        public int UpdateRate { get; set; }

        public DebugView DebugView;

        /// <summary>
        /// Screen to which I belong.
        /// </summary>
        public Screen ContainingScreen { get; set; }

        public PhysicsController(Screen containingScreen)
            : base()
        {
            ContainingScreen = containingScreen;

            world = new World(new Vector2(0, 10f));
            SetSimUnitRatio(100f);

            FarseerPhysics.Settings.ContinuousPhysics = true;
            FarseerPhysics.Settings.AllowSleep = true;

            UpdateCounter = 0;
            UpdateRate = 0;

            DebugView = new DebugView(world);
            DebugView.AppendFlags(DebugViewFlags.DebugPanel);
            DebugView.DefaultShapeColor = Color.White;
            DebugView.SleepingShapeColor = Color.LightGray;
            DebugView.DebugPanelPosition = new Vector2(10, 170);
            DebugView.PerformancePanelBounds = new Rectangle((int)DebugView.DebugPanelPosition.X,
                (int)DebugView.DebugPanelPosition.Y + 105, 100, 50);
            DebugView.LoadContent(GameController.game.GraphicsDevice, GameController.game.Content, ContainingScreen);

            UpdateRateTimer.Start();
        }

        /// <summary>
        /// !! DON'T CALL THIS !!
        /// Used to let separate threads execute the physics logic.
        /// Will result in infinite loops if called incorrectly.
        /// </summary>
        public void Run()
        {
            // Loop until my thread is killed
            while(true)
            {
                Update();

                Thread.Sleep((int)(Settings.PhysicsUpdateRate.TotalSeconds * 1000));
            }
        }

        /// <summary>
        /// Runs a single update of the physics engine.
        /// </summary>
        public override void Update()
        {
            if (Enabled)
            {
                ElapsedTime += UpdateTimer.Elapsed.Ticks;
                UpdateTimer.Stop();

                if (UpdateRateTimer.Elapsed.Seconds >= 1)
                {
                    UpdateRate = UpdateCounter;
                    UpdateCounter = 0;

                    UpdateRateTimer.Restart();
                }

                while (ShouldUpdate())
                {
                    try
                    {
                        world.Step((float)Settings.PhysicsUpdateRate.TotalSeconds);

                        UpdateCounter++;

                        ElapsedTime -= (long)(Settings.PhysicsUpdateRate.TotalSeconds * TimeSpan.TicksPerSecond);
                    }
                    catch(Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }

                UpdateTimer.Restart();

                //DebugEnabled = (UIController.UIManager.GetControl("Physics Window").Visible
                //    && UIController.DebugEnabled);

                DebugEnabled = UIController.DebugEnabled;
            }

            base.Update();
        }

        protected bool ShouldUpdate()
        {
            // Make sure my screen is the active one
            Screen first = null;
            Dictionary<string, Screen> screensLocked;
            string screenLocked = null;

            lock (ScreenController.screens)
            {
                screensLocked = ScreenController.screens;
            }

            lock (ScreenController.screenStack)
            {
                if (ScreenController.screenStack.Count() > 0)
                    screenLocked = ScreenController.screenStack.Peek();
            }

            if (screenLocked != null)
                first = screensLocked[screenLocked];

            // If my screen is the active one
            if (first != null
                && ContainingScreen == first)
            {
                if(!UpdateTimer.IsRunning)
                    UpdateTimer.Start();

                // If the elapsed time is beyond our target time...
                if (ElapsedTime >= Settings.PhysicsUpdateRate.TotalSeconds * TimeSpan.TicksPerSecond)
                {
                    // Run the update code
                    return true;
                }
            }
            else
            {
                ElapsedTime = 0;
                UpdateTimer.Stop();
            }

            return false;
        }

        public override void Draw()
        {
            if (Visible)
            {
                if (DebugEnabled)
                {
                    DebugView.AppendFlags(DebugViewFlags.Shape);
                    DebugView.AppendFlags(DebugViewFlags.Controllers);
                    DebugView.AppendFlags(DebugViewFlags.Joint);
                    DebugView.AppendFlags(DebugViewFlags.CenterOfMass);
                    DebugView.AppendFlags(DebugViewFlags.AABB);
                    DebugView.AppendFlags(DebugViewFlags.ContactNormals);
                    DebugView.AppendFlags(DebugViewFlags.ContactPoints);
                    DebugView.AppendFlags(DebugViewFlags.Controllers);

                    DebugView.RemoveFlags(DebugViewFlags.DebugPanel);
                    DebugView.RemoveFlags(DebugViewFlags.PerformanceGraph);

                    try
                    {
                        DebugView.RenderDebugData(
                            ContainingScreen.activeCamera.ProjectionMatrix, 
                            ContainingScreen.activeCamera.ViewMatrix);
                    }
                    catch (Exception e)
                    {
                        System.Console.WriteLine(e.Message);
                    }
                }
            }

            base.Draw();
        }

        /// <summary>
        /// Sets the pixel-to-simulation-unit ratio for physics.
        /// </summary>
        /// <param name="ratio">Desired ratio. Default = 100f</param>
        public void SetSimUnitRatio(float ratio = 100f)
        { ConvertUnits.SetDisplayUnitToSimUnitRatio(ratio); }
    }
}
