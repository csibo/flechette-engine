﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Threading;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Nuclex;
using Nuclex.Input;

namespace Flechette_Engine
{
    public class UIController : Controller
    {
        public static int FrameRate { get; set; }

        protected static int FrameCounter { get; set; }

        public static int UpdateRate { get; set; }

        protected static int UpdateCounter { get; set; }

        public static TimeSpan ElapsedTime { get; set; }

        public static Queue<MouseState> MouseStates { get; protected set; }

        public static Queue<KeyboardState> KeyboardStates { get; protected set; }

        //public static Dictionary<string, Keys> Controls { get; set; }

        public static SpriteFont DefaultFont { get; set; }

        new public static bool DebugEnabled { get; set; }

        protected Stopwatch DebugUpdateWatch { get; set; }

        public static InputManager InputManager;

        public static event Nuclex.Input.Devices.KeyDelegate KeyboardKeyUp;

        public static event Nuclex.Input.Devices.KeyDelegate KeyboardKeyDown;

        public static event Nuclex.Input.Devices.CharacterDelegate KeyboardCharacter;

        public static event Nuclex.Input.Devices.MouseButtonDelegate MouseButtonDown;

        public static event EventHandler MouseLeftDown;

        public static event EventHandler MouseRightDown;

        public static event EventHandler MouseMiddleDown;

        public static event EventHandler MouseX1Down;

        public static event EventHandler MouseX2Down;

        public static event Nuclex.Input.Devices.MouseButtonDelegate MouseButtonUp;

        public static event EventHandler MouseLeftUp;

        public static event EventHandler MouseRightUp;

        public static event EventHandler MouseMiddleUp;

        public static event EventHandler MouseX1Up;

        public static event EventHandler MouseX2Up;

        public static event Nuclex.Input.Devices.MouseMoveDelegate MouseMove;

        public static event Nuclex.Input.Devices.MouseWheelDelegate MouseWheelMove;

        public static Mouse Mouse { get; set; }

        public UIController()
            : base()
        {
            FrameRate = 0;
            FrameCounter = 0;
            ElapsedTime = TimeSpan.Zero;

            MouseStates = new Queue<MouseState>();
            KeyboardStates = new Queue<KeyboardState>();
            //Controls = new Dictionary<string, Keys>();

            DebugUpdateWatch = new Stopwatch();
            DebugUpdateWatch.Reset();

            Mouse = new Mouse();
        }

        public override void Initialize()
        {
            base.Initialize();

            DefaultFont = Game.Content.Load<SpriteFont>("Electrolize");

            GameController.game.Window.AllowUserResizing = true;

            Settings.CursorVisible = true;

            Settings.ResolutionIndependant = false;

            Settings.RenderResolution = new Vector2(1920, 1080);
            Settings.RenderResolution = new Vector2(1280, 720);

            if(DisplayCalculations.SupportedResolutions.Keys.Contains("1280x720"))
                Settings.DisplayResolution = new Vector2(1280, 720);
            else
                Settings.DisplayResolution = new Vector2(800, 600);

            //Settings.DisplayResolution = new Vector2(800, 600);
            //Settings.DisplayResolution = new Vector2(1920, 1080);
            Settings.DisplayResolution = new Vector2(1280, 720);

            //InitializeDebugWindow();

            InputManager = new InputManager(Game.Services, Game.Window.Handle);
            Game.Components.Add(InputManager);

            InputManager.GetKeyboard().KeyPressed += UIController_KeyPressed;
            InputManager.GetKeyboard().KeyReleased += UIController_KeyReleased;
            InputManager.GetKeyboard().CharacterEntered += UIController_CharacterEntered;

            InputManager.GetMouse().MouseButtonPressed += UIController_MouseButtonPressed;
            InputManager.GetMouse().MouseButtonReleased += UIController_MouseButtonReleased;
            InputManager.GetMouse().MouseMoved += UIController_MouseMoved;
            InputManager.GetMouse().MouseWheelRotated += UIController_MouseWheelRotated;
        }

        protected override void Dispose(bool disposing)
        {
            InputManager.Dispose();

            base.Dispose(disposing);
        }

        #region debugwindows
        //protected void InitializeDebugWindow()
        //{
        //    Window debugWindow = new Window(UIManager);
        //    debugWindow.Init();
        //    debugWindow.BorderVisible = false;
        //    debugWindow.Left = 0;
        //    debugWindow.Top = 0;
        //    debugWindow.Width = UIManager.RenderTarget.Width;
        //    debugWindow.Height = UIManager.RenderTarget.Height;
        //    debugWindow.AutoMaxSize = true;
        //    debugWindow.Shadow = false;
        //    debugWindow.Resizable = false;
        //    debugWindow.Movable = false;
        //    debugWindow.AutoScroll = false;
        //    debugWindow.Name = "Debug Window";
        //    UIManager.Add(debugWindow);

        //    ToolBar tb = new ToolBar(UIManager);
        //    tb.Init();
        //    tb.Width = debugWindow.Width;
        //    tb.FullRow = true;
        //    tb.Height = 15;
        //    tb.Name = "Toolbar";
        //    debugWindow.Add(tb);

        //    ToolBarButton tbb = new ToolBarButton(UIManager);
        //    tbb.Init();
        //    tbb.Width = 100;
        //    tbb.Height = tb.Height - 1;
        //    tbb.Text = "Memory";
        //    tbb.BackColor = new Color(55, 55, 55);
        //    tbb.Click += DebugWindow_Memory_Click;
        //    tb.Add(tbb);

        //    tbb = new ToolBarButton(UIManager);
        //    tbb.Init();
        //    tbb.Left = 110;
        //    tbb.Width = 100;
        //    tbb.Height = tb.Height - 1;
        //    tbb.Text = "Performance";
        //    tbb.BackColor = new Color(55, 55, 55);
        //    tbb.Click += DebugWindow_Performance_Click;
        //    tb.Add(tbb);

        //    tbb = new ToolBarButton(UIManager);
        //    tbb.Init();
        //    tbb.Left = 220;
        //    tbb.Width = 100;
        //    tbb.Height = tb.Height - 1;
        //    tbb.Text = "Physics";
        //    tbb.BackColor = new Color(55, 55, 55);
        //    tbb.Click += DebugWindow_Physics_Click;
        //    tb.Add(tbb);

        //    tbb = new ToolBarButton(UIManager);
        //    tbb.Init();
        //    tbb.Left = 330;
        //    tbb.Width = 100;
        //    tbb.Height = tb.Height - 1;
        //    tbb.Text = "AI";
        //    tbb.BackColor = new Color(55, 55, 55);
        //    tb.Add(tbb);

        //    tbb = new ToolBarButton(UIManager);
        //    tbb.Init();
        //    tbb.Left = 440;
        //    tbb.Width = 100;
        //    tbb.Height = tb.Height - 1;
        //    tbb.Text = "UI";
        //    tbb.BackColor = new Color(55, 55, 55);
        //    tb.Add(tbb);

        //    tbb = new ToolBarButton(UIManager);
        //    tbb.Init();
        //    tbb.Left = 550;
        //    tbb.Width = 100;
        //    tbb.Height = tb.Height - 1;
        //    tbb.Text = "Lighting";
        //    tbb.BackColor = new Color(55, 55, 55);
        //    tbb.Click += DebugWindow_Lighting_Click;
        //    tb.Add(tbb);

        //    InitializeMemoryWindow();
        //    InitializePerformanceWindow();
        //    InitializePhysicsWindow();
        //    InitializeLightingWindow();
        //}

        //protected void InitializeMemoryWindow()
        //{
        //    Window MemoryWindow = new Window(UIManager);
        //    MemoryWindow.Init();
        //    MemoryWindow.Top = 50;
        //    MemoryWindow.Left = 50;
        //    MemoryWindow.Width = 300;
        //    MemoryWindow.Height = 300;
        //    MemoryWindow.Visible = false;
        //    MemoryWindow.BorderVisible = true;
        //    MemoryWindow.Movable = true;
        //    MemoryWindow.Resizable = true;
        //    MemoryWindow.CloseButtonVisible = true;
        //    MemoryWindow.BackColor = Color.Black * 0.6f;
        //    MemoryWindow.Color = Color.Black;
        //    MemoryWindow.Alpha = 250;
        //    MemoryWindow.Shadow = false;
        //    MemoryWindow.Text = "Memory Debug Window";
        //    MemoryWindow.Name = "Memory Window";
        //    UIManager.Add(MemoryWindow);

        //    Label lbl = new Label(UIManager);
        //    lbl.Init();
        //    lbl.Top = 5;
        //    lbl.Left = 5;
        //    lbl.Text = "Memory Allocated: " + string.Format("{0:f2}", (Process.GetCurrentProcess().WorkingSet64 / 1024f / 1024f)) + " MiB";
        //    lbl.Name = "Mem Alloc";
        //    lbl.Width = 150;
        //    MemoryWindow.Add(lbl);
        //}

        //protected void InitializeLightingWindow()
        //{
        //    Window LightingWindow = new Window(UIManager);
        //    LightingWindow.Init();
        //    LightingWindow.Top = 50;
        //    LightingWindow.Left = 50;
        //    LightingWindow.Width = 300;
        //    LightingWindow.Height = 300;
        //    LightingWindow.Visible = false;
        //    LightingWindow.BorderVisible = true;
        //    LightingWindow.Movable = true;
        //    LightingWindow.Resizable = true;
        //    LightingWindow.CloseButtonVisible = true;
        //    LightingWindow.BackColor = Color.Black * 0.6f;
        //    LightingWindow.Color = Color.Black;
        //    LightingWindow.Alpha = 250;
        //    LightingWindow.Shadow = false;
        //    LightingWindow.Text = "Lighting Debug Window";
        //    LightingWindow.Name = "Lighting Window";
        //    UIManager.Add(LightingWindow);

        //    Label lbl = new Label(UIManager);
        //    lbl.Init();
        //    lbl.Top = 5;
        //    lbl.Left = 5;
        //    lbl.Text = "Lights: ";
        //    lbl.Name = "Lights";
        //    lbl.Width = 150;
        //    LightingWindow.Add(lbl);

        //    lbl = new Label(UIManager);
        //    lbl.Init();
        //    lbl.Top = 25;
        //    lbl.Left = 5;
        //    lbl.Text = "Hulls: ";
        //    lbl.Name = "Hulls";
        //    lbl.Width = 150;
        //    LightingWindow.Add(lbl);
        //}

        //protected void InitializePerformanceWindow()
        //{
        //    Window PerformanceWindow = new Window(UIManager);
        //    PerformanceWindow.Init();
        //    PerformanceWindow.Top = 50;
        //    PerformanceWindow.Left = 50;
        //    PerformanceWindow.Width = 300;
        //    PerformanceWindow.Height = 300;
        //    PerformanceWindow.Visible = false;
        //    PerformanceWindow.BorderVisible = true;
        //    PerformanceWindow.Movable = true;
        //    PerformanceWindow.Resizable = true;
        //    PerformanceWindow.CloseButtonVisible = true;
        //    PerformanceWindow.BackColor = Color.Black * 0.6f;
        //    PerformanceWindow.Color = Color.Black;
        //    PerformanceWindow.Alpha = 250;
        //    PerformanceWindow.Shadow = false;
        //    PerformanceWindow.Text = "Performance Debug Window";
        //    PerformanceWindow.Name = "Performance Window";
        //    UIManager.Add(PerformanceWindow);

        //    Label lbl = new Label(UIManager);
        //    lbl.Init();
        //    lbl.Top = 5;
        //    lbl.Left = 5;
        //    lbl.Text = "Draws Per Second: ";
        //    lbl.Name = "Draws Per Second";
        //    lbl.Width = 150;
        //    PerformanceWindow.Add(lbl);

        //    lbl = new Label(UIManager);
        //    lbl.Init();
        //    lbl.Top = 25;
        //    lbl.Left = 5;
        //    lbl.Text = "Updates Per Second: ";
        //    lbl.Name = "Updates Per Second";
        //    lbl.Width = 150;
        //    PerformanceWindow.Add(lbl);
        //}

        //protected void InitializePhysicsWindow()
        //{
        //    Window PhysicsWindow = new Window(UIManager);
        //    PhysicsWindow.Init();
        //    PhysicsWindow.Top = 50;
        //    PhysicsWindow.Left = 50;
        //    PhysicsWindow.Width = 350;
        //    PhysicsWindow.Height = 300;
        //    PhysicsWindow.Visible = false;
        //    PhysicsWindow.BorderVisible = true;
        //    PhysicsWindow.Movable = true;
        //    PhysicsWindow.Resizable = true;
        //    PhysicsWindow.CloseButtonVisible = true;
        //    PhysicsWindow.BackColor = Color.Black * 0.6f;
        //    PhysicsWindow.Color = Color.Black;
        //    PhysicsWindow.Alpha = 250;
        //    PhysicsWindow.Shadow = false;
        //    PhysicsWindow.Text = "Physics Debug Window";
        //    PhysicsWindow.Name = "Physics Window";
        //    UIManager.Add(PhysicsWindow);

        //    Label lbl = new Label(UIManager);
        //    lbl.Init();
        //    lbl.Top = 5;
        //    lbl.Left = 5;
        //    lbl.Text = "Updates Per Second: ";
        //    lbl.Name = "Updates Per Second";
        //    lbl.Width = 150;
        //    PhysicsWindow.Add(lbl);

        //    lbl = new Label(UIManager);
        //    lbl.Init();
        //    lbl.Top = 25;
        //    lbl.Left = 5;
        //    lbl.Text = "Physics Objects:";
        //    lbl.Width = 150;
        //    PhysicsWindow.Add(lbl);

        //    lbl = new Label(UIManager);
        //    lbl.Init();
        //    lbl.Top = 45;
        //    lbl.Left = 5;
        //    lbl.Name = "Bodies";
        //    lbl.Width = 150;
        //    PhysicsWindow.Add(lbl);

        //    lbl = new Label(UIManager);
        //    lbl.Init();
        //    lbl.Top = 65;
        //    lbl.Left = 5;
        //    lbl.Name = "Fixtures";
        //    lbl.Width = 150;
        //    PhysicsWindow.Add(lbl);

        //    lbl = new Label(UIManager);
        //    lbl.Init();
        //    lbl.Top = 85;
        //    lbl.Left = 5;
        //    lbl.Name = "Contacts";
        //    lbl.Width = 150;
        //    PhysicsWindow.Add(lbl);

        //    lbl = new Label(UIManager);
        //    lbl.Init();
        //    lbl.Top = 105;
        //    lbl.Left = 5;
        //    lbl.Name = "Joints";
        //    lbl.Width = 150;
        //    PhysicsWindow.Add(lbl);

        //    lbl = new Label(UIManager);
        //    lbl.Init();
        //    lbl.Top = 125;
        //    lbl.Left = 5;
        //    lbl.Name = "Controllers";
        //    lbl.Width = 150;
        //    PhysicsWindow.Add(lbl);

        //    lbl = new Label(UIManager);
        //    lbl.Init();
        //    lbl.Top = 145;
        //    lbl.Left = 5;
        //    lbl.Name = "Proxies";
        //    lbl.Width = 150;
        //    PhysicsWindow.Add(lbl);


        //    lbl = new Label(UIManager);
        //    lbl.Init();
        //    lbl.Top = 25;
        //    lbl.Left = 150;
        //    lbl.Text = "Update time:";
        //    lbl.Width = 150;
        //    PhysicsWindow.Add(lbl);

        //    lbl = new Label(UIManager);
        //    lbl.Init();
        //    lbl.Top = 45;
        //    lbl.Left = 150;
        //    lbl.Name = "Body";
        //    lbl.Width = 150;
        //    PhysicsWindow.Add(lbl);

        //    lbl = new Label(UIManager);
        //    lbl.Init();
        //    lbl.Top = 65;
        //    lbl.Left = 150;
        //    lbl.Name = "Contact";
        //    lbl.Width = 150;
        //    PhysicsWindow.Add(lbl);

        //    lbl = new Label(UIManager);
        //    lbl.Init();
        //    lbl.Top = 85;
        //    lbl.Left = 150;
        //    lbl.Name = "CCD";
        //    lbl.Width = 150;
        //    PhysicsWindow.Add(lbl);

        //    lbl = new Label(UIManager);
        //    lbl.Init();
        //    lbl.Top = 105;
        //    lbl.Left = 150;
        //    lbl.Name = "Joint";
        //    lbl.Width = 150;
        //    PhysicsWindow.Add(lbl);

        //    lbl = new Label(UIManager);
        //    lbl.Init();
        //    lbl.Top = 125;
        //    lbl.Left = 150;
        //    lbl.Name = "Controller";
        //    lbl.Width = 150;
        //    PhysicsWindow.Add(lbl);

        //    lbl = new Label(UIManager);
        //    lbl.Init();
        //    lbl.Top = 145;
        //    lbl.Left = 150;
        //    lbl.Name = "Total";
        //    lbl.Width = 150;
        //    PhysicsWindow.Add(lbl);
        //}

        //protected void UpdateMemoryWindow()
        //{
        //    if (UIManager.GetControl("Memory Window").Visible)
        //    {
        //        UIManager.GetControl("Memory Window").GetControl("Mem Alloc").Text = "Mem Alloc: "
        //            + string.Format("{0:f2}", (Process.GetCurrentProcess().WorkingSet64 / 1024f / 1024f))
        //            + " MiB";
        //    }
        //}

        //protected void UpdateLightingWindow()
        //{
        //    if (UIManager.GetControl("Lighting Window").Visible)
        //    {
        //        Krypton.KryptonEngine engine = ScreenController.screens[ScreenController.screenStack.Peek()].lightingController.LightingEngine;

        //        UIManager.GetControl("Lighting Window").GetControl("Lights").Text = "Lights: " + engine.Lights.Count();

        //        UIManager.GetControl("Lighting Window").GetControl("Hulls").Text = "Hulls: " + engine.Hulls.Count();
        //    }
        //}

        //protected void UpdatePhysicsWindow()
        //{
        //    if (UIManager.GetControl("Physics Window").Visible)
        //    {
        //        FarseerPhysics.Dynamics.World World = ScreenController.screens[ScreenController.screenStack.Peek()].physicsController.world;

        //        UIManager.GetControl("Physics Window").GetControl("Updates Per Second").Text = "Updates Per Second: " 
        //            + ScreenController.screens[ScreenController.screenStack.Peek()].physicsController.UpdateRate.ToString();

        //        UIManager.GetControl("Physics Window").GetControl("Bodies").Text = "Bodies: "
        //            + World.BodyList.Count.ToString();

        //        int fixtureCount = 0;
        //        for (int i = 0; i < World.BodyList.Count; i++)
        //        {
        //            fixtureCount += World.BodyList[i].FixtureList.Count;
        //        }
        //        UIManager.GetControl("Physics Window").GetControl("Fixtures").Text = "Bodies: "
        //            + fixtureCount.ToString();

        //        UIManager.GetControl("Physics Window").GetControl("Contacts").Text = "Contacts: "
        //            + World.ContactList.Count.ToString();

        //        UIManager.GetControl("Physics Window").GetControl("Joints").Text = "Joints: "
        //            + World.JointList.Count.ToString();

        //        UIManager.GetControl("Physics Window").GetControl("Controllers").Text = "Controllers: "
        //            + World.ControllerList.Count.ToString();

        //        UIManager.GetControl("Physics Window").GetControl("Proxies").Text = "Proxies: "
        //            + World.ProxyCount.ToString();




        //        UIManager.GetControl("Physics Window").GetControl("Body").Text = "Body: "
        //            + string.Format("{0} ms", World.SolveUpdateTime / TimeSpan.TicksPerMillisecond);

        //        UIManager.GetControl("Physics Window").GetControl("Contact").Text = "Contact: "
        //            + string.Format("{0} ms", World.ContactsUpdateTime / TimeSpan.TicksPerMillisecond);

        //        UIManager.GetControl("Physics Window").GetControl("CCD").Text = "CCD: "
        //            + string.Format("{0} ms", World.ContinuousPhysicsTime / TimeSpan.TicksPerMillisecond);

        //        UIManager.GetControl("Physics Window").GetControl("Joint").Text = "Joint: "
        //            + string.Format("{0} ms", World.Island.JointUpdateTime / TimeSpan.TicksPerMillisecond);

        //        UIManager.GetControl("Physics Window").GetControl("Controller").Text = "Controller: "
        //            + string.Format("{0} ms", World.ControllersUpdateTime / TimeSpan.TicksPerMillisecond);

        //        UIManager.GetControl("Physics Window").GetControl("Total").Text = "Total: "
        //            + string.Format("{0} ms", World.UpdateTime / TimeSpan.TicksPerMillisecond);
        //    }
        //}

        //protected void UpdatePerformanceWindow()
        //{
        //    if (UIManager.GetControl("Performance Window").Visible)
        //    {
        //        UIManager.GetControl("Performance Window").GetControl("Draws Per Second").Text = "Draws Per Second: " + UIController.FrameRate.ToString();
        //        UIManager.GetControl("Performance Window").GetControl("Updates Per Second").Text = "Updates Per Second: " + UIController.UpdateRate.ToString();
        //    }
        //}

        #endregion

        protected void UpdateDebugWindows()
        {
            if(DebugEnabled || true)
            {
                if(!DebugUpdateWatch.IsRunning)
                {
                    DebugUpdateWatch.Restart();
                }

                if(DebugUpdateWatch.Elapsed.TotalSeconds >= 0.25)
                {
                    //UpdateMemoryWindow();
                    //UpdatePerformanceWindow();
                    //UpdatePhysicsWindow();
                    //UpdateLightingWindow();

                    DebugUpdateWatch.Restart();
                }
            }
            else
            {
                DebugUpdateWatch.Stop();
            }
        }

        //void DebugWindow_Memory_Click(object sender, TomShane.Neoforce.Controls.EventArgs e)
        //{
        //    UIManager.GetControl("Memory Window").Visible = true;
        //    UIManager.GetControl("Memory Window").Enabled = true;
        //}

        //void DebugWindow_Performance_Click(object sender, TomShane.Neoforce.Controls.EventArgs e)
        //{
        //    UIManager.GetControl("Performance Window").Visible = true;
        //    UIManager.GetControl("Performance Window").Enabled = true;
        //}

        //void DebugWindow_Lighting_Click(object sender, TomShane.Neoforce.Controls.EventArgs e)
        //{
        //    UIManager.GetControl("Lighting Window").Visible = true;
        //    UIManager.GetControl("Lighting Window").Enabled = true;
        //}

        //void DebugWindow_Physics_Click(object sender, TomShane.Neoforce.Controls.EventArgs e)
        //{
        //    UIManager.GetControl("Physics Window").Visible = true;
        //    UIManager.GetControl("Physics Window").Enabled = true;
        //}

        public override void Update()
        {
            UpdateCounter++;

            //UpdateKeyboardStates();
            Mouse.Update();

            ElapsedTime += GameController.gameTime.ElapsedGameTime;

            if (ElapsedTime > TimeSpan.FromSeconds(1))
            {
                ElapsedTime = TimeSpan.Zero;
                FrameRate = FrameCounter;
                FrameCounter = 0;

                UpdateRate = UpdateCounter;
                UpdateCounter = 0;
            }

            if (DebugEnabled)
            {
                //UIManager.Update(GameController.gameTime);
                UpdateDebugWindows();
            }

            //if (CurrentKeyboardState().IsKeyUp(Keys.F12)
            //    && KeyboardStates.Count() > 1
            //    && !PreviousKeyboardState().IsKeyUp(Keys.F12))
            //{
            //    DebugEnabled = !DebugEnabled;
            //}

            base.Update();
        }

        public override void Draw()
        {
            FrameCounter++;

            if (DebugEnabled)
            {
                Screen currentScreen = ScreenController.screens[ScreenController.screenStack.Peek()];

                currentScreen.spriteBatch.Begin(
                        SpriteSortMode.BackToFront,
                        BlendState.AlphaBlend,
                        SamplerState.AnisotropicClamp,
                        DepthStencilState.Default,
                        RasterizerState.CullNone,
                        null,
                        Matrix.Identity);

                int i = 0;
                //foreach (Keys key in CurrentKeyboardState().GetPressedKeys())
                //{
                //    currentScreen.spriteBatch.DrawString(
                //        DefaultFont,
                //        key.ToString(),
                //        new Vector2(
                //            GameController.game.GraphicsDevice.Viewport.Width - 100,
                //            10)
                //            + (Vector2.UnitY * 20 * i),
                //        Color.White);

                //    i++;
                //}

                if (Mouse.State.LeftButton == ButtonState.Pressed)
                {
                    currentScreen.spriteBatch.DrawString(
                        DefaultFont,
                        "Mouse: Left",
                        new Vector2(
                            GameController.game.GraphicsDevice.Viewport.Width - 100,
                            10)
                            + (Vector2.UnitY * 20 * i),
                        Color.White);

                    i++;
                }

                if (Mouse.State.RightButton == ButtonState.Pressed)
                {
                    currentScreen.spriteBatch.DrawString(
                        DefaultFont,
                        "Mouse: Right",
                        new Vector2(
                            GameController.game.GraphicsDevice.Viewport.Width - 100,
                            10)
                            + (Vector2.UnitY * 20 * i),
                        Color.White);

                    i++;
                }

                if (Mouse.State.MiddleButton == ButtonState.Pressed)
                {
                    currentScreen.spriteBatch.DrawString(
                        DefaultFont,
                        "Mouse: Middle",
                        new Vector2(
                            GameController.game.GraphicsDevice.Viewport.Width - 100,
                            10)
                            + (Vector2.UnitY * 20 * i),
                        Color.White);

                    i++;
                }

                currentScreen.spriteBatch.End();

                //UIManager.BeginDraw(GameController.gameTime);
                //UIManager.Draw(GameController.gameTime);
                //UIManager.EndDraw();
            }

            base.Draw();
        }

        public override void Window_ClientSizeChanged(object sender, System.EventArgs e)
        {
            DisplayMechanics.ResetGraphicsState();

            //UIManager.Input.InputOffset = new InputOffset(
            //    0,
            //    0,
            //    UIManager.ScreenWidth / (float)UIManager.TargetWidth,
            //    UIManager.ScreenHeight / (float)UIManager.TargetHeight);

            //UIManager.GetControl("Debug Window").GetControl("Toolbar").Width = UIManager.ScreenWidth;
        }

        void UIController_MouseWheelRotated(float ticks) { OnMouseWheelMove(ticks); }

        void UIController_MouseMoved(float x, float y) { OnMouseMove(x, y); }

        void UIController_MouseButtonPressed(MouseButtons buttons) { OnMouseButtonDown(buttons); }

        void UIController_MouseButtonReleased(MouseButtons buttons) { OnMouseButtonUp(buttons); }

        void UIController_CharacterEntered(char character) { OnKeyboardCharacter(character); }

        void UIController_KeyPressed(Keys key) { OnKeyboardKeyDown(key); }

        void UIController_KeyReleased(Keys key) { OnKeyboardKeyUp(key); }

        public void OnKeyboardKeyDown(Keys key)
        {
            if (KeyboardKeyDown != null) KeyboardKeyDown(key);
        }

        public void OnKeyboardKeyUp(Keys key)
        {
            if (KeyboardKeyUp != null) KeyboardKeyUp(key);
        }

        public void OnKeyboardCharacter(char character)
        {
            if (KeyboardCharacter != null) KeyboardCharacter(character);
        }

        public void OnMouseButtonDown(MouseButtons buttons)
        {
            if (MouseButtonDown != null) MouseButtonDown(buttons);

            switch (buttons)
            {
                case MouseButtons.Left:
                    if (MouseLeftDown != null) MouseLeftDown(this, new EventArgs());
                    break;
                case MouseButtons.Right:
                    if (MouseRightDown != null) MouseRightDown(this, new EventArgs());
                    break;
                case MouseButtons.Middle:
                    if (MouseMiddleDown != null) MouseMiddleDown(this, new EventArgs());
                    break;
                case MouseButtons.X1:
                    if (MouseX1Down != null) MouseX1Down(this, new EventArgs());
                    break;
                case MouseButtons.X2:
                    if (MouseX2Down != null) MouseX2Down(this, new EventArgs());
                    break;
            }
        }

        public void OnMouseButtonUp(MouseButtons buttons)
        {
            if (MouseButtonUp != null) MouseButtonUp(buttons);

            switch (buttons)
            {
                case MouseButtons.Left:
                    if (MouseLeftUp != null) MouseLeftUp(this, new EventArgs());
                    break;
                case MouseButtons.Right:
                    if (MouseRightUp != null) MouseRightUp(this, new EventArgs());
                    break;
                case MouseButtons.Middle:
                    if (MouseMiddleUp != null) MouseMiddleUp(this, new EventArgs());
                    break;
                case MouseButtons.X1:
                    if (MouseX1Up != null) MouseX1Up(this, new EventArgs());
                    break;
                case MouseButtons.X2:
                    if (MouseX2Up != null) MouseX2Up(this, new EventArgs());
                    break;
            }
        }

        public void OnMouseMove(float x, float y)
        {
            if (MouseMove != null) MouseMove(x, y);
        }

        public void OnMouseWheelMove(float ticks)
        {
            if (MouseWheelMove != null) MouseWheelMove(ticks);
        }
    }
}
