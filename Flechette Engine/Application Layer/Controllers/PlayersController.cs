﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flechette_Engine
{
    /// <summary>
    /// Controls the Players and Teams within the game.
    /// </summary>
    public class PlayersController : Controller
    {
        /// <summary>
        /// All of the players in the game.
        /// </summary>
        static public List<Player> Players { get; set; }

        /// <summary>
        /// All of the teams in the game.
        /// </summary>
        static public List<Team> Teams { get; set; }

        /// <summary>
        /// The current active player.
        /// Used for turn-based games, mostly.
        /// </summary>
        static public Player CurrentPlayer { get; set; }

        /// <summary>
        /// Circular queue to control player turns.
        /// This should mostly not be set manually as it will be set automatically 
        /// in most cases.
        /// </summary>
        static public Queue<Player> PlayerQueue { get; set; }

        /// <summary>
        /// True if Players shouldn't be Updated automatically.
        /// </summary>
        static public bool ManualPlayerUpdates { get; set; }

        /// <summary>
        /// True if Players shouldn't be Drawn automatically.
        /// </summary>
        static public bool ManualPlayerDraws { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public PlayersController()
            : base()
        {
            Players = new List<Player>();
            Teams = new List<Team>();

            PlayerQueue = new Queue<Player>();

            ManualPlayerUpdates = true;
            ManualPlayerDraws = true;
        }

        /// <summary>
        /// Validate the lists and queue.
        /// Update all players if allowed.
        /// </summary>
        public override void Update()
        {
            // Validate the Players list
            if (Players == null)
                Players = new List<Player>();

            // Validate the PlayerQueue
            if (PlayerQueue == null)
                PlayerQueue = new Queue<Player>();

            // Rebuild the queue if it's empty and 
            // we have players in the list
            if (PlayerQueue.Count() == 0
                && Players.Count() > 0)
            {
                for (int i = 0; i < Players.Count(); i++)
                {
                    PlayerQueue.Enqueue(Players.ElementAt(i));
                }
            }

            // Set the current player if we don't have one
            if(CurrentPlayer == null)
                NextPlayer();

            // Update all the players if we're allowed
            if (!ManualPlayerUpdates
                && Players != null)
            {
                foreach(Player pl in Players)
                {
                    pl.Update();
                }
            }

            base.Update();
        }

        public override void Draw()
        {
            // Draw all the players if we're allowed
            if (!ManualPlayerDraws
                && Players != null)
            {
                foreach (Player pl in Players)
                {
                    pl.Draw();
                }
            }

            base.Draw();
        }

        static public void Reset()
        {
            if(Players!=null)
            {
                //foreach(Player pl in Players)
                //{
                //    pl.Dispose();
                //}
                Players.Clear();
            }

            if (PlayerQueue != null)
            {
                //foreach (Player pl in PlayerQueue)
                //{
                //    pl.Dispose();
                //}
                PlayerQueue.Clear();
            }

            CurrentPlayer = null;
        }

        static public Player NextPlayer()
        {
            List<int> hashes = new List<int>();
            for (int i = 0; i < Players.Count(); i++)
            {
                hashes.Add(Players[i].GetHashCode());
            }

            List<int> hashes2 = new List<int>();
            for (int i = 0; i < PlayerQueue.Count(); i++)
            {
                hashes2.Add(PlayerQueue.ElementAt(i).GetHashCode());
            }

            // If the queue is populated
            if (PlayerQueue != null && PlayerQueue.Count() > 0)
            {
                // Get the next player
                CurrentPlayer = PlayerQueue.Dequeue();

                // Wrap the queue
                PlayerQueue.Enqueue(CurrentPlayer);
            }

            // Return the new current player
            return CurrentPlayer;
        }
    }
}
