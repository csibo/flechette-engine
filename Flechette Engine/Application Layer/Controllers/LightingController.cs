﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Krypton;
using Krypton.Lights;
using Krypton.Common;

namespace Flechette_Engine
{
    /// <summary>
    /// Controls the lighting engine.
    /// </summary>
    public class LightingController : Controller
    {
        public KryptonEngine LightingEngine { get; set; }

        public Screen ContainingScreen { get; set; }

        /// <summary>
        /// Used to get our ShadowHull object from a Krypton.ShadowHull object.
        /// You shouldn't need to edit the values manually.
        /// </summary>
        public Dictionary<Krypton.ShadowHull, ShadowHull> Hulls = new Dictionary<Krypton.ShadowHull, ShadowHull>();

        /// <summary>
        /// Default constructor.
        /// </summary>
        public LightingController(Screen theContainingScreen)
            : base()
        {
            ContainingScreen = theContainingScreen;

            LightingEngine = new KryptonEngine(GameController.game, "KryptonEffect");
            LightingEngine.Initialize();
            LightingEngine.AmbientColor = Color.White;
        }

        public override void Update()
        {
            if(Enabled)
            {
                //DebugEnabled = (UIController.UIManager.GetControl("Lighting Window").Visible
                //    && UIController.DebugEnabled);

                DebugEnabled = UIController.DebugEnabled;
            }

            base.Update();
        }

        /// <summary>
        /// Performs pre-draw steps.
        /// </summary>
        public virtual void DrawPrepass()
        {
            if (Visible)
            {
                LightingEngine.Matrix =
                  ContainingScreen.activeCamera.ViewMatrix
                * ContainingScreen.activeCamera.ProjectionMatrix
                * ContainingScreen.activeCamera.WorldMatrix;

                LightingEngine.CullMode = CullMode.None;
                LightingEngine.Bluriness = 3;
                LightingEngine.LightMapPrepare();
            }
        }

        /// <summary>
        /// Draws the lighting effects.
        /// </summary>
        public override void Draw()
        {
            if (Visible)
            {
                LightingEngine.Draw(GameController.gameTime);
            }

            base.Draw();
        }

        /// <summary>
        /// Adds a ShadowHull to the list.
        /// </summary>
        /// <param name="hull">ShadowHull to add.</param>
        public void Add(ShadowHull hull)
        {
            LightingEngine.Hulls.Add(hull.Hull);
        }

        /// <summary>
        /// Adds a Light to the list.
        /// </summary>
        /// <param name="light">Light to add.</param>
        public void Add(Light light)
        {
            LightingEngine.Lights.Add(light);
        }

        /// <summary>
        /// Draws the debug output.
        /// </summary>
        public virtual void DrawDebug()
        {
            LightingEngine.RenderHelper.Effect.CurrentTechnique = LightingEngine.RenderHelper.Effect.Techniques["DebugDraw"];
            GameController.game.GraphicsDevice.RasterizerState = new RasterizerState()
            {
                CullMode = CullMode.None,
                FillMode = FillMode.WireFrame,
            };
            foreach (var effectPass in LightingEngine.RenderHelper.Effect.CurrentTechnique.Passes)
            {
                effectPass.Apply();
            }

            if(DebugEnabled)
            {
                GetDebugHulls();
                DrawDebug_ApplyPass();

                //DrawDebug_LightBounds();

                DrawDebug_Lights();
            }
        }

        protected virtual void DrawDebug_ApplyPass()
        {
            foreach (var effectPass in LightingEngine.RenderHelper.Effect.CurrentTechnique.Passes)
            {
                effectPass.Apply();
                LightingEngine.RenderHelper.BufferDraw();
            }
        }

        protected virtual void GetDebugHulls()
        {
            LightingEngine.RenderHelper.ShadowHullVertices.Clear();
            LightingEngine.RenderHelper.ShadowHullIndicies.Clear();

            foreach (var hull in LightingEngine.Hulls)
            {
                LightingEngine.RenderHelper.BufferAddShadowHull(hull);
            }
        }

        protected virtual void DrawDebug_LightBounds()
        {
            foreach (Light light in LightingEngine.Lights)
            {
                Rectangle rect = new Rectangle(
                    (int)light.Bounds.Left, 
                    (int)light.Bounds.Top,
                    (int)(light.Bounds.Right - light.Bounds.Left),
                    (int)(light.Bounds.Bottom - light.Bounds.Top));

                Primitives.DrawRectangle(rect, light.Color, true);
            }
        }

        protected virtual void DrawDebug_Lights()
        {
            foreach (Light light in LightingEngine.Lights)
            {
                Primitives.DrawSolidCircle(ContainingScreen.activeCamera.Project(light.Position), 10, Vector2.Zero, light.Color);
            }
        }
    }
}
