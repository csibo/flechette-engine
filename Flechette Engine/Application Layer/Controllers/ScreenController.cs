﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Flechette_Engine
{
    public class ScreenController : Controller
    {
        public static Dictionary<string, Screen> screens;
        public static Stack<string> screenStack;

        public ScreenController()
            : base()
        {
            screens = new Dictionary<string, Screen>();
            screenStack = new Stack<string>();
        }

        protected override void Dispose(bool disposing)
        {
            foreach(Screen screen in screens.Values)
            {
                screen.Dispose();
            }

            base.Dispose(disposing);
        }

        public override void Update()
        {
            if (screenStack != null
                && screenStack.Count > 0)
            {
                screens[screenStack.First()].Update();
            }
            else
            {
                throw new Exception("No screens in stack.");
            }

            base.Update();
        }

        public override void Draw()
        {
            if (screenStack != null
                && screenStack.Count > 0)
            {
                screens[screenStack.First()].Draw();
            }
            else
            {
                throw new Exception("No screens in stack.");
            }

            base.Draw();
        }
    }
}
