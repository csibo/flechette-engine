﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Flechette_Engine
{
    /// <summary>
    /// Controls the game-wide logic.
    /// </summary>
    public class GameController : Controller
    {
        /// <summary>
        /// Singleton instance for this class.
        /// </summary>
        private static GameController instance;

        /// <summary>
        /// XNA game object.
        /// </summary>
        public static Game game;

        public static GraphicsDeviceManager graphics;

        /// <summary>
        /// Controls all the Screens in the game.
        /// </summary>
        public static ScreenController screenController;

        /// <summary>
        /// Controls the UI for the game (ie, input states).
        /// </summary>
        public static UIController uiController;

        /// <summary>
        /// Controls all the Players and Teams for the game.
        /// </summary>
        public static PlayersController playersController;

        /// <summary>
        /// Tracks elapsed and total game time.
        /// </summary>
        public static GameTime gameTime;

        /// <summary>
        /// Default Constructor.
        /// </summary>
        private GameController()
            : base()
        {
            uiController = new UIController();

            screenController = new ScreenController();

            playersController = new PlayersController();
        }

        /// <summary>
        /// Creates a singleton instance.
        /// </summary>
        public static GameController Instance(Game theGame, GraphicsDeviceManager theGraphics)
        {
            if (instance == null)
            {
                GameController.game = theGame;
                GameController.graphics = theGraphics;

                instance = new GameController();

                instance.Initialize();

                GameController.game.Components.Add(instance);
            }

            return instance;
        }

        /// <summary>
        /// Dispose of myself and the other Controllers.
        /// </summary>
        /// <param name="disposing">True to dispose of unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            screenController.Dispose();
            uiController.Dispose();
            playersController.Dispose();

            base.Dispose(disposing);
        }

        public override void Initialize()
        {
            uiController.Initialize();

            // Add default screens
            screenController.Initialize();
            ScreenController.screens.Add("Main Menu", new Menu_Default());
            ScreenController.screens.Add("Settings Menu", new Settings_Default());
            ScreenController.screens.Add("Play", new Play_Default());
            ScreenController.screenStack.Push("Main Menu");

            base.Initialize();
        }

        /// <summary>
        /// Update all my Controllers.
        /// </summary>
        /// <param name="theGameTime">XNA GameTime</param>
        public override void Update(GameTime theGameTime)
        {
            gameTime = theGameTime;

            uiController.Update();
            playersController.Update();
            screenController.Update();

            base.Update();
        }

        /// <summary>
        /// Draw all my Controllers.
        /// </summary>
        /// <param name="theGameTime">XNA GameTime</param>
        public override void Draw(GameTime theGameTime)
        {
            gameTime = theGameTime;

            screenController.Draw();
            playersController.Draw();
            uiController.Draw();

            base.Draw();
        }
    }
}
