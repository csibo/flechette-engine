﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Flechette_Engine
{
    /// <summary>
    /// Base class for generic controller.
    /// </summary>
    public class Controller : DrawableGameComponent
    {
        /// <summary>
        /// Enables debug output.
        /// </summary>
        public bool DebugEnabled { get; set; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Controller() 
            : base(GameController.game) 
        {
            DebugEnabled = false;

            GameController.game.Window.ClientSizeChanged += Window_ClientSizeChanged;
        }

        protected override void Dispose(bool disposing)
        {
            GameController.game.Window.ClientSizeChanged -= Window_ClientSizeChanged;

            base.Dispose(disposing);
        }

        public virtual void Update() 
        {
            base.Update(GameController.gameTime);
        }

        public virtual void Draw()
        {
            base.Draw(GameController.gameTime);
        }

        /// <summary>
        /// Called when the game window changes size.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void Window_ClientSizeChanged(object sender, EventArgs e)
        { }
    }
}
