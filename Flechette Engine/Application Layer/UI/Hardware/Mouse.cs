﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

using Nuclex;
using Nuclex.Input;

namespace Flechette_Engine
{
    public class Mouse
    {
        public struct MouseRecord
        {
            public MouseState State;
            public TimeSpan Time;
        }

        public struct TimesSince
        {
            public ulong MouseLeftDown;
            public ulong MouseRightDown;
            public ulong MouseMiddleDown;
            public ulong MouseX1Down;
            public ulong MouseX2Down;

            public ulong MouseLeftUp;
            public ulong MouseRightUp;
            public ulong MouseMiddleUp;
            public ulong MouseX1Up;
            public ulong MouseX2Up;
        }

        public MouseRecord LastLeftDown { get; set; }

        public MouseRecord LastLeftUp { get; set; }

        public MouseRecord LastRightDown { get; set; }

        public MouseRecord LastRightUp { get; set; }

        public MouseRecord LastMiddleDown { get; set; }

        public MouseRecord LastMiddleUp { get; set; }

        public MouseRecord LastX1Down { get; set; }

        public MouseRecord LastX1Up { get; set; }

        public MouseRecord LastX2Down { get; set; }

        public MouseRecord LastX2Up { get; set; }

        public MouseRecord LastMouseRest { get; set; }

        public MouseState State { get; protected set; }

        public MouseState PreviousState { get; protected set; }

        public Queue<MouseState> States { get; protected set; }

        public TimesSince TimeSince;

        public Mouse()
        {
            UIController.MouseLeftDown += UIController_MouseLeftDown;
            UIController.MouseRightDown += UIController_MouseRightDown;
            UIController.MouseMiddleDown += UIController_MouseMiddleDown;
            UIController.MouseX1Down += UIController_MouseX1Down;
            UIController.MouseX2Down += UIController_MouseX2Down;

            UIController.MouseLeftUp += UIController_MouseLeftUp;
            UIController.MouseRightUp += UIController_MouseRightUp;
            UIController.MouseMiddleUp += UIController_MouseMiddleUp;
            UIController.MouseX1Up += UIController_MouseX1Up;
            UIController.MouseX2Up += UIController_MouseX2Up;

            UIController.MouseMove += UIController_MouseMove;

            TimeSince = new TimesSince()
            {
                MouseLeftDown = 0,
                MouseRightDown = 0,
                MouseMiddleDown = 0,
                MouseX1Down = 0,
                MouseX2Down = 0,

                MouseLeftUp = 0,
                MouseRightUp = 0,
                MouseMiddleUp = 0,
                MouseX1Up = 0,
                MouseX2Up = 0,
            };

            States = new Queue<MouseState>();
        }

        public virtual void Update()
        {
            State = UIController.InputManager.GetMouse().GetState();
            PreviousState = State;

            States.Enqueue(State);

            while (States.Count > 600)
            {
                States.Dequeue();
            }

            TimeSince.MouseLeftDown += (ulong)GameController.gameTime.ElapsedGameTime.Milliseconds;
            TimeSince.MouseRightDown += (ulong)GameController.gameTime.ElapsedGameTime.Milliseconds;
            TimeSince.MouseMiddleDown += (ulong)GameController.gameTime.ElapsedGameTime.Milliseconds;
            TimeSince.MouseX1Down += (ulong)GameController.gameTime.ElapsedGameTime.Milliseconds;
            TimeSince.MouseX2Down += (ulong)GameController.gameTime.ElapsedGameTime.Milliseconds;
            TimeSince.MouseLeftUp += (ulong)GameController.gameTime.ElapsedGameTime.Milliseconds;
            TimeSince.MouseRightUp += (ulong)GameController.gameTime.ElapsedGameTime.Milliseconds;
            TimeSince.MouseMiddleUp += (ulong)GameController.gameTime.ElapsedGameTime.Milliseconds;
            TimeSince.MouseX1Up += (ulong)GameController.gameTime.ElapsedGameTime.Milliseconds;
            TimeSince.MouseX2Up += (ulong)GameController.gameTime.ElapsedGameTime.Milliseconds;
        }

        void UIController_MouseMove(float x, float y)
        {
            LastMouseRest = new MouseRecord()
            {
                Time = GameController.gameTime.TotalGameTime,
                State = State,
            };
        }

        void UIController_MouseLeftDown(object sender, EventArgs e)
        {
            LastLeftDown = new MouseRecord()
            {
                Time = GameController.gameTime.TotalGameTime,
                State = State,
            };

            TimeSince.MouseLeftDown = 0;
        }

        void UIController_MouseRightDown(object sender, EventArgs e)
        {
            LastRightDown = new MouseRecord()
            {
                Time = GameController.gameTime.TotalGameTime,
                State = State,
            };

            TimeSince.MouseRightDown = 0;
        }

        void UIController_MouseMiddleDown(object sender, EventArgs e)
        {
            LastMiddleDown = new MouseRecord()
            {
                Time = GameController.gameTime.TotalGameTime,
                State = State,
            };

            TimeSince.MouseMiddleDown = 0;
        }

        void UIController_MouseX1Down(object sender, EventArgs e)
        {
            LastX1Down = new MouseRecord()
            {
                Time = GameController.gameTime.TotalGameTime,
                State = State,
            };

            TimeSince.MouseX1Down = 0;
        }

        void UIController_MouseX2Down(object sender, EventArgs e)
        {
            LastX2Down = new MouseRecord()
            {
                Time = GameController.gameTime.TotalGameTime,
                State = State,
            };

            TimeSince.MouseX2Down = 0;
        }

        void UIController_MouseLeftUp(object sender, EventArgs e)
        {
            LastLeftUp = new MouseRecord()
            {
                Time = GameController.gameTime.TotalGameTime,
                State = State,
            };

            TimeSince.MouseLeftUp = 0;
        }

        void UIController_MouseRightUp(object sender, EventArgs e)
        {
            LastRightUp = new MouseRecord()
            {
                Time = GameController.gameTime.TotalGameTime,
                State = State,
            };

            TimeSince.MouseRightUp = 0;
        }

        void UIController_MouseMiddleUp(object sender, EventArgs e)
        {
            LastMiddleUp = new MouseRecord()
            {
                Time = GameController.gameTime.TotalGameTime,
                State = State,
            };

            TimeSince.MouseMiddleUp = 0;
        }

        void UIController_MouseX1Up(object sender, EventArgs e)
        {
            LastX1Up = new MouseRecord()
            {
                Time = GameController.gameTime.TotalGameTime,
                State = State,
            };

            TimeSince.MouseX1Up = 0;
        }

        void UIController_MouseX2Up(object sender, EventArgs e)
        {
            LastX2Up = new MouseRecord()
            {
                Time = GameController.gameTime.TotalGameTime,
                State = State,
            };

            TimeSince.MouseX2Up = 0;
        }
    }
}
