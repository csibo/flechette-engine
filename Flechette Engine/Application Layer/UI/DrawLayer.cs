﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Flechette_Engine
{
    /// <summary>
    /// A collection of drawable GameObjects.
    /// </summary>
    public class DrawLayer
    {
        /// <summary>
        /// Default Layer names.
        /// </summary>
        public enum DefaultNames
        {
            Background,
            Middleground,
            Foreground,
            GUI,
            Debug,
        }

        /// <summary>
        /// Enum to string dictionary for default names.
        /// </summary>
        public static readonly Dictionary<DefaultNames, string> DefaultLayers = new Dictionary<DefaultNames, string>()
        {
            { DefaultNames.Background, "Background" },
            { DefaultNames.Middleground, "Middleground" },
            { DefaultNames.Foreground, "Foreground" },
            { DefaultNames.GUI, "GUI" },
            { DefaultNames.Debug, "Debug" },
        };

        /// <summary>
        /// Member GameObjects.
        /// </summary>
        public List<GameObject> members = new List<GameObject>();

        /// <summary>
        /// Screen to which this belongs.
        /// </summary>
        public Screen ContainingScreen { get; set; }

        /// <summary>
        /// Reference string.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Relative order for each DrawLayer in this Screen.
        /// </summary>
        public float DrawOrder { get; set; }

        /// <summary>
        /// Parallax effect multiplier.
        /// </summary>
        public Vector2 Parallax { get; set; }

        /// <summary>
        /// Transformative matrix used for parallax effects.
        /// </summary>
        public Matrix ParallaxMatrix { get; protected set; }

        /// <summary>
        /// Adds itself to the ContainingScreen's DrawLayer list.
        /// </summary>
        /// <param name="name">Reference string.</param>
        /// <param name="containingScreen">Screen to which this belongs.</param>
        /// <param name="drawOrder">Relative draw order. Lower is drawn sooner.</param>
        /// <param name="parallax">Parallax offset multipliers. Vector2.One is no effect.</param>
        public DrawLayer(string name, Screen containingScreen, float drawOrder, Vector2 parallax)
        {
            Name = name;
            ContainingScreen = containingScreen;
            DrawOrder = drawOrder;

            ContainingScreen.drawLayers.Add(name, this);

            Parallax = parallax;
        }

        public void Update()
        {
            // Recalculate my matrix
            ParallaxMatrix = Matrix.CreateTranslation(
                ContainingScreen.activeCamera.ViewMatrix.Translation 
                * Geometry.Vector2To3(Parallax));
        }

        /// <summary>
        /// Draw member GameObjects.
        /// </summary>
        public void Draw()
        {
            if (members != null)
            {
                ContainingScreen.spriteBatch.Begin(
                    SpriteSortMode.BackToFront,
                    BlendState.AlphaBlend,
                    SamplerState.AnisotropicClamp,
                    DepthStencilState.Default,
                    RasterizerState.CullNone,
                    null,
                    ContainingScreen.activeCamera.ViewMatrix * (ContainingScreen.UseParallax ? ParallaxMatrix : Matrix.Identity));

                foreach (GameObject item in members.Where(e => e.Visible ))//&& ContainingScreen.activeCamera.Intersects(e)))
                {
                    item.Draw();
                }

                ContainingScreen.spriteBatch.End();
            }
        }
    }
}
