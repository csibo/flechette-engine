﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Flechette_Engine
{
    public class Skin
    {
        public SkinStateProfile Enabled { get; set; }

        public SkinStateProfile Disabled { get; set; }

        public SkinStateProfile Hovered { get; set; }

        public SkinStateProfile Pressed { get; set; }

        public SkinStateProfile Focused { get; set; }

        public Skin()
        {
            Enabled = new SkinStateProfile();
            Disabled = new SkinStateProfile();
            Hovered = new SkinStateProfile();
            Pressed = new SkinStateProfile();
            Focused = new SkinStateProfile();
        }
    }
}
