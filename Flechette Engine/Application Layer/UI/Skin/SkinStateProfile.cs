﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Flechette_Engine
{
    public class SkinStateProfile
    {
        public string BackgroundImage { get; set; }

        public string ForegroundImage { get; set; }

        public Color BackgroundColor { get; set; }

        public Color ForegroundColor { get; set; }

        public SkinFontProfile FontProfile { get; set; }

        public SkinStateProfile()
        {
            BackgroundImage = null;
            ForegroundImage = null;

            BackgroundColor = Color.White;
            ForegroundColor = Color.Black;

            FontProfile = new SkinFontProfile();
        }
    }
}
