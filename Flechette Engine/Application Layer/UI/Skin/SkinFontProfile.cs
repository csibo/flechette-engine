﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Flechette_Engine
{
    /// <summary>
    /// Defines a font for use within the UI.
    /// </summary>
    public class SkinFontProfile
    {
        public SpriteFont Font { get; set; }

        public float Scale { get; set; }

        public Color Color { get; set; }

        public SkinFontProfile()
        {
            Font = UIController.DefaultFont;
            Font.DefaultCharacter = '?';

            Scale = 1;
            Color = Color.Black;
        }
    }
}
