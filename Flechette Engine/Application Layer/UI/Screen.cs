﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Design;
using Microsoft.Xna.Framework.Graphics;

using FarseerPhysics;
using FarseerPhysics.DebugView;

namespace Flechette_Engine
{
    /// <summary>
    /// Represents a single screen in the game (ie. main menu, settings menu, map screen)
    /// </summary>
    public class Screen
    {
        /// <summary>
        /// Available update phases.
        /// </summary>
        public enum UpdatePhases
        {
            Prepass,
            FullUpdate,
            Postpass,
        }

        /// <summary>
        /// Controller for this screen's physics logic.
        /// </summary>
        public PhysicsController physicsController;

        /// <summary>
        /// Controller for this screen's AI logic.
        /// </summary>
        public AIController aiController;

        /// <summary>
        /// Current active Camera object.
        /// Must always be set before an update/draw cycle.
        /// </summary>
        public Camera activeCamera;

        public UIManager defaultManager;

        /// <summary>
        /// This should never need to be managed manually as Screen objects do the management safely.
        /// </summary>
        public SpriteBatch spriteBatch;

        /// <summary>
        /// This should never need to be managed manually as Screen objects do the management safely.
        /// </summary>
        public PrimitiveBatch primitiveBatch;

        /// <summary>
        /// All member GameObjects.
        /// </summary>
        public List<GameObject> gameObjects;

        /// <summary>
        /// All of the DrawLayers belonging to this screen.
        /// </summary>
        public Dictionary<string, DrawLayer> drawLayers = new Dictionary<string, DrawLayer>();

        private Thread physicsThread;
        private Thread aiThread;

        public LightingController lightingController;

        public bool UseParallax { get; set; }

        /// <summary>
        /// Default Constructor.
        /// </summary>
        public Screen()
        {
            gameObjects = new List<GameObject>();

            // Set up all the default DrawLayers
            new DrawLayer(DrawLayer.DefaultLayers[DrawLayer.DefaultNames.Background], this, 1f, new Vector2(0.1f, 0.1f));
            new DrawLayer(DrawLayer.DefaultLayers[DrawLayer.DefaultNames.Middleground], this, 0.75f, new Vector2(0.5f, 0.5f));
            new DrawLayer(DrawLayer.DefaultLayers[DrawLayer.DefaultNames.Foreground], this, 0.25f, new Vector2(1f, 1f));
            new DrawLayer(DrawLayer.DefaultLayers[DrawLayer.DefaultNames.GUI], this, 0.1f, new Vector2(1f, 1f));
            new DrawLayer(DrawLayer.DefaultLayers[DrawLayer.DefaultNames.Debug], this, 0f, new Vector2(1f, 1f));

            UseParallax = false;

            // Get a default camera
            activeCamera = new Camera(this);
            activeCamera.Size = new Vector2(GameController.game.GraphicsDevice.Viewport.Width, GameController.game.GraphicsDevice.Viewport.Width);

            // Must come after Camera setup
            spriteBatch = new SpriteBatch(GameController.game.GraphicsDevice);
            primitiveBatch = new PrimitiveBatch(GameController.game.GraphicsDevice, this, activeCamera.ProjectionMatrix, activeCamera.ViewMatrix);

            // Set up the physics engine for this screen
            physicsController = new PhysicsController(this);

            // Set up the AI engine
            aiController = new AIController();

            lightingController = new LightingController(this);

            if (Settings.ThreadedPhysics)
            {
                physicsThread = new Thread(new ThreadStart(physicsController.Run));
                physicsThread.Start();
            }

            defaultManager = new UIManager(this);

            GameController.game.Window.ClientSizeChanged += Window_ClientSizeChanged;
        }

        public virtual void Window_ClientSizeChanged(object sender, System.EventArgs e)
        {
        }

        /// <summary>
        /// Dispose of my GameObjects and stop any related threads.
        /// </summary>
        public virtual void Dispose()
        {
            if (physicsThread != null)
            {
                physicsThread.Abort();
            }

            if (gameObjects != null)
            {
                foreach (GameObject item in gameObjects)
                {
                    item.Dispose();
                }
            }
        }

        /// <summary>
        /// Update DrawLayers, AI, GUI, and physics.
        /// </summary>
        public virtual void Update()
        {
            if (drawLayers != null)
            {
                foreach (DrawLayer layer in drawLayers.Values)
                {
                    layer.Update();
                }
            }

            aiController.Update();

            if (!Settings.ThreadedPhysics)
            {
                physicsController.Update();
            }

            UpdateComponents();

            lightingController.Update();
        }

        /// <summary>
        /// Run the update passes on all enabled game objects.
        /// </summary>
        private void UpdateComponents()
        {
            // If there are any objects
            if (gameObjects != null)
            {
                // Get all the enabled objects
                List<GameObject> toUpdate = gameObjects.Where(e => e.Enabled).ToList();

                // If there are any enabled objects
                if(toUpdate != null && toUpdate.Count() > 0)
                {
                    // Run the prepass logic on all items
                    DoUpdatePhase(toUpdate, UpdatePhases.Prepass);

                    // Run the update logic on all items
                    DoUpdatePhase(toUpdate, UpdatePhases.FullUpdate);

                    // Run the postpass logic on all items
                    DoUpdatePhase(toUpdate, UpdatePhases.Postpass);
                }
            }
        }

        /// <summary>
        /// Uses threadpool to run the specified update phase on the specified objects.
        /// </summary>
        /// <param name="toUpdate">Objects to update</param>
        /// <param name="phase">Current update phase</param>
        private void DoUpdatePhase(List<GameObject> toUpdate, UpdatePhases phase)
        {
            // The source of your work items, create a sequence of Task instances.
            Task[] tasks = Enumerable.Range(0, toUpdate.Count()).Select(i =>
                Task.Factory.StartNew(() =>
                {
                    switch (phase)
                    {
                        case UpdatePhases.Prepass:
                            toUpdate[i].UpdatePrepass();
                            break;
                        case UpdatePhases.FullUpdate:
                            toUpdate[i].FullUpdate();
                            break;
                        case UpdatePhases.Postpass:
                            toUpdate[i].UpdatePostpass();
                            break;
                    }
                }
            )).ToArray();

            // Wait on all the tasks.
            Task.WaitAll(tasks);
        }

        /// <summary>
        /// Draw the DrawLayers, AI, physics, and GUI.
        /// </summary>
        public virtual void Draw()
        {
            // We need a camera to not shit ourselves further down
            if (activeCamera == null)
            {
                throw new Exception("No active camera set.");
            }

            if (lightingController.LightingEngine.Lights.Count() > 0)
                lightingController.DrawPrepass();

            // Set the background color of the frame
            GameController.game.GraphicsDevice.Clear(activeCamera.BackColor);

            // Draw the DrawLayers
            DrawComponents();

            if(lightingController.LightingEngine.Lights.Count() > 0)
                lightingController.Draw();

            lightingController.DrawDebug();

            // Draw the AIController
            aiController.Draw();

            // Draw the PhysicsController
            physicsController.Draw();

            // Draw the GUI
            DrawUI();
        }

        /// <summary>
        /// Draw the DrawLayers
        /// </summary>
        private void DrawComponents()
        {
            if (drawLayers != null)
            {
                // Order the Layers by the specified value
                foreach (DrawLayer layer in drawLayers.Values.OrderByDescending(e => e.DrawOrder))
                {
                    layer.Draw();
                }
            }
        }

        /// <summary>
        /// Draw the GUI
        /// </summary>
        private void DrawUI()
        {
        }
    }
}
