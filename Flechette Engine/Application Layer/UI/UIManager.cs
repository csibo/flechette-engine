﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Flechette_Engine
{
    /// <summary>
    /// Manages a collection of UIControls 
    /// (ex. all the controls on a given screen).
    /// </summary>
    public class UIManager : GameObject
    {
        /// <summary>
        /// All the UIControls assigned to this manager.
        /// </summary>
        public SortedList<int, UIControl> Controls { get; set; }

        /// <summary>
        /// Currently active control.
        /// </summary>
        public UIControl FocusedControl { get; protected set; }

        public UIControl MouseLeftDownControl { get; protected set; }

        public UIControl MouseLeftUpControl { get; protected set; }

        public UIManager(Screen containingScreen)
            : base(containingScreen)
        {
            Controls = new SortedList<int, UIControl>();

            UIController.MouseLeftUp += UIController_MouseLeftUp;
            UIController.MouseLeftDown += UIController_MouseLeftDown;
            UIController.KeyboardKeyUp += UIController_KeyboardKeyUp;
        }

        void UIController_KeyboardKeyUp(Keys key)
        {
            if (ContainingScreen == ScreenController.screens[ScreenController.screenStack.First()])
            {
                if (key == Keys.Tab)
                {
                    if (FocusedControl != null)
                    {
                        NextControl();
                    }
                }
            }
        }

        void UIController_MouseLeftDown(object sender, EventArgs e)
        {
            if (ContainingScreen == ScreenController.screens[ScreenController.screenStack.First()])
            {
                UIControl targetControl = Raycast(new Vector2(UIController.Mouse.State.X, UIController.Mouse.State.Y));

                MouseLeftDownControl = null;

                if (targetControl != null)
                {
                    targetControl.OnMouseLeftDown(null);

                    MouseLeftDownControl = targetControl;
                }
            }
        }

        void UIController_MouseLeftUp(object sender, EventArgs e)
        {
            if (ContainingScreen == ScreenController.screens[ScreenController.screenStack.First()])
            {
                UIControl targetControl = Raycast(new Vector2(UIController.Mouse.State.X, UIController.Mouse.State.Y));

                MouseLeftUpControl = null;

                if (targetControl != null)
                {
                    targetControl.OnMouseLeftUp(null);

                    MouseLeftUpControl = targetControl;

                    if (MouseLeftDownControl == MouseLeftUpControl)
                    {
                        FocusOn(targetControl);

                        targetControl.OnMouseLeftClick(null);
                    }
                }
                else
                {
                    BlurAll();
                }

                MouseLeftDownControl = MouseLeftUpControl = null;
            }
        }

        public int Add(UIControl control)
        {
            if (!Controls.Values.Contains(control))
            {
                int index = 0;

                if (Controls.Count() > 0)
                { 
                    index = Controls.Keys.Max() + 1;
                }

                Controls.Add(index, control);
            }

            return Controls.IndexOfValue(control);
        }

        public bool FocusOn(UIControl control)
        {
            // If the control is invalid
            if (control == null)
                return false;

            if (Controls == null)
                return false;

            // If I don't own the control
            if (!Controls.ContainsValue(control))
                return false;

            if (!control.Focusable)
                return false;

            // Blur every other control I own
            BlurAll();

            // Focus the control
            FocusedControl = control;
            control.OnFocus(null);

            return true;
        }

        public bool BlurControl(UIControl control)
        {
            // If the control is invalid
            if (control == null)
                return false;

            if (Controls == null)
                return false;

            // If I don't own the control
            if (!Controls.ContainsValue(control))
                return false;

            if(FocusedControl == control)
                FocusedControl = null;

            control.OnBlur(null);

            return true;
        }

        /// <summary>
        /// Blurs all controls this manager owns.
        /// </summary>
        public void BlurAll()
        {
            if(Controls == null)
                return;

            if(FocusedControl != null)
                BlurControl(FocusedControl);
        }

        protected void NextControl()
        {
            float currentTabOrder = float.MinValue;

            if(FocusedControl != null)
                currentTabOrder = Controls.ElementAt(Controls.IndexOfValue(FocusedControl)).Key;

            if(Controls.Count() > 1)
            {
                if (currentTabOrder == Controls.Keys.Max())
                {
                    currentTabOrder = float.MinValue;
                }

                FocusOn(Controls.First(e => e.Key > currentTabOrder).Value);
            }
        }

        protected void PreviousControl()
        {

        }

        public UIControl Raycast(Vector2 screenLocation)
        {
            if (ContainingScreen == ScreenController.screens[ScreenController.screenStack.First()])
            {
                if (Controls == null || Controls.Count() == 0)
                    return null;

                foreach (UIControl control in Controls.Values.OrderBy(e => e.DrawOrder))
                {
                    if (control.Contains(screenLocation))
                        return control;
                }
            }

            return null;
        }

        public override void Update()
        {
            base.Update();
        }
    }
}
