﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Flechette_Engine
{
    public struct Paddings
    {
        public int Left { get; set; }
        public int Right { get; set; }
        public int Top { get; set; }
        public int Bottom { get; set; }

        /// <summary>
        /// Returns Left + Right
        /// </summary>
        public int Horizontal { get { return Left + Right; } }

        /// <summary>
        /// Returns Top + Bottom
        /// </summary>
        public int Vertical { get { return Top + Bottom; } }
    }

    /// <summary>
    /// Base class for user interface controls.
    /// </summary>
    public class UIControl : GameObject
    {
        /// <summary>
        /// UIManager responsible for this control.
        /// </summary>
        public UIManager Manager { get; set; }

        /// <summary>
        /// True to allow clicks to pass through this control.
        /// Not recommened.
        /// </summary>
        public bool AllowClickThrough { get; set; }

        /// <summary>
        /// Orders the controls within the UIManager for tab navigation.
        /// Default = 1. Lowest means first control.
        /// </summary>
        public float TabOrder { get; set; }

        public Skin Skin;

        public SkinStateProfile ActiveSkin { get; set; }

        public Paddings Padding;

        public SpriteEffects Effects { get; set; }

        public bool Focusable { get; set; }

        public event EventHandler MouseLeftClick;

        public event EventHandler MouseLeftDown;

        public event EventHandler MouseLeftUp;

        public event EventHandler MouseRightClick;

        public event EventHandler Focus;

        public event EventHandler Blur;

        public Action FocusAction { get; set; }

        public Action BlurAction { get; set; }

        public Action MouseLeftClickAction { get; set; }

        public Action MouseLeftDownAction { get; set; }

        public Action MouseLeftUpAction { get; set; }

        public Action MouseRightClickAction { get; set; }

        public Action MouseRightDownAction { get; set; }

        public Action MouseRightUpAction { get; set; }

        public Action MouseMiddleClickAction { get; set; }

        public Action MouseMiddleDownAction { get; set; }

        public Action MouseMiddleUpAction { get; set; }

        public Action MouseX1ClickAction { get; set; }

        public Action MouseX1DownAction { get; set; }

        public Action MouseX1UpAction { get; set; }

        public Action MouseX2ClickAction { get; set; }

        public Action MouseX2DownAction { get; set; }

        public Action MouseX2UpAction { get; set; }

        public UIControl(Screen containingScreen, IGameObject parent, DrawLayer drawLayer, UIManager manager)
            : base(containingScreen, parent, drawLayer)
        {
            AllowClickThrough = false;

            Focusable = true;

            Skin = new Skin()
            {
                Disabled = new SkinStateProfile
                {
                    BackgroundColor = Color.Gray,
                    ForegroundColor = Color.DarkGray,
                },
            };

            ActiveSkin = Skin.Enabled;

            Padding = new Paddings()
            {
                Left = 0,
                Right = 0,
                Top = 0,
                Bottom = 0,
            };

            Effects = SpriteEffects.None;

            Manager = manager;
            Manager.Add(this);
        }

        public override void Update()
        {
            if (!Enabled)
            {
                ActiveSkin = Skin.Disabled;
            }
            else
            {
                if (Manager.FocusedControl == this)
                    ActiveSkin = Skin.Focused;
                else
                    ActiveSkin = Skin.Enabled;


                if (Manager.MouseLeftDownControl == this)
                    ActiveSkin = Skin.Pressed;
            }

            base.Update();
        }

        public override void Draw()
        {
            if(Visible)
            {

            }

            base.Draw();
        }

        #region Events

        public virtual void OnFocus(System.EventArgs e)
        {
            if (Focus != null) Focus(this, e);

            if (FocusAction != null)
                FocusAction();
        }

        public virtual void OnBlur(System.EventArgs e)
        {
            if (Blur != null) Blur(this, e);

            if (BlurAction != null)
                BlurAction();
        }

        public virtual void OnMouseLeftClick(System.EventArgs e)
        { 
            if (MouseLeftClick != null) MouseLeftClick(this, e);

            if (MouseLeftClickAction != null)
                MouseLeftClickAction();
        }

        public virtual void OnMouseLeftDown(System.EventArgs e)
        {
            if (MouseLeftDown != null) MouseLeftDown(this, e);

            if (MouseLeftDownAction != null)
                MouseLeftDownAction();
        }

        public virtual void OnMouseLeftUp(System.EventArgs e)
        {
            if (MouseLeftUp != null) MouseLeftUp(this, e);

            if (MouseLeftUpAction != null)
                MouseLeftUpAction();
        }

        public virtual void OnMouseRightClick(System.EventArgs e)
        {
            if (MouseRightClick != null) MouseRightClick(this, e);

            if (MouseRightClickAction != null)
                MouseRightClickAction();
        }

        #endregion
    }
}
