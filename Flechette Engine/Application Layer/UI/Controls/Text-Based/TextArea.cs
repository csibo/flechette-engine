﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace Flechette_Engine
{
    public class TextArea : UIControl
    {
        public string Text { get; set; }

        protected List<string> Lines { get; set; }

        public string DisplayText { get; set; }

        protected Timer CaretTimer { get; set; }

        public int CaretFrequency { get; set; }

        public bool ShowCaret { get; set; }

        public bool ReadOnly { get; set; }

        public int CaretPosition { get; set; }

        protected Vector2 CaretRenderPosition { get; set; }

        public int TopIndex { get; set; }

        public int BottomIndex { get; set; }

        protected RenderTarget2D TextTexture { get; set; }

        public Scrollbar Scrollbar { get; set; }

        public TextArea(Screen containingScreen, IGameObject parent, DrawLayer drawLayer, UIManager manager)
            : base(containingScreen, parent, drawLayer, manager)
        {
            Size = new Vector2(350, 200);

            Skin.Enabled.ForegroundColor = Color.White;
            Skin.Focused.ForegroundColor = Color.White;
            Skin.Disabled.ForegroundColor = Color.White;
            Skin.Hovered.ForegroundColor = Color.White;
            Skin.Pressed.ForegroundColor = Color.White;
            Skin.Enabled.BackgroundImage = "Dark/TextArea";
            Skin.Focused.BackgroundImage = "Dark/TextArea";
            Skin.Disabled.BackgroundImage = "Dark/TextArea";
            Skin.Hovered.BackgroundImage = "Dark/TextArea";
            Skin.Pressed.BackgroundImage = "Dark/TextArea";

            Skin.Focused.BackgroundColor = Color.White;

            CaretFrequency = 500;
            CaretTimer = new Timer(CaretFrequency);
            CaretTimer.AutoReset = true;
            CaretTimer.Stop();
            CaretTimer.Elapsed += CaretTimer_Elapsed;

            ReadOnly = false;

            Scrollbar = AddChild<Scrollbar>(new object[] { Manager });
            Scrollbar.SyncPosition = false;
            Scrollbar.SyncSize = false;
            //Scrollbar.ScrollbarStyle = Scrollbar.ScrollbarStyles.HandleOnly;

            Lines = new List<string>();

            Padding = new Paddings()
            {
                Top = 5,
                Bottom = 5,
                Left = 10,
                Right = 10,
            };

            Clear();

            UIController.KeyboardCharacter += HandleKeyboardInput;
            UIController.KeyboardKeyDown += KeyPress;
        }

        protected override void Dispose(bool disposing)
        {
            Scrollbar.Dispose();

            base.Dispose(disposing);
        }

        public void Clear()
        {
            Text = "";
            DisplayText = "";

            ShowCaret = false;
            CaretPosition = 0;

            TopIndex = 0;
            BottomIndex = 0;
        }

        protected void HandleKeyboardInput(char character)
        {
            if (Enabled
                && Manager.FocusedControl == this)
            {
                if (character == (char)Keys.Back)
                {
                    if(CaretPosition > 0)
                    {
                        string tempText = Text.Substring(0, CaretPosition - 1);
                        tempText += Text.Substring(CaretPosition);
                        Text = tempText;

                        if(BottomIndex > Text.Length)
                            BottomIndex = Text.Length;

                        CaretPosition--;
                        ScrollTextDown();
                    }
                }
                else if(character == (char)Keys.Tab)
                {
                    return;
                }
                else
                {
                    if (!ReadOnly)
                        AddCharacter(character);
                }
            }
        }

        protected void KeyPress(Keys key)
        {
            if (Enabled
                && Manager.FocusedControl == this)
            {
                if (key == Keys.Left
                    || key == Keys.Right
                    || key == Keys.Up
                    || key == Keys.Down)
                {
                    RequestCaretMove((char)key);
                }
            }
        }

        protected void RequestCaretMove(char character)
        {
            if (character == (char)Keys.Right)
            {
                if (CaretPosition < Text.Length)
                    CaretPosition++;

                if (MeasureText(Text.Substring(TopIndex, CaretPosition - TopIndex)).Y - 6 > Size.Y - MeasureText("aaa").X)
                    ScrollTextUp();
            }
            else if (character == (char)Keys.Left)
            {
                if (CaretPosition > 0)
                    CaretPosition--;

                if (MeasureText(Text.Substring(TopIndex, CaretPosition - TopIndex)).Y - 6 < MeasureText("aaa").Y)
                    ScrollTextDown();
            }

            ShowCaret = true;
            CaretTimer.Stop();
            CaretTimer.Start();
        }

        protected void CaretTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            ShowCaret = !ShowCaret;

            if(Manager.FocusedControl != this)
                ShowCaret = false;
        }

        public void SetCaretAtEnd()
        {
            CaretPosition = Text.Length;
            BottomIndex = CaretPosition;
        }

        protected Vector2 GetCaretPostion()
        {
            int x = (int)(MeasureText(Text.Substring(0, CaretPosition)).X - (MeasureText("|").X / 2));
            int y = 0;

            while(x > Size.X - Padding.Horizontal - Scrollbar.Size.X)
            {
                y += (int)MeasureText("|").Y;
                x -= (int)(Size.X - Padding.Horizontal - Scrollbar.Size.X);
            }

            Vector2 pos = new Vector2(x, y);

            return pos;
        }

        protected void ScrollTextUp(int rounds = 1)
        {
            if (BottomIndex < Text.Length)
                BottomIndex++;

            if (MeasureText(Text.Substring(TopIndex, BottomIndex - TopIndex)).Y > Size.Y)
                TopIndex++;
        }

        protected void ScrollTextDown(int rounds = 1)
        {
            if (TopIndex > 0)
                TopIndex--;

            if (MeasureText(Text.Substring(TopIndex, BottomIndex - TopIndex)).Y > Size.Y)
                BottomIndex--;
        }

        public void AddCharacter(char c)
        {
            if (CaretPosition < Text.Length)
                Text = Text.Insert(CaretPosition, c.ToString());
            else
                Text += c;

            RequestCaretMove((char)Keys.Right);
        }

        public void AddString(string text)
        {
            foreach(char c in text)
            {
                AddCharacter(c);
            }
        }

        public override void Update()
        {
            if(Enabled)
            {
                TopIndex = (int)Scrollbar.Value;

                TopIndex = Math.Max(TopIndex, 0);
                BottomIndex = Math.Min(BottomIndex, Lines.Count());

                if (TopIndex > Lines.Count())
                {
                    TopIndex = Lines.Count() - 1;
                }
                if (BottomIndex < Lines.Count())
                {
                    BottomIndex = Text.Length;
                }

                DisplayText = ParseText(Text);

                ManageCaret();

                Scrollbar.MinValue = 0;
                if (TextTexture != null)
                {
                    Scrollbar.MaxValue = TextTexture.Bounds.Height;
                    Scrollbar.StepSize = (TextTexture.Bounds.Height / Lines.Count());
                }
                Scrollbar.Size = new Vector2(Scrollbar.Size.X, Size.Y);
                Scrollbar.Position = new Vector2(Position.X + Size.X - Scrollbar.Size.X, Position.Y);
                Scrollbar.Update();
            }

            base.Update();
        }

        public override void Draw()
        {
            if (Visible)
            {
                RenderText();

                ContainingScreen.spriteBatch.Draw(
                    TextTexture,
                    GetRectangle(),
                    TextTexture.Bounds,
                    ActiveSkin.BackgroundColor,
                    Rotation,
                    Vector2.Zero,
                    Effects,
                    DrawOrder);
            }

            base.Draw();
        }

        protected void RenderText()
        {
            if (TextTexture != null)
                TextTexture.Dispose();

            TextTexture = new RenderTarget2D(GameController.game.GraphicsDevice, (int)Size.X, (int)Size.Y + Padding.Bottom);

            GameController.game.GraphicsDevice.SetRenderTarget(TextTexture);
            GameController.game.GraphicsDevice.Clear(Color.Transparent);

            SpriteBatch batch = new SpriteBatch(GameController.game.GraphicsDevice);

            Texture2D background = GraphicsDefaults.WhitePixel;

            if (ActiveSkin.BackgroundImage != null)
                background = Game.Content.Load<Texture2D>(ActiveSkin.BackgroundImage);

            batch.Begin();

            batch.Draw(
                background,
                TextTexture.Bounds,
                background.Bounds,
                ActiveSkin.BackgroundColor,
                0,
                Vector2.Zero,
                Effects,
                0.6f);

            batch.DrawString(
                ActiveSkin.FontProfile.Font,
                DisplayText,
                new Vector2(
                    Padding.Left,
                    Padding.Top - Scrollbar.Value),
                ActiveSkin.ForegroundColor,
                0,
                Vector2.Zero,
                ActiveSkin.FontProfile.Scale,
                SpriteEffects.None,
                0.5f);

            if (Manager.FocusedControl == this && ShowCaret)
                batch.DrawString(
                    ActiveSkin.FontProfile.Font,
                    "|",
                    CaretRenderPosition,
                    ActiveSkin.ForegroundColor,
                    0,
                    Vector2.Zero,
                    ActiveSkin.FontProfile.Scale,
                    SpriteEffects.None,
                    0.4f);

            batch.End();

            GameController.game.GraphicsDevice.SetRenderTarget(null);
        }

        private String ParseText(String text)
        {
            String line = String.Empty;
            String returnString = String.Empty;
            String[] wordArray = text.Split(' ');
            Lines = new List<string>();
            int chars = 0;


            foreach (String word in wordArray)
            {
                if (ActiveSkin.FontProfile.Font.MeasureString(line + word).X > Size.X - Scrollbar.Size.X - Padding.Horizontal)
                {
                    returnString = returnString + line + '\n';

                    Lines.Add(line);

                    if (chars < CaretPosition)
                        CaretRenderPosition += new Vector2(0, (MeasureText("\n").Y/2));

                    line = String.Empty;
                }

                line += word + ' ';

                chars += word.Count();

                if (chars > CaretPosition)
                    CaretRenderPosition += new Vector2(MeasureText(line.Substring(0, CaretPosition)).X, 0);

            }

            Lines.Add(line);

            return returnString + line;
        }

        /// <summary>
        /// Places the navigation caret in the text.
        /// </summary>
        protected void ManageCaret()
        {
            // If this is the active control
            if (Manager.FocusedControl == this)
            {
                // Start the caret timer if it's off
                if (!CaretTimer.Enabled)
                    CaretTimer.Start();
            }
            else
            {
                // Turn off the caret timer
                if (CaretTimer.Enabled)
                    CaretTimer.Stop();
            }
        }

        public Vector2 MeasureText(string line = null)
        {
            Vector2 measurements = new Vector2(0,0);

            if (line == null) line = Text;

            if (ActiveSkin.FontProfile.Font != null)
                measurements = TextDisplayUtils.MeasureText(line, ActiveSkin.FontProfile.Font);

            return measurements;
        }
    }
}
