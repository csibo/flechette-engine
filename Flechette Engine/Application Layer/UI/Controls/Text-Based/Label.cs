﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Flechette_Engine
{
    public class Label : UIControl
    {
        public TextSprite TextSprite { get; protected set; }

        public string Text { get; set; }

        public float Scale { get; set; }

        public SpriteFont Font { get; set; }

        public Label(Screen containingScreen, IGameObject parent, DrawLayer drawLayer, UIManager manager)
            : base(containingScreen, parent, drawLayer, manager)
        {
            Color = Color.Black;
            Text = "Label";
            Scale = 1f;
            Font = UIController.DefaultFont;

            TextSprite = AddChild<TextSprite>();
        }

        public override void Update()
        {
            if (Enabled)
            {
                if (TextSprite != null)
                {
                    TextSprite.Color = Color;
                    TextSprite.Text = Text;
                    TextSprite.Scale = Scale;
                    TextSprite.Font = Font;
                    TextSprite.DrawOrder = DrawOrder;
                }
            }

            base.Update();
        }
    }
}
