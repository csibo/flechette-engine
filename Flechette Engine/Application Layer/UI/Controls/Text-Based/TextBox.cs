﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace Flechette_Engine
{
    public class TextBox : UIControl
    {
        public string Text { get; set; }

        public string DisplayText { get; set; }

        public bool IsPassword { get; set; }

        public char PasswordCharacter { get; set; }

        public TextOverflowMethods TextOverflowMethod { get; set; }

        public Timer CaretTimer { get; set; }

        public bool ShowCaret { get; set; }

        public bool ReadOnly { get; set; }

        public int CaretPosition { get; set; }

        public int LeftIndex { get; set; }

        public int RightIndex { get; set; }

        protected RenderTarget2D TextTexture { get; set; }

        public TextBox(Screen containingScreen, IGameObject parent, DrawLayer drawLayer, UIManager manager)
            : base(containingScreen, parent, drawLayer, manager)
        {
            Size = new Vector2(150, 50);

            Skin.Enabled.ForegroundColor = Color.White;
            Skin.Focused.ForegroundColor = Color.White;
            Skin.Disabled.ForegroundColor = Color.White;
            Skin.Hovered.ForegroundColor = Color.White;
            Skin.Pressed.ForegroundColor = Color.White;
            Skin.Enabled.BackgroundImage = "Dark/Textbox";
            Skin.Focused.BackgroundImage = "Dark/Textbox";
            Skin.Disabled.BackgroundImage = "Dark/Textbox";
            Skin.Hovered.BackgroundImage = "Dark/Textbox";
            Skin.Pressed.BackgroundImage = "Dark/Textbox";

            IsPassword = false;
            PasswordCharacter = '*';

            TextOverflowMethod = TextOverflowMethods.Hide;

            CaretTimer = new Timer(500);
            CaretTimer.AutoReset = true;
            CaretTimer.Stop();
            CaretTimer.Elapsed += CaretTimer_Elapsed;

            ReadOnly = false;

            Clear();

            UIController.KeyboardCharacter += HandleKeyboardInput;
            UIController.KeyboardKeyDown += KeyPress;
            UIController.MouseWheelMove += UIController_MouseWheelMove;
        }

        public void Clear()
        {
            Text = "";
            DisplayText = "";

            ShowCaret = false;
            CaretPosition = 0;

            LeftIndex = 0;
            RightIndex = 0;
        }

        protected void HandleKeyboardInput(char character)
        {
            if (Enabled
                && Manager.FocusedControl == this)
            {
                if (character == (char)Keys.Back)
                {
                    if(CaretPosition > 0)
                    {
                        string tempText = Text.Substring(0, CaretPosition - 1);
                        tempText += Text.Substring(CaretPosition);
                        Text = tempText;

                        if(RightIndex > Text.Length)
                            RightIndex = Text.Length;

                        CaretPosition--;
                        ScrollTextLeft();
                    }
                }
                else if(character == (char)Keys.Tab)
                {
                    return;
                }
                else
                {
                    if (!ReadOnly)
                        AddCharacter(character);
                }
            }
        }

        protected void KeyPress(Keys key)
        {
            if (Enabled
                && Manager.FocusedControl == this)
            {
                if (key == Keys.Left
                    || key == Keys.Right
                    || key == Keys.Up
                    || key == Keys.Down)
                {
                    RequestCaretMove((char)key);
                }
            }
        }

        protected void RequestCaretMove(char character)
        {
            if (character == (char)Keys.Right)
            {
                if (CaretPosition < Text.Length)
                    CaretPosition++;

                if (MeasureText(Text.Substring(LeftIndex, CaretPosition - LeftIndex)).X - 6 > Size.X - MeasureText("aaa").X)
                    ScrollTextRight();
            }
            else if (character == (char)Keys.Left)
            {
                if (CaretPosition > 0)
                    CaretPosition--;

                if (MeasureText(Text.Substring(LeftIndex, CaretPosition - LeftIndex)).X - 6 < MeasureText("aaa").X)
                    ScrollTextLeft();
            }

            ShowCaret = true;
            CaretTimer.Stop();
            CaretTimer.Start();
        }

        protected void CaretTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            ShowCaret = !ShowCaret;

            if(Manager.FocusedControl != this)
                ShowCaret = false;
        }

        public int SetCaretAtEnd()
        {
            CaretPosition = Text.Length;
            RightIndex = CaretPosition;

            return CaretPosition;
        }

        protected Vector2 GetCaretPostion()
        {
            int x = (int)(MeasureText(DisplayText.Substring(LeftIndex, CaretPosition - LeftIndex)).X - (MeasureText("|").X / 2));
            int y = 0;

            Vector2 pos = new Vector2(x, y);

            return pos;
        }

        void UIController_MouseWheelMove(float ticks)
        {
            if (Manager.FocusedControl == this
                ||
                    (Parent != null
                    && Manager.FocusedControl == this.Parent))
            {
                RequestCaretMove((ticks < 0) ? (char)Keys.Right : (char)Keys.Left);
            }
        }

        protected void ScrollTextRight(int rounds = 1)
        {
            if (RightIndex < Text.Length)
                RightIndex++;

            if (MeasureText(Text.Substring(LeftIndex, RightIndex - LeftIndex)).X > Size.X)
                LeftIndex++;
        }

        protected void ScrollTextLeft(int rounds = 1)
        {
            if (LeftIndex > 0)
                LeftIndex--;

            if (MeasureText(Text.Substring(LeftIndex, RightIndex - LeftIndex)).X > Size.X)
                RightIndex--;
        }

        public void AddCharacter(char c)
        {
            if (CaretPosition < Text.Length)
                Text = Text.Insert(CaretPosition, c.ToString());
            else
                Text += c;

            RequestCaretMove((char)Keys.Right);
        }

        public void AddString(string text)
        {
            foreach (char c in text)
            {
                AddCharacter(c);
            }
        }

        public override void Update()
        {
            if(Enabled)
            {
                LeftIndex = Math.Max(LeftIndex, 0);
                RightIndex = Math.Min(RightIndex, Text.Length);

                if(LeftIndex > Text.Length)
                {
                    LeftIndex = Text.Length - 1;
                }
                if (RightIndex < Text.Length)
                {
                    RightIndex = Text.Length;
                }

                //DisplayText = Text.Substring(LeftIndex, RightIndex - LeftIndex);
                DisplayText = Text;

                // Hide password text
                if(IsPassword)
                    DisplayText = TextCreationUtils.Replace(DisplayText, PasswordCharacter);

                ManageCaret();
            }

            base.Update();
        }

        public override void Draw()
        {
            if (Visible)
            {
                RenderText();

                ContainingScreen.spriteBatch.Draw(
                    TextTexture,
                    GetRectangle(),
                    TextTexture.Bounds,
                    ActiveSkin.BackgroundColor,
                    Rotation,
                    Vector2.Zero,
                    Effects,
                    DrawOrder);
            }

            base.Draw();
        }

        protected void RenderText()
        {
            if (TextTexture != null)
                TextTexture.Dispose();

            TextTexture = new RenderTarget2D(GameController.game.GraphicsDevice, (int)Size.X, (int)Size.Y);

            GameController.game.GraphicsDevice.SetRenderTarget(TextTexture);
            GameController.game.GraphicsDevice.Clear(Color.Transparent);

            SpriteBatch batch = new SpriteBatch(GameController.game.GraphicsDevice);

            Texture2D background = GraphicsDefaults.WhitePixel;

            if (ActiveSkin.BackgroundImage != null)
                background = Game.Content.Load<Texture2D>(ActiveSkin.BackgroundImage);

            batch.Begin();

            batch.Draw(
                background,
                TextTexture.Bounds,
                background.Bounds,
                ActiveSkin.BackgroundColor,
                0,
                Vector2.Zero,
                SpriteEffects.None,
                0.6f);

            batch.DrawString(
                ActiveSkin.FontProfile.Font,
                DisplayText,
                new Vector2(
                    -MeasureText(Text.Substring(0, LeftIndex)).X,
                    (Size.Y / 2) - (MeasureText().Y / 2)),
                ActiveSkin.ForegroundColor,
                0,
                Vector2.Zero,
                ActiveSkin.FontProfile.Scale,
                SpriteEffects.None,
                0.5f);

            if (Manager.FocusedControl == this && ShowCaret)
                batch.DrawString(
                    ActiveSkin.FontProfile.Font,
                    "|",
                    GetCaretPostion(),
                    ActiveSkin.ForegroundColor,
                    0,
                    Vector2.Zero,
                    ActiveSkin.FontProfile.Scale,
                    SpriteEffects.None,
                    0.5f);

            batch.End();

            GameController.game.GraphicsDevice.SetRenderTarget(null);
        }

        /// <summary>
        /// Places the navigation caret in the text.
        /// </summary>
        protected void ManageCaret()
        {
            // If this is the active control
            if (Manager.FocusedControl == this)
            {
                // Start the caret timer if it's off
                if (!CaretTimer.Enabled)
                    CaretTimer.Start();
            }
            else
            {
                // Turn off the caret timer
                if (CaretTimer.Enabled)
                    CaretTimer.Stop();
            }
        }

        public Vector2 MeasureText(string line = null)
        {
            Vector2 measurements = new Vector2(0,0);

            if (line == null) line = DisplayText;

            if (ActiveSkin.FontProfile.Font != null)
                measurements = TextDisplayUtils.MeasureText(line, ActiveSkin.FontProfile.Font);

            return measurements;
        }
    }
}
