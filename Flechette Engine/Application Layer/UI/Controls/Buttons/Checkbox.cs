﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace Flechette_Engine
{
    /// <summary>
    /// Control that is either checked or unchecked.
    /// </summary>
    public class Checkbox : UIControl
    {
        protected RenderTarget2D DisplayTexture { get; set; }

        public bool IsChecked { get; set; }

        public event EventHandler Checked;

        public event EventHandler Unchecked;

        public Checkbox(Screen containingScreen, IGameObject parent, DrawLayer drawLayer, UIManager manager)
            : base(containingScreen, parent, drawLayer, manager)
        {
            Size = new Vector2(15, 15);

            IsChecked = false;

            Skin.Enabled.ForegroundColor = Color.White;
            Skin.Disabled.ForegroundColor = Color.White;
            Skin.Focused.ForegroundColor = Color.White;
            Skin.Hovered.ForegroundColor = Color.White;
            Skin.Pressed.BackgroundColor = new Color(200, 200, 200);

            Skin.Enabled.BackgroundImage = "Dark/Checkbox - Unchecked";
            Skin.Focused.BackgroundImage = "Dark/Checkbox - Unchecked";
            Skin.Disabled.BackgroundImage = "Dark/Checkbox - Unchecked";
            Skin.Hovered.BackgroundImage = "Dark/Checkbox - Unchecked";
            Skin.Pressed.BackgroundImage = "Dark/Checkbox - Unchecked";

            MouseLeftClick += Checkbox_MouseLeftClick;
        }

        void Checkbox_MouseLeftClick(object sender, EventArgs e)
        {
            IsChecked = !IsChecked;
        }

        public override void Draw()
        {
            if(Visible)
            {
                if (IsChecked)
                {

                    Skin.Enabled.BackgroundImage = "Dark/Checkbox - Checked";
                    Skin.Focused.BackgroundImage = "Dark/Checkbox - Checked";
                    Skin.Disabled.BackgroundImage = "Dark/Checkbox - Checked";
                    Skin.Hovered.BackgroundImage = "Dark/Checkbox - Checked";
                    Skin.Pressed.BackgroundImage = "Dark/Checkbox - Checked";
                }
                else
                {
                    Skin.Enabled.BackgroundImage = "Dark/Checkbox - Unchecked";
                    Skin.Focused.BackgroundImage = "Dark/Checkbox - Unchecked";
                    Skin.Disabled.BackgroundImage = "Dark/Checkbox - Unchecked";
                    Skin.Hovered.BackgroundImage = "Dark/Checkbox - Unchecked";
                    Skin.Pressed.BackgroundImage = "Dark/Checkbox - Unchecked";
                }

                Texture2D background = GraphicsDefaults.WhitePixel;

                if (ActiveSkin.BackgroundImage != null)
                    background = Game.Content.Load<Texture2D>(ActiveSkin.BackgroundImage);

                ContainingScreen.spriteBatch.Draw(
                    background,
                    GetRectangle(),
                    background.Bounds,
                    ActiveSkin.BackgroundColor,
                    Rotation,
                    Vector2.Zero,
                    Effects,
                    DrawOrder);
            }

            base.Draw();
        }

        public void OnChecked()
        {
            if (Checked != null) Checked.Invoke(this, null);
        }

        public void OnUnchecked()
        {
            if (Unchecked != null) Unchecked(this, null);
        }
    }
}
