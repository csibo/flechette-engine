﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flechette_Engine
{
    public class RadioButtonController
    {
        public List<RadioButton> RadioButtons { get; set; }

        public RadioButtonController()
        {
            RadioButtons = new List<RadioButton>();
        }

        public bool Check(RadioButton button)
        {
            if (RadioButtons == null)
                return false;

            if (!RadioButtons.Contains(button))
                return false;

            UncheckAll();

            button.IsChecked = true;
            button.OnChecked();

            return true;
        }

        public void UncheckAll()
        {
            foreach(RadioButton button in RadioButtons)
            {
                button.IsChecked = false;
                button.OnUnchecked();
            }
        }
    }
}
