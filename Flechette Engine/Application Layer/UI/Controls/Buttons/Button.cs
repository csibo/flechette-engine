﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace Flechette_Engine
{
    /// <summary>
    /// Control that performs an action on click events.
    /// </summary>
    public class Button : UIControl
    {
        protected RenderTarget2D DisplayTexture { get; set; }

        public string Text { get; set; }

        public Button(Screen containingScreen, IGameObject parent, DrawLayer drawLayer, UIManager manager)
            : base(containingScreen, parent, drawLayer, manager)
        {
            Size = new Vector2(100, 25);

            Text = "Button";

            Skin.Enabled.ForegroundColor = Color.White;
            Skin.Disabled.ForegroundColor = Color.White;
            Skin.Focused.ForegroundColor = Color.White;
            Skin.Hovered.ForegroundColor = Color.White;
            Skin.Pressed.ForegroundColor = Color.White;

            Skin.Enabled.BackgroundImage = "Dark/Button";
            Skin.Focused.BackgroundImage = "Dark/Button";
            Skin.Disabled.BackgroundImage = "Dark/Button";
            Skin.Hovered.BackgroundImage = "Dark/Button";
            Skin.Pressed.BackgroundImage = "Dark/Button";

            Skin.Pressed.BackgroundColor = new Color(200, 200, 200);
        }

        public override void Draw()
        {
            if(Visible)
            {
                RenderText();

                ContainingScreen.spriteBatch.Draw(
                    DisplayTexture,
                    GetRectangle(),
                    DisplayTexture.Bounds,
                    Color.White,
                    Rotation,
                    Vector2.Zero,
                    Effects,
                    DrawOrder);
            }

            base.Draw();
        }

        protected void RenderText()
        {
            DisplayTexture = new RenderTarget2D(GameController.game.GraphicsDevice, (int)Size.X, (int)Size.Y);

            GameController.game.GraphicsDevice.SetRenderTarget(DisplayTexture);
            GameController.game.GraphicsDevice.Clear(Color.Transparent);

            SpriteBatch batch = new SpriteBatch(GameController.game.GraphicsDevice);

            Texture2D background = GraphicsDefaults.WhitePixel;

            if(ActiveSkin.BackgroundImage != null)
                background = Game.Content.Load<Texture2D>(ActiveSkin.BackgroundImage);

            batch.Begin();

            batch.Draw(
                background,
                DisplayTexture.Bounds,
                background.Bounds,
                ActiveSkin.BackgroundColor,
                0,
                Vector2.Zero,
                SpriteEffects.None,
                0.8f);

            batch.DrawString(
                ActiveSkin.FontProfile.Font,
                Text,
                new Vector2(
                    (Size.X / 2f) - ((ActiveSkin.FontProfile.Font.MeasureString(Text).X * ActiveSkin.FontProfile.Scale) / 2f) - 1,
                    (Size.Y / 2f) - ((ActiveSkin.FontProfile.Font.MeasureString(Text).Y * ActiveSkin.FontProfile.Scale) / 2f)),
                ActiveSkin.ForegroundColor,
                0,
                Vector2.Zero,
                ActiveSkin.FontProfile.Scale,
                SpriteEffects.None,
                0.5f);

            batch.End();

            GameController.game.GraphicsDevice.SetRenderTarget(null);
        }
    }
}
