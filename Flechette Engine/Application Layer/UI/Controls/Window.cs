﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flechette_Engine
{
    public class Window : UIControl
    {
        public bool DisplayBorder { get; set; }

        public bool DisplayTitleBar { get; set; }

        public Window(Screen containingScreen, IGameObject parent, DrawLayer drawLayer, UIManager manager)
            : base(containingScreen, parent, drawLayer, manager)
        {
            DisplayBorder = true;
            DisplayTitleBar = true;
        }
    }
}
