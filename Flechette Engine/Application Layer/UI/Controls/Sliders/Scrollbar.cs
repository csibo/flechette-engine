﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace Flechette_Engine
{
    /// <summary>
    /// Control that scrolls the view of another item.
    /// </summary>
    public class Scrollbar : UIControl
    {
        public enum ScrollbarStyles
        {
            HandleAndButtons,
            HandleOnly,
            ButtonsOnly
        }

        public float Value { get; protected set; }

        public float MaxValue { get; set; }

        public float MinValue { get; set; }

        public float StepSize { get; set; }

        public int HandleSize { get; set; }

        public ScrollbarStyles ScrollbarStyle { get; set; }

        protected Button Handle;

        protected Button IncButton;

        protected Button DecButton;

        protected RenderTarget2D DisplayTexture { get; set; }

        public event EventHandler ValueChanged;

        public Scrollbar(Screen containingScreen, IGameObject parent, DrawLayer drawLayer, UIManager manager)
            : base(containingScreen, parent, drawLayer, manager)
        {
            Size = new Vector2(15, 200);

            MinValue = 0;
            MaxValue = 10;
            StepSize = 1;
            Value = 0;

            Padding = new Paddings()
            {
                Top = 0,
                Bottom = 0,
                Left = 2,
                Right = 2,
            };

            Skin.Enabled.BackgroundColor = Color.White;
            Skin.Focused.BackgroundColor = Color.White;
            Skin.Disabled.BackgroundColor = Color.White;
            Skin.Pressed.BackgroundColor = Color.White;
            Skin.Hovered.BackgroundColor = Color.White;

            Skin.Enabled.BackgroundImage = "Dark/Scrollbar";
            Skin.Focused.BackgroundImage = "Dark/Scrollbar";
            Skin.Disabled.BackgroundImage = "Dark/Scrollbar";
            Skin.Pressed.BackgroundImage = "Dark/Scrollbar";
            Skin.Hovered.BackgroundImage = "Dark/Scrollbar";

            HandleSize = 75;

            ScrollbarStyle = ScrollbarStyles.HandleAndButtons;

            Handle = AddChild<Button>(new object[] { Manager });
            Handle.SyncPosition = false;
            Handle.SyncSize = false;
            Handle.Text = "";
            Handle.Skin.Enabled.BackgroundImage = "Dark/Scrollbar Handle";
            Handle.Skin.Pressed.BackgroundImage = "Dark/Scrollbar Handle";
            Handle.Skin.Focused.BackgroundImage = "Dark/Scrollbar Handle";
            Handle.Skin.Pressed.BackgroundColor = new Color(200, 200, 200);

            DecButton = AddChild<Button>(new object[] { Manager });
            DecButton.SyncPosition = false;
            DecButton.SyncSize = false;
            DecButton.Text = "";
            DecButton.Skin.Enabled.BackgroundImage = "Dark/Small Arrow button";
            DecButton.Skin.Pressed.BackgroundImage = "Dark/Small Arrow button";
            DecButton.Skin.Focused.BackgroundImage = "Dark/Small Arrow button";
            DecButton.Skin.Pressed.BackgroundColor = new Color(200, 200, 200);
            DecButton.Focus += FocusPassthrough;
            DecButton.MouseLeftUpAction = () =>
            {
                SetValue(Value - StepSize);
            };

            IncButton = AddChild<Button>(new object[] { Manager });
            IncButton.SyncPosition = false;
            IncButton.SyncSize = false;
            IncButton.Effects = SpriteEffects.FlipVertically;
            IncButton.Text = "";
            IncButton.Skin.Enabled.BackgroundImage = "Dark/Small Arrow button";
            IncButton.Skin.Pressed.BackgroundImage = "Dark/Small Arrow button";
            IncButton.Skin.Focused.BackgroundImage = "Dark/Small Arrow button";
            IncButton.Skin.Pressed.BackgroundColor = new Color(200, 200, 200);
            IncButton.MouseLeftUpAction = () =>
                {
                    SetValue(Value + StepSize);
                };

            UIController.MouseMove += UIController_MouseMove;
            UIController.MouseWheelMove += UIController_MouseWheelMove;

            Focus += FocusPassthrough;
            MouseLeftDown += FocusPassthrough;
        }

        void UIController_MouseWheelMove(float ticks)
        {
            if (Manager.FocusedControl == this
                || Manager.MouseLeftDownControl == Handle
                || (Parent != null
                    && Manager.FocusedControl == this.Parent))
            {
                SetValue(Value - (ticks * StepSize));
            }
        }

        void FocusPassthrough(object sender, EventArgs e)
        {
            if (Parent != null)
                Manager.FocusOn((UIControl)Parent);
        }

        void UIController_MouseMove(float x, float y)
        {
            if (Manager.MouseLeftDownControl == this
                || Manager.MouseLeftDownControl == Handle)
            {
                SetValue(new Vector2(x, y));
            }
        }

        public float CalcAbsoluteStepSize()
        {
            float temp = Size.Y - Padding.Vertical - HandleSize;

            if (ScrollbarStyle == ScrollbarStyles.HandleAndButtons)
                temp -= (IncButton.Size.Y + DecButton.Size.Y);

            return (temp / CalcRelativeStepSize());
        }

        protected float CalcRelativeStepSize()
        {
            return (MaxValue - MinValue) / StepSize;
        }

        protected Vector2 CalcHandlePosition()
        {
            float x = Padding.Left;
            float y = Padding.Top;

            if (ScrollbarStyle == ScrollbarStyles.HandleAndButtons)
                y += IncButton.Size.Y;

            float temp = Size.Y - Padding.Vertical - HandleSize;

            if (ScrollbarStyle == ScrollbarStyles.HandleAndButtons)
                temp -= (IncButton.Size.Y + DecButton.Size.Y);

            y += (Value / (MaxValue - MinValue)) * temp;

            return new Vector2(x, y);
        }

        public void SetValue(Vector2 screenPosition)
        {
            float value = 0;

            float MinPos = Position.Y + Padding.Vertical;
            float MaxPos = Position.Y + Size.Y - Padding.Vertical + HandleSize;

            if(ScrollbarStyle == ScrollbarStyles.HandleAndButtons)
            {
                MinPos += IncButton.Size.Y;
                MaxPos += (DecButton.Size.Y + IncButton.Size.Y);
            }

            if(screenPosition.Y <= MinPos)
            {
                value = 0;
            }
            else if (screenPosition.Y >= MaxPos)
            {
                value = MaxValue;
            }
            else
            {
                value = screenPosition.Y - Position.Y;

                if (ScrollbarStyle == ScrollbarStyles.HandleAndButtons)
                    value -= DecButton.Size.Y;

                float denom = Size.Y - Padding.Vertical;

                if (ScrollbarStyle == ScrollbarStyles.HandleAndButtons)
                    denom -= (IncButton.Size.Y + DecButton.Size.Y);

                value /= denom;

                value *= MaxValue;
            }

            SetValue(value);
        }

        public void SetValue(float value)
        {
            Value = value;

            Value -= Value % StepSize;

            Value = Math.Max(Value, MinValue);
            Value = Math.Min(Value, MaxValue);

            OnValueChanged(null);
        }

        public override void Update()
        {
            Handle.Position = Position + CalcHandlePosition();
            Handle.Size = new Vector2(Size.X - Padding.Horizontal, HandleSize);

            DecButton.Position = Position;
            DecButton.Size = new Vector2(Size.X, Size.X);

            IncButton.Position = Position + new Vector2(0, Size.Y - DecButton.Size.Y);
            IncButton.Size = new Vector2(Size.X, Size.X);

            if (ScrollbarStyle == ScrollbarStyles.HandleOnly)
            {
                DecButton.Visible = false;
                DecButton.Enabled = false;

                IncButton.Visible = false;
                IncButton.Enabled = false;
            }

            base.Update();
        }

        public override void Draw()
        {
            if(Visible)
            {
                Texture2D background = GraphicsDefaults.WhitePixel;

                if (ActiveSkin.BackgroundImage != null)
                    background = Game.Content.Load<Texture2D>(ActiveSkin.BackgroundImage);

                ContainingScreen.spriteBatch.Draw(
                    background,
                    GetRectangle(),
                    background.Bounds,
                    ActiveSkin.BackgroundColor,
                    Rotation,
                    Vector2.Zero,
                    Effects,
                    DrawOrder);
            }

            base.Draw();
        }

        public virtual void OnValueChanged(System.EventArgs e)
        {
            if (ValueChanged != null) ValueChanged(this, e);
        }
    }
}
