﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Flechette_Engine
{
    /// <summary>
    /// Collection of settings for the engine.
    /// </summary>
    public static class Settings
    {
        /// <summary>
        /// Make physics use its own thread or not.
        /// Experimental.
        /// </summary>
        public static bool ThreadedPhysics { get; set; }

        /// <summary>
        /// Make AI use its own thread or not.
        /// Experimental.
        /// </summary>
        public static bool ThreadedAI { get; set; }

        /// <summary>
        /// Target time between Update() calls for the engine in general.
        /// </summary>
        public static TimeSpan EngineUpdateRate { 
            get { return GameController.game.TargetElapsedTime; }
            set { GameController.game.TargetElapsedTime = value; }
        }

        /// <summary>
        /// Target time between Update() calls for the physics.
        /// </summary>
        public static TimeSpan PhysicsUpdateRate { get; set; }

        /// <summary>
        /// Whether to impose the TargetUpdateRate restriction or not.
        /// If not, Update() will be called as often as possible.
        /// </summary>
        public static bool FixedTimeStep { 
            get { return GameController.game.IsFixedTimeStep; } 
            set { GameController.game.IsFixedTimeStep = value; } }

        /// <summary>
        /// Whether to use V-Sync or not.
        /// </summary>
        public static bool VerticalSync { 
            get { return GameController.graphics.SynchronizeWithVerticalRetrace; } 
            set { GameController.graphics.SynchronizeWithVerticalRetrace = value; } }

        public static bool ResolutionIndependant { get; set; }

        public static Vector2 RenderResolution { get; set; }

        public static Vector2 DisplayResolution 
        {
            get { return new Vector2(GameController.game.GraphicsDevice.Viewport.Width, GameController.game.GraphicsDevice.Viewport.Height); }
            set { DisplayMechanics.ChangeResolution((int)value.X, (int)value.Y); }
        }

        public static bool CursorVisible
        {
            get { return GameController.game.IsMouseVisible; }
            set { GameController.game.IsMouseVisible = value; }
        }

        /// <summary>
        /// Static Constructor.
        /// </summary>
        static Settings()
        {
            ThreadedPhysics = false;

            ThreadedAI = false;

            EngineUpdateRate = TimeSpan.FromMilliseconds(16.5);

            PhysicsUpdateRate = TimeSpan.FromMilliseconds(16.5);

            FixedTimeStep = true;

            VerticalSync = false;

            ResolutionIndependant = false;
        }
    }
}
